/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.json

import com.swapper.reflect.TypeParser

fun JsonArray.parseBooleanArray(): BooleanArray = parseObject(BooleanArray::class.java)
fun JsonArray.parseCharArray(): CharArray = parseObject(CharArray::class.java)
fun JsonArray.parseByteArray(): ByteArray = parseObject(ByteArray::class.java)
fun JsonArray.parseShortArray(): ShortArray = parseObject(ShortArray::class.java)
fun JsonArray.parseIntArray(): IntArray = parseObject(IntArray::class.java)
fun JsonArray.parseLongArray(): LongArray = parseObject(LongArray::class.java)
fun JsonArray.parseFloatArray(): FloatArray = parseObject(FloatArray::class.java)
fun JsonArray.parseDoubleArray(): DoubleArray = parseObject(DoubleArray::class.java)

fun JsonArray.parseBooleanArray(context: JsonContext): BooleanArray = parseObject(context, BooleanArray::class.java)
fun JsonArray.parseCharArray(context: JsonContext): CharArray = parseObject(context, CharArray::class.java)
fun JsonArray.parseByteArray(context: JsonContext): ByteArray = parseObject(context, ByteArray::class.java)
fun JsonArray.parseShortArray(context: JsonContext): ShortArray = parseObject(context, ShortArray::class.java)
fun JsonArray.parseIntArray(context: JsonContext): IntArray = parseObject(context, IntArray::class.java)
fun JsonArray.parseLongArray(context: JsonContext): LongArray = parseObject(context, LongArray::class.java)
fun JsonArray.parseFloatArray(context: JsonContext): FloatArray = parseObject(context, FloatArray::class.java)
fun JsonArray.parseDoubleArray(context: JsonContext): DoubleArray = parseObject(context, DoubleArray::class.java)

inline fun <reified T> JsonArray.parseArray(): Array<T> =
  parseObject(object : TypeParser<Array<T>>() {}.type)

inline fun <reified T> JsonArray.parseArray(context: JsonContext): Array<T> =
  parseObject(context, object : TypeParser<Array<T>>() {}.type)

inline fun <reified T> JsonArray.parseList(): List<T> =
  parseObject(object : TypeParser<List<T>>() {}.type)

inline fun <reified T> JsonArray.parseMutableList(): MutableList<T> =
  parseObject(object : TypeParser<List<T>>() {}.type)

inline fun <reified T> JsonArray.parseSet(): Set<T> =
  parseObject(object : TypeParser<Set<T>>() {}.type)

inline fun <reified T> JsonArray.parseMutableSet(): MutableSet<T> =
  parseObject(object : TypeParser<Set<T>>() {}.type)

inline fun <reified T> JsonArray.parseList(context: JsonContext): List<T> =
  parseObject(context, object : TypeParser<List<T>>() {}.type)

inline fun <reified T> JsonArray.parseMutableList(context: JsonContext): MutableList<T> =
  parseObject(context, object : TypeParser<List<T>>() {}.type)

inline fun <reified T> JsonArray.parseSet(context: JsonContext): Set<T> =
  parseObject(context, object : TypeParser<Set<T>>() {}.type)

inline fun <reified T> JsonArray.parseMutableSet(context: JsonContext): MutableSet<T> =
  parseObject(context, object : TypeParser<Set<T>>() {}.type)
