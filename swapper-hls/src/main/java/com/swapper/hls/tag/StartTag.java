package com.swapper.hls.tag;

import com.swapper.hls.internal.enumerated.Whether;
import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaOrMasterPlaylist)
public final class StartTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-START";

  private Float mTimeOffset;
  private Whether mPrecise;

  public StartTag() {
    super(TAG_NAME);
  }
}
