package com.swapper.json.test.field;

import com.swapper.json.TestDataSource;
import com.swapper.json.TestSupportByArray;
import org.junit.jupiter.api.Test;

public class FieldNamingBeanTest {

  @Test
  public void test() {
    TestSupportByArray.test(TestDataSource.generate(FieldNamingBean.class));
  }
}
