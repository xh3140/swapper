package com.swapper.hls;

/**
 * https://www.jianshu.com/p/e97f6555a070
 * https://datatracker.ietf.org/doc/html/rfc8216
 */
public final class HLSToken {
  public static final int MASK_COLON = 0x1;
  public static final int MASK_COMMA = 0x2;
  public static final int MASK_EQUAL = 0x4;
  public static final int MASK_RETURN = 0x8;
  public static final int MASK_LINE_FEED = 0x10;
  public static final int MASK_TAG = 0x20;
  public static final int MASK_COMMENTING = 0x40;
  public static final int MASK_ATTRIBUTE = 0x80;
  public static final int MASK_DATE_TIME = 0x100;
  public static final int MASK_URI = 0x200;
  public static final int MASK_DEC_INTEGER = 0x400;
  public static final int MASK_HEX_INTEGER = 0x800;
  public static final int MASK_FLOAT = 0x1000;
  public static final int MASK_SIGNED_FLOAT = 0x2000;
  public static final int MASK_STRING = 0x4000;
  public static final int MASK_ENUM = 0x8000;
  public static final int MASK_RESOLUTION = 0x10000;
  public static final int MASK_END = 0x20000;

  public static final HLSToken COLON = new HLSToken(MASK_COLON, ":");
  public static final HLSToken COMMA = new HLSToken(MASK_COMMA, ",");
  public static final HLSToken EQUAL = new HLSToken(MASK_EQUAL, "=");
  public static final HLSToken RETURN = new HLSToken(MASK_RETURN, "\r");
  public static final HLSToken LINE_FEED = new HLSToken(MASK_LINE_FEED, "\n");
  public static final HLSToken END = new HLSToken(MASK_END, "\uffff");

  private final int _mask;
  private final String _value;

  public HLSToken(int mask, String value) {
    _mask = mask;
    _value = value;
  }

  public int mask() {
    return _mask;
  }

  public String value() {
    return _value;
  }

  @Override
  public String toString() {
    switch (_mask) {
      case MASK_COLON:
        return "COLON(:)";
      case MASK_COMMA:
        return "COMMA(,)";
      case MASK_EQUAL:
        return "EQUAL(=)";
      case MASK_RETURN:
        return "RETURN(\\r)";
      case MASK_LINE_FEED:
        return "LINE_FEED(\\n)";
      case MASK_TAG:
        return "TAG(" + _value + ")";
      case MASK_COMMENTING:
        return "COMMENTING(" + _value + ")";
      case MASK_ATTRIBUTE:
        return "ATTRIBUTE(" + _value + ")";
      case MASK_DATE_TIME:
        return "DATE_TIME(" + _value + ")";
      case MASK_URI:
        return "URI(" + _value + ")";
      case MASK_DEC_INTEGER:
        return "DEC_INTEGER(" + _value + ")";
      case MASK_HEX_INTEGER:
        return "HEX_INTEGER(" + _value + ")";
      case MASK_FLOAT:
        return "FLOAT(" + _value + ")";
      case MASK_SIGNED_FLOAT:
        return "SIGNED_FLOAT(" + _value + ")";
      case MASK_STRING:
        return "STRING(" + _value + ")";
      case MASK_ENUM:
        return "ENUM(" + _value + ")";
      case MASK_RESOLUTION:
        return "RESOLUTION(" + _value + ")";
    }
    throw new IllegalStateException("Unexpected HlsToken{mask=" + _mask + ",value=" + _value + "}.");
  }
}
