package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaPlaylist)
public final class StreamInfTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-STREAM-INF";

  private Integer mBandwidth;
  private Integer mAverageBandwidth;
  private String mCodecs;
  private String mResolution;
  private Float mFrameRate;
  private String mHDCPLevel;
  private String mAudio;
  private String mVideo;
  private String mSubtitles;
  private String mClosedCaptions;
  private String mUri;

  public StreamInfTag() {
    super(TAG_NAME);
  }

  @Override
  public String toString() {
    return "";
  }
}
