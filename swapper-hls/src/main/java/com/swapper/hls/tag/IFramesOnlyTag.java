package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaPlaylist)
public final class IFramesOnlyTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-I-FRAMES-ONLY";

  private IFramesOnlyTag() {
    super(TAG_NAME);
  }

  public static IFramesOnlyTag getInstance() {
    return SingletonHolder.INSTANCE;
  }

  public static final class SingletonHolder {
    private static final IFramesOnlyTag INSTANCE = new IFramesOnlyTag();
  }
}
