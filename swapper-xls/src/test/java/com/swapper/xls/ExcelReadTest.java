package com.swapper.xls;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.regex.Pattern;


public class ExcelReadTest {

  private static final Pattern PATTERN = Pattern.compile("^.*(4[abc]|5|6)类.*$");

  @Test
  public void test07Version() throws IOException {
    FileInputStream ins = new FileInputStream("E:\\xh3140\\mg.xlsx");
    Workbook workbookOut = new XSSFWorkbook();
    Sheet sheetOut = workbookOut.createSheet("回访名单");

    Workbook workbookIn = new XSSFWorkbook(ins);
    Sheet sheetIn = workbookIn.getSheetAt(0);
    if (sheetIn != null) {
      int lastRowNum = sheetIn.getLastRowNum();
      for (int rowNum = 0; rowNum < lastRowNum; rowNum++) {
        Row row = sheetIn.getRow(rowNum);
        if (row != null) {
          int lastCellNum = row.getLastCellNum();
          System.out.println(lastCellNum);
          for (int cellNum = 0; cellNum < lastCellNum; cellNum++) {
            Cell cell = row.getCell(cellNum);
            if (cell != null) {
              CellType cellType = cell.getCellType();
              String cellValue = cellToString(cell);
              if (PATTERN.matcher(cellValue).matches()) {
                int lastRowNumOut = sheetOut.getLastRowNum();
                Row rowOut = sheetOut.createRow(lastRowNumOut);
                copyRow(rowOut, row);
              }
              System.out.print(cellValue + " | ");
            }
          }
          System.out.println();
        }
      }
    }

    FileOutputStream fileOutputStream = new FileOutputStream("C:\\Users\\dell\\Desktop\\poi-gen2.xls");
    workbookOut.write(fileOutputStream);
    fileOutputStream.close();
    System.out.println("文件生成完毕");
  }

  private String cellToString(Cell cell) {
    if (cell == null) {
      return null;
    }
    switch (cell.getCellType()) {
      case STRING:
        return cell.getStringCellValue();
      case BOOLEAN:
        return String.valueOf(cell.getBooleanCellValue());
      case NUMERIC:
        if (DateUtil.isCellDateFormatted(cell)) {
          Date dateCellValue = cell.getDateCellValue();
          Instant instant = dateCellValue.toInstant();
          LocalDate localDate = LocalDate.from(instant);
          return localDate.toString();
        } else {
          return String.valueOf(cell.getNumericCellValue());
        }
      case _NONE:
      case BLANK:
      case FORMULA:
      case ERROR:
      default:
        return "";
    }
  }

  private void copyRow(Row row1, Row row2) {
    short lastCellNum = row2.getLastCellNum();
    for (int i = 0; i < lastCellNum; i++) {
      Cell cell = row1.createCell(i);
      cell.setCellValue(cellToString(row2.getCell(i)));
    }
  }
}
