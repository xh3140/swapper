/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.test.examples;

import com.swapper.json.JsonObject;
import com.swapper.json.test.bean.Example1Bean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Example1Test {
  private static final Example1Bean example1;
  private static final String example1_json;

  static {
    Example1Bean.Image.Thumbnail thumbnail = new Example1Bean.Image.Thumbnail();
    thumbnail.url = "http://www.example.com/image/481989943";
    thumbnail.height = 125;
    thumbnail.width = "100";
    Example1Bean.Image image = new Example1Bean.Image();
    image.width = 800;
    image.height = 600;
    image.title = "View from 15th Floor";
    image.thumbnail = thumbnail;
    image.ids = new int[]{116, 943, 234, 38793};
    example1 = new Example1Bean();
    example1.image = image;
    example1_json = "{\"Image\":{" +
            "\"Width\":800," +
            "\"Height\":600," +
            "\"Title\":\"View from 15th Floor\"," +
            "\"Thumbnail\":{" +
            "\"Url\":\"http://www.example.com/image/481989943\"," +
            "\"Height\":125," +
            "\"Width\":\"100\"" +
            "}," +
            "\"IDs\":[116,943,234,38793]" +
            "}" +
            "}\n";
  }

  @Test
  public void test_serialization() {
    String json = JsonObject.fromObject(example1).toJson();
    Assertions.assertNotNull(json);
  }

  @Test
  public void test_deserialization() {
    JsonObject jsonObject = JsonObject.fromJson(example1_json);
    Example1Bean ex1 = jsonObject.parseObject(Example1Bean.class);
    Assertions.assertEquals(ex1, example1);
  }
}
