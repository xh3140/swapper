package com.swapper.mock.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MockRange {
  char[] chars() default {};

  byte[] bytes() default {};

  short[] shorts() default {};

  int[] ints() default {};

  long[] longs() default {};

  float[] floats() default {};

  double[] doubles() default {};
}
