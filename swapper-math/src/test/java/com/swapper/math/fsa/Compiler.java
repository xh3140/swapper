package com.swapper.math.fsa;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Compiler {


  public static void main(String[] args) {
    int[] workload = {
      5, 29, 8, 24, 23, 21, 20, 14, 21, 17, 24, 20, 18, 18, 24, 17, 22, 35, 35, 14, 18, 18, 25, 50
    };
    sampling(workload, 25, 8);
  }


  public static void sampling(int[] workload, int sampleSize, int dayPart) {
    if (workload == null || workload.length == 0) {
      throw new IllegalArgumentException("No workload.");
    }
    int sum = Arrays.stream(workload).sum();
    if (sum < 1) {
      throw new IllegalArgumentException("No workload.");
    }
    if (sampleSize < 1) {
      throw new IllegalArgumentException("Sample size less than 1.");
    }
    if (sampleSize > sum) {
      throw new IllegalArgumentException("The sample size is larger than the population.");
    }
    if (dayPart < 1) {
      throw new IllegalArgumentException("Day part < 1.");
    }
    Map<Integer, Set<Integer>> selected = new TreeMap<>(Comparator.comparing(d -> d));
    Random random = new Random(System.currentTimeMillis());
    int count = 0;
    while (count < sampleSize) {
      int day = random.nextInt(workload.length) + 1;
      int dayNums = workload[day - 1];
      int dayMaxCount = Math.max(1, dayNums / dayPart);
      Set<Integer> set = selected.getOrDefault(day, new TreeSet<>(Comparator.comparing(i -> i)));
      if (set.size() < dayMaxCount) {
        int index = random.nextInt(dayNums) + 1;
        if (!set.contains(index)) {
          set.add(index);
          ++count;
        }
      }
      selected.put(day, set);
    }
    selected.forEach((day, position) -> System.out.printf("%d: %s\n", day, position));
  }


  @Test
  public void aa() throws ParseException {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date parse = simpleDateFormat.parse("2021-08-14");
    Date date = new Date(parse.getTime() + 90 * 24 * 3600 * 1000L);
    System.out.println(simpleDateFormat.format(date));
  }


  @Test
  void b() {
    Random random = new Random(System.currentTimeMillis());
    {
      int[] allIds = {
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
        21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
        31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
        41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
        51, 52, 53, 54, 55, 56, 57, 58, 59, 60
      };
      ArrayList<Integer> ids = new ArrayList<>();
      while (ids.size() < 12) {
        int i = random.nextInt(allIds.length);
        int id = allIds[i];
        if (!ids.contains(id)) {
          ids.add(id);
        }
      }
      System.out.println("法律法规 单选 = " + ids);
    }
    {
      int[] allIds = {
        61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
        71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
        81, 82, 83, 84
      };
      ArrayList<Integer> ids = new ArrayList<>();
      while (ids.size() < 3) {
        int i = random.nextInt(allIds.length);
        int id = allIds[i];
        if (!ids.contains(id)) {
          ids.add(id);
        }
      }
      System.out.println("法律法规 多选 = " + ids);
    }
    {
      int[] allIds = {
        1, 2, 4, 8, 9, 10,
        11, 12, 13, 14, 15, 17, 18, 19,
        21, 22, 23, 24, 25, 26, 27, 29,
        32, 33, 35, 36, 38, 39,
        41, 43, 45, 46, 47, 48, 49, 50,
        51, 57, 58, 59, 61, 62, 64, 65, 66, 68, 69, 70,
        71, 72, 73, 75, 76, 77, 79, 80, 81, 82, 83, 85,
        86, 87, 88, 89, 90
      };
      ArrayList<Integer> ids = new ArrayList<>();
      while (ids.size() < 12) {
        int i = random.nextInt(allIds.length);
        int id = allIds[i];
        if (!ids.contains(id)) {
          ids.add(id);
        }
      }
      System.out.println("防护基础 单选 = " + ids);
    }
    {
      int[] allIds = {
        3, 5, 6, 7, 16, 20, 200002, 28, 30, 31, 34, 37,
        40, 42, 44, 52, 53, 54, 55, 56, 60, 63, 67, 74,
        78, 84
      };
      ArrayList<Integer> ids = new ArrayList<>();
      while (ids.size() < 3) {
        int i = random.nextInt(allIds.length);
        int id = allIds[i];
        if (!ids.contains(id)) {
          ids.add(id);
        }
      }
      System.out.println("防护基础 多选 = " + ids);
    }
    {
      int[] allIds = {
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
        21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
        31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
        41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
        51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
        61
      };
      ArrayList<Integer> ids = new ArrayList<>();
      while (ids.size() < 12) {
        int i = random.nextInt(allIds.length);
        int id = allIds[i];
        if (!ids.contains(id)) {
          ids.add(id);
        }
      }
      System.out.println("医用Ⅲ类 单选 = " + ids);
    }
    {
      int[] allIds = {
        62, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76
      };
      ArrayList<Integer> ids = new ArrayList<>();
      while (ids.size() < 3) {
        int i = random.nextInt(allIds.length);
        int id = allIds[i];
        if (!ids.contains(id)) {
          ids.add(id);
        }
      }
      System.out.println("医用Ⅲ类 多选 = " + ids);
    }
    {
      int[] allIds = {
        1, 2, 3, 4, 5, 6, 8, 10,
        11, 13, 14, 15, 16, 18, 19, 20,
        21, 23, 24, 25, 26, 27, 28, 29, 30,
        31, 33, 35, 36, 37, 38, 40,
        42, 43, 45, 47, 48, 49, 50,
        52, 53, 55, 57, 59, 60,
        62, 63, 64, 65, 66, 67, 68,
        71, 72, 73, 74, 75, 77, 78, 79, 80, 82
      };
      ArrayList<Integer> ids = new ArrayList<>();
      while (ids.size() < 4) {
        int i = random.nextInt(allIds.length);
        int id = allIds[i];
        if (!ids.contains(id)) {
          ids.add(id);
        }
      }
      System.out.println("非医用Ⅲ类 单选 = " + ids);
    }
    {
      int[] allIds = {
        7, 9, 12, 17, 22, 32, 34, 39, 41, 44, 46, 51,
        54, 56, 58, 60, 61, 69, 70, 76, 81
      };
      ArrayList<Integer> ids = new ArrayList<>();
      while (ids.size() < 1) {
        int i = random.nextInt(allIds.length);
        int id = allIds[i];
        if (!ids.contains(id)) {
          ids.add(id);
        }
      }
      System.out.println("非医用Ⅲ类 多选 = " + ids);
    }
  }

}
