/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.supports;

import com.swapper.mock.*;
import com.swapper.mock.annotations.MockerSource;
import com.swapper.reflect.TypeHelper;
import com.swapper.reflect.creator.InstanceCreator;
import com.swapper.reflect.creator.InstanceCreatorFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public enum ReflectionMockerFactory implements MockerFactory {
  INSTANCE;

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Override
  public <T> Mocker<T> create(Type type) {
    Class<? super Object> rawType = TypeHelper.getRawType(type);
    InstanceCreator<? super Object> creator = InstanceCreatorFactory.create(type, rawType);
    return new ReflectionMocker(creator, rawType);
  }

  private static final class ReflectionMocker<T> implements Mocker<T> {
    private final InstanceCreator<T> creator;
    private final Class<? super T> type;
    private final List<Field> fields;

    public ReflectionMocker(InstanceCreator<T> creator, Class<? super T> type) {
      this.creator = Objects.requireNonNull(creator);
      this.type = Objects.requireNonNull(type);
      this.fields = Arrays.stream(type.getDeclaredFields())
        .filter((field) -> !Modifier.isStatic(field.getModifiers()))
        .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    @Override
    public T mock(MockContext context) {
      MockerSource source;
      source = type.getDeclaredAnnotation(MockerSource.class);
      if (source != null) {
        Class<? extends MockerProvider<?>> clazz = source.value();
        try {
          MockerProvider<?> provider = clazz.getDeclaredConstructor().newInstance();
          Mocker<?> mocker = provider.provideMocker();
          return (T) mocker.mock(context);
        } catch (InvocationTargetException | NoSuchMethodException
          | InstantiationException | IllegalAccessException e) {
          throw new IllegalStateException(e);
        }
      }
      T instance = creator.create();
      for (Field field : fields) {
        try {
          field.setAccessible(true);
          field.set(instance, context.mock(field.getGenericType()));
        } catch (IllegalAccessException e) {
          throw new IllegalStateException(e);
        }
      }
      return instance;
    }
  }
}
