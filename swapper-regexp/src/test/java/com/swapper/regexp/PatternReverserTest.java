/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.regexp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class PatternReverserTest {
  public static final class TestGenerateArgumentsProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      List<Arguments> arguments = new ArrayList<>();
      arguments.add(Arguments.of("正整数", "[1-9][0-9]*"));
      arguments.add(Arguments.of("负整数", "-[1-9][0-9]*"));
      arguments.add(Arguments.of("非负整数", "0|[1-9][0-9]*"));
      arguments.add(Arguments.of("非正整数", "0|-[1-9][0-9]*"));
      arguments.add(Arguments.of("整数", "0|-?[1-9][0-9]*"));
      arguments.add(Arguments.of("小数点后有3位的浮点数", "(0|-?[1-9][0-9]*)\\.[0-9]{3}"));
      arguments.add(Arguments.of("小数点后有4-6位的浮点数", "(0|-?[1-9][0-9]*)\\.[0-9]{4,6}"));
      arguments.add(Arguments.of("整数或浮点数", "(0|-?[1-9][0-9]*)(\\.[0-9]+)?((e|E)(\\+|-)?[0-9]+)?"));
      arguments.add(Arguments.of("RGB颜色", "#[0-9A-F]{6}"));
      arguments.add(Arguments.of("汉字", "[\\u4e00-\\u9fa5]+"));
      arguments.add(Arguments.of("邮箱", "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*"));
      arguments.add(Arguments.of("域名", "[0-9A-Za-z][-0-9A-Za-z]{0,62}(/.[0-9A-Za-z][-0-9A-Za-z]{0,62})+/.?"));
      arguments.add(Arguments.of("QQ号码", "[1-9]\\d{4,10}"));
      arguments.add(Arguments.of("手机号码", "(13[0-9]|14[0-9]|15[0-9]|16[0-9]|17[0-9]|18[0-9]|19[0-9])\\d{8}"));
      arguments.add(Arguments.of("中国大陆身份证号", "\\d{15}(\\d\\d[0-9xX])?"));
      arguments.add(Arguments.of("IPv4", "((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})(\\.((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})){3}"));
      arguments.add(Arguments.of("车牌", "[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-HJ-NP-Z][A-HJ-NP-Z0-9]{4,5}[A-HJ-NP-Z0-9挂学警港澳]"));
      arguments.add(Arguments.of("新能源车牌", "[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-HJ-NP-Z](((\\d{5}[A-HJK])|([A-HJK][A-HJ-NP-Z0-9][0-9]{4}))|[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳])"));
      arguments.add(Arguments.of("时间", "([0-1][0-9]|2[0-3])(:[0-5][0-9]){2}"));
      return arguments.stream();
    }
  }

  @ParameterizedTest
  @ArgumentsSource(TestGenerateArgumentsProvider.class)
  public void test(String title, String pattern) {
    Pattern p0 = Pattern.compile(pattern);
    PatternReverser rrg = new PatternReverser(pattern);
    Random random = new Random(System.currentTimeMillis());
    System.out.println(title + ": " + pattern);
    for (int i = 0; i < 5; ++i) {
      String s = rrg.reverse(random);
      System.out.println(s);
      Assertions.assertTrue(p0.matcher(s).matches());
    }
  }

  public static final class TestWeightArgumentsProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      List<Arguments> arguments = new ArrayList<>();
      arguments.add(Arguments.of("[aaaaabbbc]"));
      arguments.add(Arguments.of("a|b|c|d|e"));
      arguments.add(Arguments.of("0|\\d[0-9]?"));
      arguments.add(Arguments.of("0|[5-8]|[a-c]"));
      arguments.add(Arguments.of("abc|[bcd]|\\d"));
      arguments.add(Arguments.of("[a-c][d-e]|xyz"));
      return arguments.stream();
    }
  }

  @ParameterizedTest
  @ArgumentsSource(TestWeightArgumentsProvider.class)
  public void test_random_weight(String pattern) {
    Random random = new Random(System.currentTimeMillis());
    PatternReverser rr = new PatternReverser(pattern);
    Map<String, Integer> map = new HashMap<>();
    for (int i = 0; i < 100000; ++i) {
      String s = rr.reverse(random);
      map.put(s, map.getOrDefault(s, 0) + 1);
    }
    System.out.println(pattern + ": \n" + map);
  }

  @ParameterizedTest
  @ValueSource(strings = {
    ".{10,20}",
    "\\d{10,20}", "\\D{10,20}",
    "\\s{10,20}", "\\S{10,20}",
    "\\w{10,20}", "\\W{10,20}",
    "\\h{10,20}", "\\H{10,20}",
    "\\v{10,20}", "\\V{10,20}"
  })
  public void test_predefined_character_classes(String pattern) {
    PatternReverser reverser = new PatternReverser(pattern);
    System.out.printf("%s: %s\n", pattern, reverser.reverse());
  }

  @ParameterizedTest
  @ValueSource(strings = {
    "\\p{Lower}{10,20}", "\\p{IsLowercase}{10,20}",
    "\\p{Upper}{10,20}", "\\p{IsUppercase}{10,20}",
    "\\p{ASCII}{10,20}", "\\p{Alpha}{10,20}", "\\p{IsAlphabetic}{10,20}",
    "\\p{Digit}{10,20}", "\\p{IsDigit}{10,20}", "\\p{Alnum}{10,20}",
    "\\p{Punct}{10,20}", "\\p{IsPunctuation}{10,20}",
    "\\p{Graph}{10,20}", "\\p{Print}{10,20}", "\\p{Blank}{10,20}",
    "\\p{Cntrl}{10,20}", "\\p{XDigit}{10,20}",
    "\\p{Space}{10,20}", "\\p{IsWhite_Space}{10,20}"
  })
  public void test_posix_character_classes(String pattern) {
    PatternReverser reverser = new PatternReverser(pattern);
    System.out.printf("%s: %s\n", pattern, reverser.reverse());
  }

  @Test
  public void test_surrogate_character() {
    // U+1F000 - U+1F02F
    PatternReverser reverser = new PatternReverser("[\\x{1F000}-\\x{1F02F}]{30}");
    System.out.println(reverser.reverse());
    PatternReverser reverser2 = new PatternReverser("\uD83C\uDC17\uD83C\uDC2F\uD83C\uDC0E\uD83C\uDC16\uD83C\uDC04\uD83C\uDC28\uD83C\uDC2A\uD83C\uDC20\uD83C\uDC0F\uD83C\uDC28\uD83C\uDC05\uD83C\uDC20\uD83C\uDC1E\uD83C\uDC14\uD83C\uDC0C\uD83C\uDC02\uD83C\uDC1F\uD83C\uDC15\uD83C\uDC2F\uD83C\uDC2B\uD83C\uDC29\uD83C\uDC2C\uD83C\uDC00\uD83C\uDC18\uD83C\uDC00\uD83C\uDC1C\uD83C\uDC27\uD83C\uDC13\uD83C\uDC04\uD83C\uDC15");
    System.out.println(reverser2.reverse());
  }
}
