package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaSegment)
public final class DiscontinuityTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-DISCONTINUITY";

  public DiscontinuityTag() {
    super(TAG_NAME);
  }

  public static DiscontinuityTag getInstance() {
    return SingletonHolder.INSTANCE;
  }

  public static final class SingletonHolder {
    private static final DiscontinuityTag INSTANCE = new DiscontinuityTag();
  }
}
