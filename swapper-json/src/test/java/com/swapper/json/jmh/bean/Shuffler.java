package com.swapper.json.jmh.bean;

public enum Shuffler {
  Null, Each, Reverse
}
