package com.swapper.xls;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;


public class ExcelWriteTest {

  @Test
  public void test03Version() throws IOException {
    // 1.创建工作簿
    Workbook workbook = new HSSFWorkbook();
    // 2.创建工作表
    Sheet sheet = workbook.createSheet("sheet_1");
    // 3.创建行
    Row row = sheet.createRow(0);
    // 4. 创建单元格
    Cell cell = row.createCell(0);
    cell.setCellValue("单元格A1");
    // 5. 生成表
    FileOutputStream fileOutputStream = new FileOutputStream("C:\\Users\\xh3140\\Desktop\\poi-gen.xls");
    workbook.write(fileOutputStream);
    fileOutputStream.close();
    System.out.println("文件生成完毕");
  }
}
