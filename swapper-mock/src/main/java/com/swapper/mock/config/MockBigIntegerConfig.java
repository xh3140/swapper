/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.config;

import com.swapper.mock.annotations.MockBigInteger;
import com.swapper.mock.MockConfig;

import java.lang.annotation.Annotation;

public final class MockBigIntegerConfig extends MockConfig {
  private boolean unsigned = true;
  private int bits = Long.SIZE + 1;

  public MockBigIntegerConfig() {
  }

  public MockBigIntegerConfig(boolean unsigned, int bits) {
    setUnsigned(unsigned);
    setBits(bits);
  }

  @Override
  public void withAnnotationConfig(Annotation annotation) {
    if (annotation instanceof MockBigInteger) {
      MockBigInteger config = (MockBigInteger) annotation;
      setUnsigned(config.unsigned());
      setBits(config.bits());
    }
  }

  public boolean isUnsigned() {
    return unsigned;
  }

  public int getBits() {
    return bits;
  }

  public void setUnsigned(boolean unsigned) {
    this.unsigned = unsigned;
  }

  public void setBits(int bits) {
    if (bits < 1) {
      throw new IllegalArgumentException("The bits of an integer must be greater than 0.");
    }
    this.bits = bits;
  }
}
