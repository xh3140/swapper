/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.regexp;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CharacterNodeTest {
  @RepeatedTest(10)
  public void test_method_codePoint() {
    PatternReverser context = new PatternReverser("context");
    Random random = new Random(System.currentTimeMillis());
    int codePoint = random.nextInt(Character.MAX_CODE_POINT + 1);
    assertEquals(codePoint, new CharacterNode(context, codePoint).codePoint());
  }

  @RepeatedTest(10)
  @DisplayName("The weight of fixed unit is always 1.")
  public void test_method_weight() {
    PatternReverser context = new PatternReverser("context");
    Random random = new Random(System.currentTimeMillis());
    int codePoint = random.nextInt(Character.MAX_CODE_POINT + 1);
    assertEquals(1, new CharacterNode(context, codePoint).weight());
  }

  @RepeatedTest(10)
  @DisplayName("The fixed unit random is fixed.")
  public void test_method_random() {
    PatternReverser context = new PatternReverser("context");
    Random random = new Random(System.currentTimeMillis());
    int codePoint = random.nextInt(Character.MAX_CODE_POINT + 1);
    CharacterNode unit = new CharacterNode(context, codePoint);
    assertEquals(CharacterHelper.codePointToString(codePoint), unit.random(random));
  }
}
