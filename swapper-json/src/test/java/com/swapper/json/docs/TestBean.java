/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.docs;

import com.swapper.mock.bean.QuickDirtyBean;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.DayOfWeek;
import java.util.Date;

/**
 * Test basic data types.
 * For code brevity, use the public modifier instead of the setter and getter.
 */
public final class TestBean extends QuickDirtyBean {
  public Object mNull = null;
  public Boolean mBooleanTrue = true;
  public Boolean mBooleanFalse = false;
  public Byte mByte = Byte.MAX_VALUE;
  public Short mShort = Short.MAX_VALUE;
  public Integer mInteger = Integer.MAX_VALUE;
  public Long mLong = Long.MAX_VALUE;
  public Float mFloat = Float.MAX_VALUE;
  public Double mDouble = Double.MAX_VALUE;
  public BigInteger mBigInteger = new BigInteger("6b7404b3f87e49376fac3b6a43dbe3be", 16);
  public BigDecimal mBigDecimal = new BigDecimal("3.14159265358979323846264338327950288419716939937510");
  public String mStringEmpty = "";
  public String mStringNormal = "Hello,world!";
  public String mStringEscape = "\\ \b \f \n \r \t";
  public String mStringHexText = "\u4f60\u597d\uff0c\u4e16\u754c\uff01";
  public Character mCharacter = '#';
  public Date mDate = new Date(1607703132000L);
  public DayOfWeek mEnum = DayOfWeek.MONDAY;
}
