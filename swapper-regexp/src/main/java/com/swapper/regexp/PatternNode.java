/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.regexp;

import java.util.Objects;
import java.util.Random;

/**
 * The regular expression node.
 *
 * @see ConcatNode "ab"
 * @see EitherNode "a|b"
 * @see QuantifierNode "a+"
 * @see CharacterClassesNode "[a-z]"
 */
abstract class PatternNode implements Weighty {
  protected final PatternReverser context;

  /**
   * Protected constructor.
   *
   * @param context the generator context.
   * @throws NullPointerException if {@code context} is null.
   */
  protected PatternNode(PatternReverser context) {
    this.context = Objects.requireNonNull(context);
  }

  /**
   * Generates a random string that matches the node.
   *
   * @param random the random generator.
   * @return a random string that matches the node.
   */
  public abstract String random(Random random);
}
