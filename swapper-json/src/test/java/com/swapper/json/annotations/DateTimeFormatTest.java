package com.swapper.json.annotations;

import com.swapper.json.JsonObject;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

class DateTimeFormatTest {

  private static class Data {
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private final Date date1 = new Date();

    @DateTimeFormat(pattern = "yyyy年MM月dd日 HH:mm:ss")
    private final LocalDateTime date2 = LocalDateTime.now();

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Data data = (Data) o;
      return Objects.equals(date1, data.date1) && Objects.equals(date2, data.date2);
    }

    @Override
    public int hashCode() {
      return Objects.hash(date1, date2);
    }
  }

  @Test
  public void test() {
    Data data1 = new Data();
    JsonObject jsonObject1 = JsonObject.fromObject(data1);
    String json = jsonObject1.toJson();

    System.out.println(json);

    JsonObject jsonObject2 = JsonObject.fromJson(json);
    Data data2 = jsonObject2.parseObject(Data.class);

    System.out.println(JsonObject.fromObject(data2).toJson());

  }
}
