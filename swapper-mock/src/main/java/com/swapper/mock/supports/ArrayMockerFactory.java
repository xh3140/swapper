/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.supports;

import com.swapper.mock.MockContext;
import com.swapper.mock.Mocker;
import com.swapper.mock.MockerFactory;
import com.swapper.mock.config.MockLengthConfig;
import com.swapper.reflect.TypeHelper;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.Objects;

public enum ArrayMockerFactory implements MockerFactory {
  INSTANCE;

  @SuppressWarnings("unchecked")
  @Override
  public <T> Mocker<T> create(Type type) {
    Type componentType = TypeHelper.getArrayComponentType(type);
    if (componentType != null) {
      return (Mocker<T>) new ArrayMocker(componentType);
    }
    return null;
  }

  private static class ArrayMocker implements Mocker<Object> {
    private final Type componentType;
    private final Class<?> componentRawType;

    public ArrayMocker(Type componentType) {
      this.componentType = Objects.requireNonNull(componentType);
      this.componentRawType = TypeHelper.getRawType(componentType);
    }

    @Override
    public Object mock(MockContext context) {
      MockLengthConfig config = context.getConfig(MockLengthConfig.class);
      int length = config.random(context.random());
      Object array = Array.newInstance(componentRawType, length);
      for (int i = 0; i < length; ++i) {
        Array.set(array, i, context.mock(componentType));
      }
      return array;
    }
  }
}
