/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.supports;

import com.swapper.mock.MockContext;
import com.swapper.mock.Mocker;
import com.swapper.mock.MockerFactory;
import com.swapper.mock.config.MockBigIntegerConfig;

import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.Random;

public enum BigIntegerMockerFactory implements MockerFactory {
  INSTANCE;

  @SuppressWarnings("unchecked")
  @Override
  public <T> Mocker<T> create(Type type) {
    return type == BigInteger.class ? (Mocker<T>) BigIntegerMocker.INSTANCE : null;
  }

   enum BigIntegerMocker implements Mocker<BigInteger> {
    INSTANCE;

    @Override
    public BigInteger mock(MockContext context) {
      Random random = context.random();
      MockBigIntegerConfig config = context.getConfig(MockBigIntegerConfig.class);
      BigInteger integer = new BigInteger(config.getBits(), random);
      if (!config.isUnsigned() && random.nextBoolean()) {
        return integer.negate();
      }
      return integer;
    }
  }

}
