/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.config;

import com.swapper.mock.MockConfig;
import com.swapper.mock.annotations.MockString;

import java.lang.annotation.Annotation;
import java.util.Objects;

public final class MockStringConfig extends MockConfig {
  private String pattern = "\\w+";
  private int hugeQuantifierLimit = 8;
  private boolean negatedMetaWithAsciiDisplay = true;

  public MockStringConfig() {
  }

  public MockStringConfig(String pattern) {
    this(pattern, 8, true);
  }

  public MockStringConfig(String pattern, int hugeQuantifierLimit, boolean negatedMetaWithAsciiDisplay) {
    setPattern(pattern);
    setHugeQuantifierLimit(hugeQuantifierLimit);
    setNegatedMetaWithAsciiDisplay(negatedMetaWithAsciiDisplay);
  }

  @Override
  public void withAnnotationConfig(Annotation annotation) {
    if (annotation instanceof MockString) {
      MockString config = (MockString) annotation;
      setPattern(config.pattern());
    }
  }

  public String getPattern() {
    return pattern;
  }

  public void setPattern(String pattern) {
    this.pattern = Objects.requireNonNull(pattern);
  }

  public int getHugeQuantifierLimit() {
    return hugeQuantifierLimit;
  }

  public void setHugeQuantifierLimit(int hugeQuantifierLimit) {
    if (hugeQuantifierLimit < 0) {
      throw new IllegalArgumentException("Quantifier times must be a non-negative number!");
    }
    this.hugeQuantifierLimit = hugeQuantifierLimit;
  }

  public boolean isNegatedMetaWithAsciiDisplay() {
    return negatedMetaWithAsciiDisplay;
  }

  public void setNegatedMetaWithAsciiDisplay(boolean negatedMetaWithAsciiDisplay) {
    this.negatedMetaWithAsciiDisplay = negatedMetaWithAsciiDisplay;
  }
}
