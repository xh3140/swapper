package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaPlaylist)
public class TargetDurationTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-TARGETDURATION";

  private final int mDuration;

  public TargetDurationTag(int duration) {
    super(TAG_NAME);
    mDuration = duration;
  }

  public int duration() {
    return mDuration;
  }
}
