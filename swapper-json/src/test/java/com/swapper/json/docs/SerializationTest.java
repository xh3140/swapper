/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.docs;

import com.swapper.json.JsonObject;
import com.swapper.json.JsonValue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SerializationTest {
  @Test
  public void testAutoSerialization() {
    TestBean bean = new TestBean();
    // Auto build the data structure and fill in the parameters
    JsonObject jsonObject = JsonObject.fromObject(bean);
    // JSON serialization
    String json = jsonObject.toJson();
    Assertions.assertNotNull(json);
  }

  @Test
  public void testManuallySerialization() {
    TestBean bean = new TestBean();
    // Manually build the data structure and fill in the parameters
    JsonObject jsonObject = new JsonObject();
    jsonObject.put("mNull", JsonValue.valueOf(bean.mNull));
    jsonObject.put("mBooleanTrue", JsonValue.valueOf(bean.mBooleanTrue));
    jsonObject.put("mBooleanFalse", JsonValue.valueOf(bean.mBooleanFalse));
    jsonObject.put("mByte", JsonValue.valueOf(bean.mByte));
    jsonObject.put("mShort", JsonValue.valueOf(bean.mShort));
    jsonObject.put("mInteger", JsonValue.valueOf(bean.mInteger));
    jsonObject.put("mLong", JsonValue.valueOf(bean.mLong));
    jsonObject.put("mFloat", JsonValue.valueOf(bean.mFloat));
    jsonObject.put("mDouble", JsonValue.valueOf(bean.mDouble));
    jsonObject.put("mBigInteger", JsonValue.valueOf(bean.mBigInteger));
    jsonObject.put("mStringEmpty", JsonValue.valueOf(bean.mStringEmpty));
    jsonObject.put("mStringNormal", JsonValue.valueOf(bean.mStringNormal));
    jsonObject.put("mStringEscape", JsonValue.valueOf(bean.mStringEscape));
    jsonObject.put("mStringHexText", JsonValue.valueOf(bean.mStringHexText));
    jsonObject.put("mCharacter", JsonValue.valueOf(bean.mCharacter));
    jsonObject.put("mDate", JsonValue.valueOf(bean.mDate));
    jsonObject.put("mEnum", JsonValue.valueOf(bean.mEnum));
    // JSON serialization
    String json = jsonObject.toJson();
    Assertions.assertNotNull(json);
  }
}
