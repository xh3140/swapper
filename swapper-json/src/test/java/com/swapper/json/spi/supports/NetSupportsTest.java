/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.spi.supports;

import com.swapper.json.TestSupportByArray;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class NetSupportsTest {
  @Test
  public void test_support_net_uri() throws URISyntaxException {
    TestSupportByArray.test(new URI("geo:38.899533,-77.036476"));
  }

  @Test
  public void test_support_net_url() throws MalformedURLException {
    TestSupportByArray.test(new URL("https://www.ionos.com/digitalguide/websites/web-development/uniform-resource-identifier-uri/"));
  }
}
