package com.swapper.mock.supports;

import com.swapper.mock.MockContext;
import com.swapper.mock.Mocker;
import com.swapper.mock.MockerFactory;
import com.swapper.mock.config.MockByteConfig;
import com.swapper.mock.config.MockCharacterConfig;

import java.lang.reflect.Type;

public enum CharacterMockerFactory implements MockerFactory {
  INSTANCE;

  @SuppressWarnings("unchecked")
  @Override
  public <T> Mocker<T> create(Type type) {
    return type == char.class || type == Character.class ? (Mocker<T>) CharacterMocker.INSTANCE : null;
  }

  enum CharacterMocker implements Mocker<Character> {
    INSTANCE;

    @Override
    public Character mock(MockContext context) {
      return context.getConfig(MockCharacterConfig.class).random(context.random());
    }
  }
}
