/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DateTimeFormat {
  /**
   * The date time format pattern.
   * eg: "yyyy-MM-dd HH:mm:ss".
   *
   * @see java.text.DateFormat
   * @see java.time.format.DateTimeFormatter
   */
  String pattern();

  /**
   * The date time format locale.
   * format: language[_country][_variant].
   *
   * @see java.util.Locale
   */
  String locale() default "";

  /**
   * The date time format timezone id.
   * eg: "Asia/Shanghai".
   *
   * @see java.util.TimeZone
   * @see java.time.ZoneId
   */
  String timezone() default "";
}
