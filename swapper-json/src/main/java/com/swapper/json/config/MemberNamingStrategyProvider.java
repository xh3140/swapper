package com.swapper.json.config;

public interface MemberNamingStrategyProvider {
  MemberNamingStrategy provideStrategy();
}
