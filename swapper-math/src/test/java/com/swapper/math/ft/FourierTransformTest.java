package com.swapper.math.ft;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.Random;

public class FourierTransformTest {
  @Test()
  public void t() {
    int L = 8;
    double[] X0 = new double[L];
    double[] Y0 = new double[L];
    Random random = new Random(System.currentTimeMillis());
    for (int I = 0; I < L; ++I) {
      X0[I] = 1D + random.nextInt(99);
      Y0[I] = 1D + random.nextInt(99);
    }
    double[] X1 = X0.clone();
    double[] Y1 = Y0.clone();
    Instant t1 = Instant.now();
    FourierTransform.fft(X1, Y1);
    Instant t2 = Instant.now();
    System.out.println(" FFT  :" + FourierTransform.toArrayString(X1, Y1));
    Instant t3 = Instant.now();
    FourierTransform.ifft(X1, Y1);
    Instant t4 = Instant.now();
    System.out.println("   RAW:" + FourierTransform.toArrayString(X0, Y0));
    System.out.println("IFFT  :" + FourierTransform.toArrayString(X1, Y1));
    System.out.println(" FFT-T:" + Duration.between(t1, t2).toNanos());
    System.out.println("IFFT-T:" + Duration.between(t3, t4).toNanos());
  }
}
