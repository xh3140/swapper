package com.swapper.xml;

public final class XmlTexts {
  private XmlTexts() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  public static final char END = 0xffff;

  public static final char WHITESPACE_SPACE = 0x20;
  public static final char WHITESPACE_HOR_TAB = 0x9;
  public static final char WHITESPACE_LINE_FEED = 0xa;
  public static final char WHITESPACE_CARRIAGE_RETURN = 0xd;

  public static final char DIGIT_0 = 0x30;
  public static final char DIGIT_1 = 0x31;
  public static final char DIGIT_2 = 0x32;
  public static final char DIGIT_3 = 0x33;
  public static final char DIGIT_4 = 0x34;
  public static final char DIGIT_5 = 0x35;
  public static final char DIGIT_6 = 0x36;
  public static final char DIGIT_7 = 0x37;
  public static final char DIGIT_8 = 0x38;
  public static final char DIGIT_9 = 0x39;

  public static final char LETTER_H_A = 0x41;
  public static final char LETTER_H_B = 0x42;
  public static final char LETTER_H_C = 0x43;
  public static final char LETTER_H_D = 0x44;
  public static final char LETTER_H_E = 0x45;
  public static final char LETTER_H_F = 0x46;
  public static final char LETTER_H_G = 0x47;
  public static final char LETTER_H_H = 0x48;
  public static final char LETTER_H_I = 0x49;
  public static final char LETTER_H_J = 0x4a;
  public static final char LETTER_H_K = 0x4b;
  public static final char LETTER_H_L = 0x4c;
  public static final char LETTER_H_M = 0x4d;
  public static final char LETTER_H_N = 0x4e;
  public static final char LETTER_H_O = 0x4f;
  public static final char LETTER_H_P = 0x50;
  public static final char LETTER_H_Q = 0x51;
  public static final char LETTER_H_R = 0x52;
  public static final char LETTER_H_S = 0x53;
  public static final char LETTER_H_T = 0x54;
  public static final char LETTER_H_U = 0x55;
  public static final char LETTER_H_V = 0x56;
  public static final char LETTER_H_W = 0x57;
  public static final char LETTER_H_X = 0x58;
  public static final char LETTER_H_Y = 0x59;
  public static final char LETTER_H_Z = 0x5a;
  public static final char LETTER_L_A = 0x61;
  public static final char LETTER_L_B = 0x62;
  public static final char LETTER_L_C = 0x63;
  public static final char LETTER_L_D = 0x64;
  public static final char LETTER_L_E = 0x65;
  public static final char LETTER_L_F = 0x66;
  public static final char LETTER_L_G = 0x67;
  public static final char LETTER_L_H = 0x68;
  public static final char LETTER_L_I = 0x69;
  public static final char LETTER_L_J = 0x6a;
  public static final char LETTER_L_K = 0x6b;
  public static final char LETTER_L_L = 0x6c;
  public static final char LETTER_L_M = 0x6d;
  public static final char LETTER_L_N = 0x6e;
  public static final char LETTER_L_O = 0x6f;
  public static final char LETTER_L_P = 0x70;
  public static final char LETTER_L_Q = 0x71;
  public static final char LETTER_L_R = 0x72;
  public static final char LETTER_L_S = 0x73;
  public static final char LETTER_L_T = 0x74;
  public static final char LETTER_L_U = 0x75;
  public static final char LETTER_L_V = 0x76;
  public static final char LETTER_L_W = 0x77;
  public static final char LETTER_L_X = 0x78;
  public static final char LETTER_L_Y = 0x79;
  public static final char LETTER_L_Z = 0x7a;

}
