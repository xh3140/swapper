package com.swapper.markdown.elements;

import com.swapper.markdown.MarkdownElement;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class MarkdownOrderedList extends MarkdownElement {
  private final List<String> _items = new ArrayList<>();

  @Override
  public void writeToHtml(Writer writer) throws IOException {
    writer.write("<ol>");
    writer.write(System.lineSeparator());
    for (String item : _items) {
      writer.write("<li>");
      writer.write(item);
      writer.write("</li>");
      writer.write(System.lineSeparator());
    }
    writer.write("</ol>");
    writer.write(System.lineSeparator());
  }
}
