package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaSegment, since = 4)
public final class ByteRangeTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-BYTERANGE";
  private final Integer mRange;
  private final Integer mOffset;

  public ByteRangeTag(Integer range) {
    this(range, null);
  }

  public ByteRangeTag(Integer range, Integer offset) {
    super(TAG_NAME);
    if (range == null) {
      throw new NullPointerException("HLS tag(" + TAG_NAME + ") range is null.");
    }
    if (range < 1) {
      throw new IllegalArgumentException("HLS tag(" + TAG_NAME + ") range < 1.");
    }
    if (offset != null && offset < 0) {
      throw new IllegalArgumentException("HLS tag(" + TAG_NAME + ") offset < 0.");
    }
    mRange = range;
    mOffset = offset;
  }

  public Integer range() {
    return mRange;
  }

  public Integer offset() {
    return mOffset;
  }

  @Override
  public String toString() {
    if (mOffset == null) {
      return String.format("%s:%d\n", mName, mRange);
    } else {
      return String.format("%s:%d@%d\n", mName, mRange, mOffset);
    }
  }
}
