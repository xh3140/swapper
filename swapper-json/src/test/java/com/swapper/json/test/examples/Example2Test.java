/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.test.examples;

import com.swapper.json.JsonArray;
import com.swapper.json.test.bean.Example2Bean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Example2Test {
  private static final Example2Bean[] example2s;
  private static final String example2s_json;

  static {
    Example2Bean ex_at0 = new Example2Bean();
    ex_at0.precision = "zip";
    ex_at0.latitude = 37.7668f;
    ex_at0.longitude = -122.3959f;
    ex_at0.address = "";
    ex_at0.city = "SAN FRANCISCO";
    ex_at0.state = "CA";
    ex_at0.zip = "94107";
    ex_at0.country = "US";
    Example2Bean ex_at1 = new Example2Bean();
    ex_at1.precision = "zip";
    ex_at1.latitude = 37.371991f;
    ex_at1.longitude = -122.026020f;
    ex_at1.address = "";
    ex_at1.city = "SUNNYVALE";
    ex_at1.state = "CA";
    ex_at1.zip = "94085";
    ex_at1.country = "US";
    example2s = new Example2Bean[]{ex_at0, ex_at1};
    example2s_json = "[\n" +
      "  {\n" +
      "    \"precision\": \"zip\",\n" +
      "    \"Latitude\": 37.7668,\n" +
      "    \"Longitude\": -122.3959,\n" +
      "    \"Address\": \"\",\n" +
      "    \"City\": \"SAN FRANCISCO\",\n" +
      "    \"State\": \"CA\",\n" +
      "    \"Zip\": \"94107\",\n" +
      "    \"Country\": \"US\"\n" +
      "  },\n" +
      "  {\n" +
      "    \"precision\": \"zip\",\n" +
      "    \"Latitude\": 37.37199,\n" +
      "    \"Longitude\": -122.02602,\n" +
      "    \"Address\": \"\",\n" +
      "    \"City\": \"SUNNYVALE\",\n" +
      "    \"State\": \"CA\",\n" +
      "    \"Zip\": \"94085\",\n" +
      "    \"Country\": \"US\"\n" +
      "  }\n" +
      "]";
  }

  @Test
  public void test_serialization() {
    String json = JsonArray.fromObject(example2s).toJson();
    Assertions.assertNotNull(json);
  }

  @Test
  public void test_deserialization() {
    JsonArray jsonArray = JsonArray.fromJson(example2s_json);
    Example2Bean[] ex2s = jsonArray.parseObject(Example2Bean[].class);
    Assertions.assertArrayEquals(example2s, ex2s);
  }
}
