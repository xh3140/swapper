package com.swapper.json.config;

import java.lang.reflect.Field;

public interface MemberNamingStrategy {
  String translate(Field member);
}
