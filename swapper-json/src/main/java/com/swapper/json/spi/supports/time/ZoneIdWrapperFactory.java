package com.swapper.json.spi.supports.time;

import com.swapper.json.*;

import java.lang.reflect.Type;
import java.time.ZoneId;

public enum ZoneIdWrapperFactory implements JsonWrapperFactory {
  INSTANCE;

  @SuppressWarnings("unchecked")
  @Override
  public <T> JsonWrapper<T> create(JsonContext context, Type type) {
    if (type instanceof Class<?> && ZoneId.class.isAssignableFrom((Class<?>) type)) {
      return (JsonWrapper<T>) Wrapper.INSTANCE;
    }
    return null;
  }

  public enum Wrapper implements JsonWrapper<ZoneId> {
    INSTANCE;

    @Override
    public JsonValue wrap(ZoneId value) {
      return value == null ? JsonValue.NULL : JsonString.valueOf(value.getId());
    }

    @Override
    public ZoneId unwrap(JsonValue value) {
      return JsonValue.NULL.equals(value) ? null : ZoneId.of(value.toString());
    }
  }
}
