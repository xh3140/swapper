package com.swapper.hls.internal.enumerated;

public enum MediaType {
  AUDIO, VIDEO, SUBTITLES, CLOSED_CAPTIONS
}
