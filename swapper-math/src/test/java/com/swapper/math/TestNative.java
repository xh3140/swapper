package com.swapper.math;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * javac TestNative.java -h .
 * gcc -m64 -I "D:\Program Files\Java\jdk-11.0.12\include" -I "D:\Program Files\Java\jdk-11.0.12\include\win32" -I "E:\Coder\QtProjects\crypto" -shared -o hello.dll E:\Coder\QtProjects\crypto\com_swapper_math_TestNative.c E:\Coder\QtProjects\crypto\sm3.c
 */
public class TestNative {
  public native void helloFormCpp();

  public native int addByCpp(int a, int b);

  public native byte[] sm3(byte[] message);

  static {
    System.load("E:\\Coder\\JavaProjects\\swapper\\swapper-math\\src\\test\\java\\com\\swapper\\math\\hello.dll");
  }

  public static void main(String[] args) {
    TestNative testNative = new TestNative();
    testNative.helloFormCpp();
    System.out.println(testNative.addByCpp(2, 3));

    byte[] bytes1 = "Hello World".getBytes();
    byte[] bytes = testNative.sm3(bytes1);
    System.out.println(new String(bytes));
    for (int i = 0; i < 32; i++) {
      System.out.printf("%02x", bytes[i]);
    }

  }
}
