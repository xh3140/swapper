package com.swapper.hls.internal.info;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface Attribute {
  Necessity necessity();

  int since() default 1;

  enum Necessity {Required, Optional, Condition}
}
