/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.supports;

import com.swapper.mock.MockContext;
import com.swapper.mock.Mocker;
import com.swapper.mock.MockerFactory;
import com.swapper.mock.config.MockSizeConfig;
import com.swapper.reflect.TypeHelper;
import com.swapper.reflect.creator.InstanceCreator;
import com.swapper.reflect.creator.InstanceCreatorFactory;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Objects;

public enum CollectionMockerFactory implements MockerFactory {
  INSTANCE;

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Override
  public <T> Mocker<T> create(Type type) {
    Class<? super Object> rawType = TypeHelper.getRawType(type);
    Type elementType = TypeHelper.getCollectionElementType(type, rawType);
    if (elementType != null) {
      InstanceCreator<? super Object> creator = InstanceCreatorFactory.create(type, rawType);
      return new CollectionMocker(creator, elementType);
    }
    return null;
  }

  private static final class CollectionMocker<T extends Collection<?>> implements Mocker<T> {
    private final InstanceCreator<T> creator;
    private final Type elementType;

    public CollectionMocker(InstanceCreator<T> creator, Type elementType) {
      this.creator = Objects.requireNonNull(creator);
      this.elementType = Objects.requireNonNull(elementType);
    }

    @Override
    public T mock(MockContext context) {
      MockSizeConfig config = context.getConfig(MockSizeConfig.class);
      int size = config.random(context.random());
      T collection = creator.create();
      while (collection.size() < size) {
        collection.add(context.mock(elementType));
      }
      return collection;
    }
  }
}
