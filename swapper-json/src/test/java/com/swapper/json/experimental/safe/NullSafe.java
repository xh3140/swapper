/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.experimental.safe;

public final class NullSafe {
  private NullSafe() {
    throw new UnsupportedOperationException(getClass().getName());
  }

  public static <T> T nullSafe(T instance) {
    if (instance == null) {
      throw new NullPointerException();
    }
    return instance;
  }

  public static <T> T nullSafe(T instance, String message) {
    if (instance == null) {
      throw new NullPointerException(message);
    }
    return instance;
  }

  public static <T> T nullSafe(T instance, T defaultValue) {
    if (instance == null) {
      if (defaultValue == null) {
        throw new NullPointerException("defaultValue is null.");
      }
      return defaultValue;
    }
    return instance;
  }
}
