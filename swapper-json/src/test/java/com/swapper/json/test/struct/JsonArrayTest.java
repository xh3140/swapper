/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.test.struct;

import com.swapper.json.JsonArray;
import com.swapper.json.JsonValue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JsonArrayTest {
  @Test
  public void test_method_size() {
    List<JsonValue> values = new ArrayList<>();
    values.add(JsonValue.NULL);
    values.add(JsonValue.TRUE);
    values.add(JsonValue.FALSE);

    JsonArray jsonArray = new JsonArray();
    Assertions.assertTrue(jsonArray.appends(values));
    Assertions.assertEquals(3, jsonArray.size());

    Assertions.assertEquals(JsonValue.NULL, jsonArray.remove(0));
    Assertions.assertEquals(JsonValue.TRUE, jsonArray.remove(0));
    Assertions.assertEquals(JsonValue.FALSE, jsonArray.remove(0));
    Assertions.assertEquals(0, jsonArray.size());

    Assertions.assertTrue(jsonArray.appends(values));
    Assertions.assertEquals(3, jsonArray.size());
  }

  @Test
  public void test_method_isEmpty() {
    List<JsonValue> values = new ArrayList<>();
    values.add(JsonValue.NULL);
    values.add(JsonValue.TRUE);
    values.add(JsonValue.FALSE);

    JsonArray jsonArray = new JsonArray();

    Assertions.assertTrue(jsonArray.appends(values));
    Assertions.assertFalse(jsonArray.isEmpty());

    Assertions.assertEquals(JsonValue.NULL, jsonArray.remove(0));
    Assertions.assertEquals(JsonValue.TRUE, jsonArray.remove(0));
    Assertions.assertEquals(JsonValue.FALSE, jsonArray.remove(0));
    Assertions.assertTrue(jsonArray.isEmpty());

    Assertions.assertTrue(jsonArray.appends(values));
    Assertions.assertFalse(jsonArray.isEmpty());
  }

  @Test
  public void test_method_contains() {
    List<JsonValue> values = new ArrayList<>();
    values.add(JsonValue.NULL);
    values.add(JsonValue.TRUE);
    values.add(JsonValue.FALSE);

    JsonArray jsonArray = new JsonArray();

    Assertions.assertTrue(jsonArray.appends(values));

    Assertions.assertTrue(jsonArray.contains(JsonValue.NULL));
    Assertions.assertTrue(jsonArray.contains(JsonValue.TRUE));
    Assertions.assertTrue(jsonArray.contains(JsonValue.FALSE));
  }

  @Test
  public void test_method_append_remove() {
    JsonArray jsonArray = new JsonArray();

    jsonArray.append(JsonValue.NULL);
    jsonArray.append(JsonValue.TRUE);
    jsonArray.append(JsonValue.FALSE);

    Assertions.assertEquals(3, jsonArray.size());

    Assertions.assertTrue(jsonArray.remove(JsonValue.NULL));
    Assertions.assertTrue(jsonArray.remove(JsonValue.TRUE));
    Assertions.assertTrue(jsonArray.remove(JsonValue.FALSE));

    Assertions.assertEquals(0, jsonArray.size());
  }

  @Test
  public void test_method_iterator() {
    List<JsonValue> values = new ArrayList<>();
    values.add(JsonValue.NULL);
    values.add(JsonValue.TRUE);
    values.add(JsonValue.FALSE);
    JsonArray jsonArray = new JsonArray();
    jsonArray.appends(values);
    Iterator<JsonValue> iterator = jsonArray.iterator();
    Assertions.assertNotNull(iterator);
    Assertions.assertEquals(JsonValue.NULL, iterator.next());
    Assertions.assertEquals(JsonValue.TRUE, iterator.next());
    Assertions.assertEquals(JsonValue.FALSE, iterator.next());
    Assertions.assertFalse(iterator.hasNext());
  }

  @Test
  public void test_method_toArray() {
    List<JsonValue> values = new ArrayList<>();
    values.add(JsonValue.NULL);
    values.add(JsonValue.TRUE);
    values.add(JsonValue.FALSE);
    JsonArray jsonArray = new JsonArray();
    jsonArray.appends(values);
    JsonValue[] array1 = jsonArray.toArray();
    JsonValue[] array2 = {JsonValue.NULL, JsonValue.TRUE, JsonValue.FALSE};
    Assertions.assertNotNull(array1);
    Assertions.assertArrayEquals(array1, array2);
  }
}
