/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.spi.providers;

import com.swapper.json.JsonWrapperFactory;
import com.swapper.json.spi.JsonSupportsProvider;
import com.swapper.json.spi.supports.atomic.*;
import com.swapper.json.spi.supports.sql.SqlDateWrapperFactory;
import com.swapper.json.spi.supports.sql.SqlTimeWrapperFactory;
import com.swapper.json.spi.supports.sql.SqlTimestampWrapperFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class JavaAtomicSupportsProvider implements JsonSupportsProvider {

  @Override
  public Collection<? extends JsonWrapperFactory> provideSupports() {
    List<JsonWrapperFactory> factories = new ArrayList<>();
    factories.add(AtomicBooleanWrapperFactory.INSTANCE);
    factories.add(AtomicIntegerWrapperFactory.INSTANCE);
    factories.add(AtomicIntegerArrayWrapperFactory.INSTANCE);
    factories.add(AtomicLongWrapperFactory.INSTANCE);
    factories.add(AtomicLongArrayWrapperFactory.INSTANCE);
    return Collections.unmodifiableList(factories);
  }
}
