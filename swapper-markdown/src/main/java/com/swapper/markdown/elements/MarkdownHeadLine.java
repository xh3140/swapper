package com.swapper.markdown.elements;

import com.swapper.markdown.MarkdownElement;

import java.io.IOException;
import java.io.Writer;

public class MarkdownHeadLine extends MarkdownElement {
  private final int _level;

  private final String _content;

  public MarkdownHeadLine(int level, String content) {
    if (level < 1 || level > 6) {
      throw new IllegalArgumentException("The head level must be between 1 and 6.");
    }
    _level = level;
    _content = content;
  }

  @Override
  public void writeToHtml(Writer writer) throws IOException {
    writer.write("<h");
    writer.write('0' + _level);
    writer.write(">");
    writer.write(_content);
    writer.write("</h");
    writer.write('0' + _level);
    writer.write(">");
    writer.write(System.lineSeparator());
  }
}
