package com.swapper.json.experimental.struct;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class JSON_ARRAY_TEST {
  @Test
  public void test(){
    ArrayList<Object> list = new ArrayList<>();
    list.remove(666);
  }


  @Test
  public void test_constructor_empty() {
    JSON_ARRAY jsonArray = new JSON_ARRAY();
    Assertions.assertEquals(0, jsonArray.size());
    Assertions.assertTrue(jsonArray.isEmpty());
  }

  @RepeatedTest(10)
  public void test_constructor_capacity() {
    Assertions.assertThrows(IllegalArgumentException.class, () -> new JSON_ARRAY(-1));
    JSON_ARRAY jsonArray = new JSON_ARRAY(10);
    Assertions.assertEquals(0, jsonArray.size());
    Assertions.assertTrue(jsonArray.isEmpty());
  }
}
