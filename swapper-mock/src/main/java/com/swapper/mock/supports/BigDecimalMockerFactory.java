/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.supports;

import com.swapper.mock.MockContext;
import com.swapper.mock.Mocker;
import com.swapper.mock.MockerFactory;
import com.swapper.mock.config.MockBigDecimalConfig;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Random;

public enum BigDecimalMockerFactory implements MockerFactory {
  INSTANCE;

  @SuppressWarnings("unchecked")
  @Override
  public <T> Mocker<T> create(Type type) {
    return type == BigDecimal.class ? (Mocker<T>) BigDecimalMocker.INSTANCE : null;
  }

   enum BigDecimalMocker implements Mocker<BigDecimal> {
    INSTANCE;

    @Override
    public BigDecimal mock(MockContext context) {
      Random random = context.random();
      MockBigDecimalConfig config = context.getConfig(MockBigDecimalConfig.class);
      int bits = config.getBits();
      int scale = config.getScale();
      double factor = context.random().nextDouble();
      BigDecimal pow = BigDecimal.TEN.pow(scale + 1);
      BigDecimal floated = pow.subtract(BigDecimal.ONE)
        .multiply(BigDecimal.valueOf(factor))
        .divide(pow, scale, RoundingMode.HALF_EVEN);
      BigDecimal decimal = new BigDecimal(new BigInteger(bits, random)).add(floated);
      if (!config.isUnsigned() && random.nextBoolean()) {
        return decimal.negate();
      }
      return decimal;
    }
  }
}
