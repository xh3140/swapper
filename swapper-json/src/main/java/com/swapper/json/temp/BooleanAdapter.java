package com.swapper.json.temp;

import com.swapper.json.io.JsonReader;
import com.swapper.json.io.JsonWriter;

import java.io.IOException;

public class BooleanAdapter implements TypeAdapter<Boolean> {
  @Override
  public Boolean read(JsonReader reader) throws IOException {
    int token = reader.peek();
    if (token == JsonReader.TOKEN_NULL) {
      reader.nextNull();
      return null;
    }
    return reader.nextBoolean();
  }

  @Override
  public void write(JsonWriter writer, Boolean value) {

  }
}
