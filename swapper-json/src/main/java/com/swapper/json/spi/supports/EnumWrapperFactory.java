/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.spi.supports;

import com.swapper.json.*;

import java.lang.reflect.Type;
import java.util.Objects;

/**
 * The {@code Enum<?>} wrapper factory.
 */
public enum EnumWrapperFactory implements JsonWrapperFactory {
  INSTANCE;

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Override
  public <T> JsonWrapper<T> create(JsonContext context, Type type) {
    return (type instanceof Class<?> && ((Class<?>) type).isEnum()) ? new EnumWrapper((Class<?>) type) : null;
  }

  public static final class EnumWrapper<T extends Enum<T>> implements JsonWrapper<T> {
    private final Class<T> enumType;

    public EnumWrapper(Class<T> enumType) {
      this.enumType = Objects.requireNonNull(enumType, "enumType");
    }

    @Override
    public JsonValue wrap(T value) {
      return value == null ? JsonValue.NULL : JsonString.valueOf(value.name());
    }

    @Override
    public T unwrap(JsonValue value) {
      return JsonValue.NULL.equals(value) ? null : Enum.valueOf(enumType, value.toString());
    }
  }
}
