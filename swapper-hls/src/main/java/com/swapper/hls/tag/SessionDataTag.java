package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaPlaylist)
public final class SessionDataTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-SESSION-DATA";

  private String mDataId;
  private String mValue;
  private String mUri;
  private String mLanguage;

  public SessionDataTag() {
    super(TAG_NAME);
  }

  @Override
  public String toString() {
    return "";
  }
}
