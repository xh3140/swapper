/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.regexp;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CharacterClassesTest {
  @Test
  public void test_method_unmodifiable() {
    CharacterClasses classes = new CharacterClasses();
    classes.union(new CharacterRange('x'));
    classes.unmodifiable();
    assertThrows(UnsupportedOperationException.class, () -> classes.union(new CharacterRange('a')));
    assertThrows(UnsupportedOperationException.class, () -> classes.unions(Arrays.asList(new CharacterRange('b'), new CharacterRange('c'))));
    assertThrows(UnsupportedOperationException.class, () -> {
      Iterator<CharacterRange> iterator = classes.iterator();
      if (iterator.hasNext()) {
        iterator.remove();
      }
    });
  }

  @Test
  public void test_method_union() {
    CharacterClasses classes = new CharacterClasses();
    classes.union(new CharacterRange('a'));
    classes.union(new CharacterRange('b'));
    classes.union(new CharacterRange('c'));
    assertEquals(1, classes.size());
    assertEquals(new CharacterRange('a', 'c'), classes.first());
    classes.union(new CharacterRange('e'));
    assertEquals(2, classes.size());
    assertEquals(new CharacterRange('e'), classes.last());
    classes.union(new CharacterRange('f', 's'));
    assertEquals(2, classes.size());
    assertEquals(new CharacterRange('e', 's'), classes.last());
    classes.union(new CharacterRange('d', 'h'));
    assertEquals(1, classes.size());
    assertEquals(new CharacterRange('a', 's'), classes.first());
    classes.union(new CharacterRange('c', 'k'));
    assertEquals(1, classes.size());
    assertEquals(new CharacterRange('a', 's'), classes.first());
  }

  @Test
  public void test_method_except() {
    assertEquals(CharacterClasses.NON_DIGIT, CharacterClasses.DIGIT.except());
    assertEquals(CharacterClasses.NON_WHITESPACE, CharacterClasses.WHITESPACE.except());
    assertEquals(CharacterClasses.NON_WORD, CharacterClasses.WORD.except());
    assertEquals(CharacterClasses.NON_HORIZONTAL_WHITESPACE, CharacterClasses.HORIZONTAL_WHITESPACE.except());
    assertEquals(CharacterClasses.NON_VERTICAL_WHITESPACE, CharacterClasses.VERTICAL_WHITESPACE.except());
  }
}
