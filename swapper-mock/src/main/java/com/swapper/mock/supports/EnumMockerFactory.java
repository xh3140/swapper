/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.supports;

import com.swapper.mock.MockContext;
import com.swapper.mock.Mocker;
import com.swapper.mock.MockerFactory;
import com.swapper.reflect.TypeHelper;

import java.lang.reflect.Type;
import java.util.Objects;

public enum EnumMockerFactory implements MockerFactory {
  INSTANCE;

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Override
  public <T> Mocker<T> create(Type type) {
    return TypeHelper.isEnum(type) ? new EnumMocker((Class) type) : null;
  }

  private static final class EnumMocker<E extends Enum<E>> implements Mocker<E> {
    private final E[] constants;

    public EnumMocker(Class<E> enumType) {
      constants = Objects.requireNonNull(enumType).getEnumConstants();
    }

    @Override
    public E mock(MockContext context) {
      return constants[context.random().nextInt(constants.length)];
    }
  }
}
