/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.spi.providers;

import com.swapper.json.JsonWrapperFactory;
import com.swapper.json.spi.JsonSupportsProvider;
import com.swapper.json.spi.supports.time.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class JavaTimeSupportsProvider implements JsonSupportsProvider {

  @Override
  public Collection<? extends JsonWrapperFactory> provideSupports() {
    List<JsonWrapperFactory> factories = new ArrayList<>();
    factories.add(LocalDateWrapperFactory.INSTANCE);
    factories.add(LocalTimeWrapperFactory.INSTANCE);
    factories.add(LocalDateTimeWrapperFactory.INSTANCE);
    factories.add(ZonedDateTimeWrapperFactory.INSTANCE);
    factories.add(OffsetTimeWrapperFactory.INSTANCE);
    factories.add(OffsetDateTimeWrapperFactory.INSTANCE);
    factories.add(InstantWrapperFactory.INSTANCE);
    factories.add(ZoneIdWrapperFactory.INSTANCE);
    factories.add(YearWrapperFactory.INSTANCE);
    factories.add(MonthWrapperFactory.INSTANCE);
    factories.add(DayOfWeekWrapperFactory.INSTANCE);
    factories.add(YearMonthWrapperFactory.INSTANCE);
    factories.add(MonthDayWrapperFactory.INSTANCE);
    factories.add(DurationWrapperFactory.INSTANCE);
    factories.add(PeriodWrapperFactory.INSTANCE);
    return Collections.unmodifiableList(factories);
  }
}
