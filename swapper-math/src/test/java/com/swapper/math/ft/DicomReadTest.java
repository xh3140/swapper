package com.swapper.math.ft;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class DicomReadTest {

  /**
   * 7fe0,0010
   */
  public static void main(String[] args) throws IOException {
    String path = "E:\\Coder\\JavaProjects\\swapper\\swapper-math\\src\\test\\resources\\mr.dcm";
    File file = new File(path);
    FileInputStream stream = new FileInputStream(file);
    byte[] buffer = new byte[16];
    int count = 0;
    int last = 0;
    boolean flag = false;
    while (stream.read(buffer) != -1) {
      for (byte b : buffer) {
        int i = (b & 0xFF);
        if (!flag && last == 0xe0 && i == 0x7f) {
          flag = true;
        }
        last = i;
        if (flag){
          System.out.printf("\33[32;4m%02X ", b);
        }else {
          System.out.printf("%02X ", b);
        }
      }
      if (flag && ++count > 20) {
        break;
      }
      System.out.println();
    }
  }
}
