import org.gradle.plugin.use.PluginDependenciesSpec
import org.gradle.plugin.use.PluginDependencySpec

object Versions {
  const val java = "1.8"
  const val kotlin = "1.5.31"
  const val gradle = "7.2.0"
  const val junit_jupiter = "5.8.2"
  const val junit_platform = "1.8.2"
}

object Modules {
  const val math = "swapper-math"
  const val reflect = "swapper-reflect"
  const val regexp = "swapper-regexp"
  const val mock = "swapper-mock"
  const val json = "swapper-json"
  const val json_ktx = "swapper-json-ktx"
  const val xml = "swapper-xml"
  const val hls = "swapper-hls"
}

object Publication {
  const val group = "com.swapper"
  const val version = "0.0.1"
  val artifactIds = Modules
}

object Dependencies {
  const val kotlin_stdlib = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}"
  const val junit_jupiter_api = "org.junit.jupiter:junit-jupiter-api:${Versions.junit_jupiter}"
  const val junit_jupiter_engine = "org.junit.jupiter:junit-jupiter-engine:${Versions.junit_jupiter}"
  const val junit_jupiter_params = "org.junit.jupiter:junit-jupiter-params:${Versions.junit_jupiter}"
  const val junit_platform_suite = "org.junit.platform:junit-platform-suite:${Versions.junit_platform}"
}

inline val PluginDependenciesSpec.`kotlin-jvm`: PluginDependencySpec
  get() = id("org.jetbrains.kotlin.jvm").version(Versions.kotlin)
