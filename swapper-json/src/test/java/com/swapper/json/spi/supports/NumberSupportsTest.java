/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.spi.supports;

import com.swapper.json.TestDataSource;
import com.swapper.json.TestSupportByArray;
import org.junit.jupiter.api.RepeatedTest;

import java.math.BigDecimal;
import java.math.BigInteger;

public class NumberSupportsTest {
  @RepeatedTest(10)
  public void test_support_byte() {
    TestSupportByArray.test(TestDataSource.generate(Byte.class));
  }

  @RepeatedTest(10)
  public void test_support_short() {
    TestSupportByArray.test(TestDataSource.generate(Short.class));
  }

  @RepeatedTest(10)
  public void test_support_integer() {
    TestSupportByArray.test(TestDataSource.generate(Integer.class));
  }

  @RepeatedTest(10)
  public void test_support_long() {
    TestSupportByArray.test(TestDataSource.generate(Long.class));
  }

  @RepeatedTest(10)
  public void test_support_float() {
    TestSupportByArray.test(TestDataSource.generate(Float.class));
  }

  @RepeatedTest(10)
  public void test_support_double() {
    TestSupportByArray.test(TestDataSource.generate(Double.class));
  }

  @RepeatedTest(10)
  public void test_support_big_integer() {
    TestSupportByArray.test(TestDataSource.generate(BigInteger.class));
  }

  @RepeatedTest(10)
  public void test_support_big_decimal() {
    TestSupportByArray.test(TestDataSource.generate(BigDecimal.class));
  }
}
