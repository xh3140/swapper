package com.swapper.markdown.elements;

import com.swapper.markdown.MarkdownElement;

import java.io.IOException;
import java.io.Writer;

public class MarkdownImage extends MarkdownElement {
  private String _alter;
  private String _url;
  private String _title;


  @Override
  public void writeToHtml(Writer writer) throws IOException {
    writer.write("<img src=\"");
    writer.write(_url);
    writer.write("\"alt=\"");
    writer.write(_alter);
    writer.write("\"/>");
    writer.write(System.lineSeparator());
  }
}
