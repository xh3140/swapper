plugins {
  `java-library`
  `maven-publish`
}

group = Publication.group
version = Publication.version

java {
  sourceCompatibility = JavaVersion.toVersion(Versions.java)
  targetCompatibility = JavaVersion.toVersion(Versions.java)
}

dependencies {
  api(project(":${Modules.reflect}"))
  testImplementation(project(":${Modules.mock}"))
  testImplementation(Dependencies.junit_jupiter_api)
  testRuntimeOnly(Dependencies.junit_jupiter_engine)
  testImplementation(Dependencies.junit_jupiter_params)
  testImplementation(Dependencies.junit_platform_suite)
  testImplementation("com.google.code.gson:gson:2.9.0")
  testImplementation("com.alibaba.fastjson2:fastjson2:2.0.5")
  testImplementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.13.3")
  testImplementation("com.fasterxml.jackson.datatype:jackson-datatype-jdk8:2.13.3")
  testImplementation("org.openjdk.jmh:jmh-core:1.35")
  testAnnotationProcessor("org.openjdk.jmh:jmh-generator-annprocess:1.35")
//  testImplementation("org.openjdk.jmh:jmh-generator-annprocess:1.35")
//  testImplementation("org.openjdk.jmh:jmh-generator-reflection:1.35")
//  testImplementation("org.openjdk.jmh:jmh-generator-bytecode:1.35")

}

tasks.getByName<Test>("test") {
  useJUnitPlatform()
}

publishing {
  publications {
    create<MavenPublication>("mavenJava") {
      groupId = Publication.group
      artifactId = Publication.artifactIds.json
      version = Publication.version
      from(components["java"])
    }
  }
}
