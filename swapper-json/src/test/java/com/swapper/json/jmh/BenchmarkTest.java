package com.swapper.json.jmh;

import com.alibaba.fastjson2.JSON;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.swapper.json.JsonObject;
import com.swapper.json.TestJsonFile;
import com.swapper.json.jmh.bean.ChoiceQuestionPager;
import org.openjdk.jmh.annotations.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Fork(1)
@Warmup(time = 1, iterations = 5)
@Measurement(time = 1, iterations = 5)
@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class BenchmarkTest {
  @Benchmark
  public void swapperjson() {
    TestJsonFile.getResourceJsonFiles()
      .stream().filter(s -> s.contains("lece"))
      .forEach(s -> {
        try {
          FileReader reader = new FileReader(s);
          JsonObject jsonObject = JsonObject.fromJson(reader);
          ChoiceQuestionPager pager = jsonObject.parseObject(ChoiceQuestionPager.class);
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        }
      });
  }

  @Benchmark
  public void gson() {
    Gson gson = new Gson();
    TestJsonFile.getResourceJsonFiles()
      .stream().filter(s -> s.contains("lece"))
      .forEach(s -> {
        try {
          FileReader reader = new FileReader(s);
          ChoiceQuestionPager pager = gson.fromJson(reader, ChoiceQuestionPager.class);
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        }
      });
  }

  @Benchmark
  public void fastjson2() {
    TestJsonFile.getResourceJsonFiles()
      .stream().filter(s -> s.contains("lece"))
      .forEach(s -> {
        try {
          FileInputStream fileInputStream = new FileInputStream(s);
          ChoiceQuestionPager pager = JSON.parseObject(fileInputStream, ChoiceQuestionPager.class);
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        }
      });
  }

  @Benchmark
  public void jackson() {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    TestJsonFile.getResourceJsonFiles()
      .stream().filter(s -> s.contains("lece"))
      .forEach(s -> {
        try {
          FileReader reader = new FileReader(s);
          ChoiceQuestionPager pager = objectMapper.readValue(reader, ChoiceQuestionPager.class);
        } catch (IOException e) {
          e.printStackTrace();
        }
      });
  }
}



