/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.supports;

import com.swapper.mock.MockContext;
import com.swapper.mock.Mocker;
import com.swapper.mock.MockerFactory;
import com.swapper.mock.config.MockDoubleConfig;

import java.lang.reflect.Type;
import java.math.BigDecimal;

public enum DoubleMockerFactory implements MockerFactory {
  INSTANCE;

  @SuppressWarnings("unchecked")
  @Override
  public <T> Mocker<T> create(Type type) {
    return type == double.class || type == Double.class ? (Mocker<T>) DoubleMocker.INSTANCE : null;
  }

   enum DoubleMocker implements Mocker<Double> {
    INSTANCE;

    @Override
    public Double mock(MockContext context) {
      MockDoubleConfig config = context.getConfig(MockDoubleConfig.class);
      double[] range = config.getRange();
      double factor = context.random().nextDouble();
      return BigDecimal.valueOf(range[1])
        .subtract(BigDecimal.valueOf(range[0]))
        .multiply(BigDecimal.valueOf(factor))
        .add(BigDecimal.valueOf(range[0]))
        .doubleValue();
    }
  }
}
