/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.json.spi;

import com.swapper.json.JsonWrapperFactory;
import com.swapper.json.spi.providers.JavaAtomicSupportsProvider;
import com.swapper.json.spi.supports.*;
import com.swapper.json.spi.supports.EnumWrapperFactory;
import com.swapper.json.spi.supports.NumberWrapperFactory;
import com.swapper.json.spi.supports.ObjectWrapperFactory;
import com.swapper.json.spi.supports.ReflectionWrapperFactory;
import com.swapper.json.spi.supports.atomic.*;
import com.swapper.json.spi.supports.net.URIWrapperFactory;
import com.swapper.json.spi.supports.net.URLWrapperFactory;
import com.swapper.json.spi.supports.sql.SqlDateWrapperFactory;
import com.swapper.json.spi.supports.sql.SqlTimeWrapperFactory;
import com.swapper.json.spi.supports.sql.SqlTimestampWrapperFactory;
import com.swapper.json.spi.supports.time.*;
import com.swapper.json.spi.supports.util.CalendarWrapperFactory;
import com.swapper.json.spi.supports.util.CurrencyWrapperFactory;
import com.swapper.json.spi.supports.util.DateWrapperFactory;
import com.swapper.json.spi.supports.util.UUIDWrapperFactory;

import java.util.*;

public final class JsonSupportsManager {

  private JsonSupportsManager() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  public static Collection<? extends JsonWrapperFactory> supports(Collection<? extends JsonWrapperFactory> customs) {
    List<JsonWrapperFactory> factories = new ArrayList<>(55);
    factories.add(ObjectWrapperFactory.INSTANCE);
    factories.add(BooleanWrapperFactory.INSTANCE);
    factories.add(CharacterWrapperFactory.INSTANCE);
    factories.add(StringWrapperFactory.INSTANCE);
    factories.add(ByteWrapperFactory.INSTANCE);
    factories.add(ShortWrapperFactory.INSTANCE);
    factories.add(IntegerWrapperFactory.INSTANCE);
    factories.add(LongWrapperFactory.INSTANCE);
    factories.add(FloatWrapperFactory.INSTANCE);
    factories.add(DoubleWrapperFactory.INSTANCE);
    factories.add(BigIntegerWrapperFactory.INSTANCE);
    factories.add(BigDecimalWrapperFactory.INSTANCE);
    factories.add(NumberWrapperFactory.INSTANCE);
    factories.add(EnumWrapperFactory.INSTANCE);
    factories.add(ArrayWrapperFactory.INSTANCE);
    factories.add(CollectionWrapperFactory.INSTANCE);
    factories.add(MapWrapperFactory.INSTANCE);
    ServiceLoader.load(JsonSupportsProvider.class).forEach(provider ->
      factories.addAll(provider.provideSupports())
    );
    factories.addAll(customs);
    factories.add(ReflectionWrapperFactory.INSTANCE);
    return Collections.unmodifiableList(factories);
  }
}
