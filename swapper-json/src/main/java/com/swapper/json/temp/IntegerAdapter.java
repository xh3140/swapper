package com.swapper.json.temp;

import com.swapper.json.io.JsonReader;
import com.swapper.json.io.JsonWriter;

import java.io.IOException;

public class IntegerAdapter implements TypeAdapter<Integer> {
  @Override
  public Integer read(JsonReader reader) throws IOException {
    int token = reader.peek();
    if (token == JsonReader.TOKEN_NULL) {
      reader.nextNull();
      return null;
    }
    String s = reader.nextIntegerString();
    return Integer.parseInt(s);
  }

  @Override
  public void write(JsonWriter writer, Integer value) {

  }
}
