# swapper-json

## 一、介绍

**swapper-json**是一个由Java编写的JSON序列化与反序列化工具。

## 二、引用

#### 在gradle项目中引用

```groovy
repositories {
  maven { url "https://jitpack.io" }
}

dependencies {
  implementation 'com.gitee.swapper:json:${version}'
}
```

### 三、序列化(Serialization)和反序列化(Deserialization)

#### 1.测试类

```java
public enum Week {
  Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
}
```

```java
/**
 * Test basic data types.
 * For code brevity, use the public modifier instead of the setter and getter.
 * Omit other methods.
 */
public final class TestBean {
  public Object mNull = null;
  public Boolean mBooleanTrue = true;
  public Boolean mBooleanFalse = false;
  public Byte mByte = Byte.MAX_VALUE;
  public Short mShort = Short.MAX_VALUE;
  public Integer mInteger = Integer.MAX_VALUE;
  public Long mLong = Long.MAX_VALUE;
  public Float mFloat = Float.MAX_VALUE;
  public Double mDouble = Double.MAX_VALUE;
  public BigInteger mBigInteger = new BigInteger("6b7404b3f87e49376fac3b6a43dbe3be", 16);
  public BigDecimal mBigDecimal = new BigDecimal("3.14159265358979323846264338327950288419716939937510");
  public String mStringEmpty = "";
  public String mStringNormal = "Hello, world!";
  public String mStringEscape = " \\ \r \n \b \t \f \" ";
  public String mStringHexText = "\u4f60\u597d\uff0c\u4e16\u754c\uff01";
  public Character mCharacter = 'X';
  public Date mDate = new Date(1607703132000L);
  public Week mEnum = Week.Monday;
}
```

#### 2. 序列化（自动与手动）

```java
public class SerializationTest {
  @Test
  public void testAuto() {
    TestBean bean = new TestBean();
    // Auto build the data structure and fill in the parameters
    JsonObject jsonObject = JsonObject.fromObject(bean);
    // JSON serialization
    String json = jsonObject.toJson();
    System.out.println(json);
  }

  @Test
  public void testManually() {
    TestBean bean = new TestBaseData();
    // Manually build the data structure and fill in the parameters
    JsonObject jsonObject = new JsonObject();
    jsonObject.set("mNull", bean.mNull);
    jsonObject.set("mBooleanTrue", bean.mBooleanTrue);
    jsonObject.set("mBooleanFalse", bean.mBooleanFalse);
    jsonObject.set("mByte", bean.mByte);
    jsonObject.set("mShort", bean.mShort);
    jsonObject.set("mInteger", bean.mInteger);
    jsonObject.set("mLong", bean.mLong);
    jsonObject.set("mFloat", bean.mFloat);
    jsonObject.set("mDouble", bean.mDouble);
    jsonObject.set("mBigInteger", bean.mBigInteger);
    jsonObject.set("mBigDecimal", bean.mBigDecimal);
    jsonObject.set("mStringEmpty", bean.mStringEmpty);
    jsonObject.set("mStringNormal", bean.mStringNormal);
    jsonObject.set("mStringEscape", bean.mStringEscape);
    jsonObject.set("mStringHexText", bean.mStringHexText);
    jsonObject.set("mCharacter", bean.mCharacter);
    jsonObject.set("mDate", bean.mDate);
    jsonObject.set("mEnum", bean.mEnum);
    // JSON serialization
    String json = jsonObject.toJson();
    System.out.println(json);
  }
}
```

#### 3.序列化输出结果（经过格式化）

```json
{
  "mNull": null,
  "mBooleanTrue": true,
  "mBooleanFalse": false,
  "mByte": 127,
  "mShort": 32767,
  "mInteger": 2147483647,
  "mLong": 9223372036854775807,
  "mFloat": 3.4028235E38,
  "mDouble": 1.7976931348623157E308,
  "mBigInteger": 142829797372960495909951083877238563774,
  "mBigDecimal": 3.14159265358979323846264338327950288419716939937510,
  "mStringEmpty": "",
  "mStringNormal": "Hello,world!",
  "mStringEscape": " \\ \r \n \b \t \f \" ",
  "mStringHexText": "你好，世界！",
  "mCharacter": "X",
  "mDate": 1607703132000,
  "mEnum": "Monday"
}
```

#### 4.反序列化（自动与手动）

```java
public class DeserializationTest {
  @Test
  public void testAuto() {
    String json = "{" +
      "\"mNull\": null," +
      "\"mBooleanTrue\": true," +
      "\"mBooleanFalse\": false," +
      "\"mByte\": 127," +
      "\"mShort\": 32767," +
      "\"mInteger\": 2147483647," +
      "\"mLong\": 9223372036854775807," +
      "\"mFloat\": 3.4028235E38," +
      "\"mDouble\": 1.7976931348623157E308," +
      "\"mBigInteger\": 142829797372960495909951083877238563774," +
      "\"mBigDecimal\": 3.14159265358979323846264338327950288419716939937510," +
      "\"mStringEmpty\": \"\"," +
      "\"mStringNormal\": \"Hello,world!\"," +
      "\"mStringEscape\": \" \\\\ \\r \\n \\b \\t \\f \\\" \"," +
      "\"mStringHexText\": \"你好，世界！\"," +
      "\"mCharacter\": \"X\"," +
      "\"mDate\": 1607703132000," +
      "\"mEnum\": \"Monday\"" +
      "}";
    // Auto build the data structure and fill in the parameters
    JsonObject jsonObject = JsonObject.fromJson(json);
    // JSON deserialization
    TestBean bean = jsonObject.parseObject(TestBaseData.class);
    System.out.println(bean);
  }

  @Test
  public void testManually() {
    // Manually build the data structure and fill in the parameters
    JsonObject jsonObject = new JsonObject();
    jsonObject.set("mNull", null);
    jsonObject.set("mBooleanTrue", true);
    jsonObject.set("mBooleanFalse", false);
    jsonObject.set("mByte", Byte.MAX_VALUE);
    jsonObject.set("mShort", Short.MAX_VALUE);
    jsonObject.set("mInteger", Integer.MAX_VALUE);
    jsonObject.set("mLong", Long.MAX_VALUE);
    jsonObject.set("mFloat", Float.MAX_VALUE);
    jsonObject.set("mDouble", Double.MAX_VALUE);
    jsonObject.set("mBigInteger", new BigInteger("6b7404b3f87e49376fac3b6a43dbe3be", 16));
    jsonObject.set("mBigDecimal", new BigDecimal("3.14159265358979323846264338327950288419716939937510"));
    jsonObject.set("mStringEmpty", "");
    jsonObject.set("mStringNormal", "Hello,world!");
    jsonObject.set("mStringEscape", " \\ \r \n \b \t \f \" ");
    jsonObject.set("mStringHexText", "\u4f60\u597d\uff0c\u4e16\u754c\uff01");
    jsonObject.set("mCharacter", 'X');
    jsonObject.set("mDate", new Date(1607703132000L));
    jsonObject.set("mEnum", Week.Monday);
    // JSON deserialization
    TestBean bean = jsonObject.parseObject(TestBean.class);
    System.out.println(bean);
  }
}
```

#### 5.序列化与反序列化检测

```java
public class SerializationDeserializationTest {
  @Test
  public void test() {
    TestBean bean1 = new TestBean();
    JsonObject jsonObject1 = JsonObject.fromObject(bean1);
    String json = jsonObject1.toJson();
    JsonObject jsonObject2 = JsonObject.fromJson(json);
    TestBean bean2 = jsonObject2.parseObject(TestBean.class);
    Assert.assertEquals(bean1, bean2);
    System.out.println(json);
  }
}
```
