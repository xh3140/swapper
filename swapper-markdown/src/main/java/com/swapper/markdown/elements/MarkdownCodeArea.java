package com.swapper.markdown.elements;

import com.swapper.markdown.MarkdownElement;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class MarkdownCodeArea extends MarkdownElement {
  private final String _language;
  private final List<String> _codeLines;


  public MarkdownCodeArea(String language, List<String> codeLines) {
    _language = language;
    _codeLines = codeLines;
  }

  @Override
  public void writeToHtml(Writer writer) throws IOException {
    writer.write("<pre>");
    writer.write(System.lineSeparator());
    for (String codeLine : _codeLines) {
      writer.write("<code>");
      writer.write(codeLine);
      writer.write("</code>");
      writer.write(System.lineSeparator());
    }
    writer.write("</pre>");
    writer.write(System.lineSeparator());
  }
}
