package com.swapper.math.work;

import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * 离子消毒
 */
public class LZXD {
  @Test
  public void test() {
    // 消毒持续时间
    int[] dur1 = {60, 90};
    // 消毒间隔时间
    int[] dur2 = {3, 8};
    // 消毒设备
    List<String> eqs = Arrays.asList(
      "DR1", "DR2", "MG1", "MG2", "CT", "RF", "CBCT"
    );

    DateTimeFormatter f1 = DateTimeFormatter.ofPattern("HH:mm");
//    DateTimeFormatter f2 = DateTimeFormatter.ofPattern("h时m分钟");

    Random random = new Random(System.currentTimeMillis());
    // 打乱消毒顺序
    Collections.shuffle(eqs, random);
    // 开始时间
    LocalTime current = LocalTime.now();
    for (String eq : eqs) {
      int dur3 = dur1[0] + random.nextInt(dur1[1] - dur1[0] + 1);
      LocalTime time = current.plusMinutes(dur3);
      int dur4 = dur2[0] + random.nextInt(dur2[1] - dur2[0] + 1);
      LocalTime time1 = time.plusMinutes(dur4);

      System.out.printf("设备：%s，于%s开始消毒，%s结束，持续了%d分钟。%n",
        eq, f1.format(current), f1.format(time), dur3
      );

      System.out.printf("等待了%d分钟...%n\n", dur4);
      current = time1;
    }
  }
}
