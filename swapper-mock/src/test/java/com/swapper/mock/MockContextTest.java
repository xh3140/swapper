/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock;

import com.swapper.mock.config.MockIntegerConfig;
import com.swapper.mock.config.MockStringConfig;
import com.swapper.reflect.TypeHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class MockContextTest {
  @Test
  public void t() {
    MockContext context = new MockContext();
    for (int i = 0; i < 20; i++) {
      TestBean o = context.mock(TestBean.class);
      System.out.println(o);
    }
  }


  public static final class SupportsArgumentsProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      List<Arguments> arguments = new ArrayList<>();
      MockContext ctx = new MockContext();
      // base
      arguments.add(Arguments.of(ctx, Boolean.class));
      arguments.add(Arguments.of(ctx, Character.class));
      arguments.add(Arguments.of(ctx, Byte.class));
      arguments.add(Arguments.of(ctx, Short.class));
      arguments.add(Arguments.of(ctx, Integer.class));
      arguments.add(Arguments.of(ctx, Long.class));
      arguments.add(Arguments.of(ctx, Float.class));
      arguments.add(Arguments.of(ctx, Double.class));
      arguments.add(Arguments.of(ctx, BigInteger.class));
      arguments.add(Arguments.of(ctx, BigDecimal.class));
      // base array 1d
      arguments.add(Arguments.of(ctx, Boolean[].class));
      arguments.add(Arguments.of(ctx, Character[].class));
      arguments.add(Arguments.of(ctx, Byte[].class));
      arguments.add(Arguments.of(ctx, Short[].class));
      arguments.add(Arguments.of(ctx, Integer[].class));
      arguments.add(Arguments.of(ctx, Long[].class));
      arguments.add(Arguments.of(ctx, Float[].class));
      arguments.add(Arguments.of(ctx, Double[].class));
      arguments.add(Arguments.of(ctx, BigInteger[].class));
      arguments.add(Arguments.of(ctx, BigDecimal[].class));
      return arguments.stream();
    }
  }

  @ParameterizedTest
  @ArgumentsSource(SupportsArgumentsProvider.class)
  public void test_supports(MockContext context, Type type) {
    System.out.printf("%s: %s\n", TypeHelper.name(type), context.mock(type));
  }

  @ParameterizedTest
  @ValueSource(classes = {
    boolean.class,
    char.class,
    byte.class,
    short.class,
    int.class,
    long.class,
    float.class,
    double.class
  })
  public void test_supports_primitive(Type type) {
    System.out.printf("%s: %s\n", TypeHelper.name(type), MockContext.defaultMock(type));
  }

  @ParameterizedTest
  @ValueSource(classes = {
    Boolean.class,
    Character.class,
    Byte.class,
    Short.class,
    Integer.class,
    Long.class,
    Float.class,
    Double.class
  })
  public void test_supports_base(Type type) {
    System.out.printf("%s: %s\n", TypeHelper.name(type), MockContext.defaultMock(type));
  }

  @ParameterizedTest
  @ValueSource(classes = {
    Boolean[].class,
    Character[].class,
    Byte[].class,
    Short[].class,
    Integer[].class,
    Long[].class,
    Float[].class,
    Double[].class
  })
  public void test_supports_base_array_1d(Type type) {
    System.out.printf("%s: %s\n", TypeHelper.name(type), Arrays.toString((Object[]) MockContext.defaultMock(type)));
  }

  @ParameterizedTest
  @ValueSource(classes = {
    Boolean[][].class,
    Character[][].class,
    Byte[][].class,
    Short[][].class,
    Integer[][].class,
    Long[][].class,
    Float[][].class,
    Double[][].class
  })
  public void test_supports_base_array_2d(Type type) {
    System.out.printf("%s: %s\n", TypeHelper.name(type), Arrays.deepToString(MockContext.defaultMock(type)));
  }

  @Test
  public void test_supports_() {
    MockContext context = new MockContext()
      .withConfig(MockIntegerConfig.class, config -> config.setRange(50, 100))
      .withConfig(MockStringConfig.class, config -> config.setPattern("\\d+abdc"));

    System.out.println(context.mock(String.class));
  }
}
