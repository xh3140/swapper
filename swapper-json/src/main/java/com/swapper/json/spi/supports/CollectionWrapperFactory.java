/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.spi.supports;

import com.swapper.json.*;
import com.swapper.reflect.TypeHelper;
import com.swapper.reflect.creator.InstanceCreator;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * The collection wrapper factory.
 *
 * @see java.util.Collection<>
 */
public enum CollectionWrapperFactory implements JsonWrapperFactory {
  INSTANCE;

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Override
  public <T> JsonWrapper<T> create(JsonContext context, Type type) {
    Class<?> rawType = context.getRawType(type);
    Type elementType = TypeHelper.getCollectionElementType(type, rawType);
    if (elementType != null) {
      InstanceCreator<T> creator = context.getInstanceCreator(type);
      JsonWrapper<?> elementWrapper = context.findWrapper(elementType);
      return new CollectionWrapper(creator, elementWrapper);
    }
    return null;
  }

  private static final class CollectionWrapper<E, T extends Collection<E>> implements JsonWrapper<T> {
    private final InstanceCreator<T> _creator;
    private final JsonWrapper<E> _elementWrapper;

    public CollectionWrapper(InstanceCreator<T> creator, JsonWrapper<E> elementWrapper) {
      _creator = Objects.requireNonNull(creator);
      _elementWrapper = Objects.requireNonNull(elementWrapper);
    }

    @Override
    public JsonValue wrap(T value) {
      if (value == null) {
        return JsonValue.NULL;
      }
      List<JsonValue> values = new ArrayList<>(value.size());
      for (E element : value) {
        values.add(_elementWrapper.wrap(element));
      }
      return new JsonArray(values);
    }

    @Override
    public T unwrap(JsonValue value) {
      if (JsonValue.NULL.equals(value)) {
        return null;
      }
      JsonArray jsonArray = value.toJsonArray();
      T collection = _creator.create();
      for (JsonValue jsonValue : jsonArray) {
        collection.add(_elementWrapper.unwrap(jsonValue));
      }
      return collection;
    }
  }
}
