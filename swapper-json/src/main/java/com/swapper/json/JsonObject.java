/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json;

import java.io.Reader;
import java.lang.reflect.Type;
import java.util.*;

/**
 * JsonObject is an encapsulation of the object type in JSON.
 */
public final class JsonObject extends JsonDocument implements JsonValueMap {
  /**
   * Static constructor.
   * Construct a JSON object instance from a JSON reader.
   *
   * @param in a JSON reader.
   * @return an instance constructed from a JSON reader.
   */
  public static JsonObject fromJson(Reader in) {
    return JsonDocument.fromJson(in).toJsonObject();
  }

  /**
   * Static constructor.
   * Construct a JSON object instance from a JSON string.
   *
   * @param in a JSON string.
   * @return an instance constructed from a JSON string.
   * @see JsonDocument#fromJson(String)
   */
  public static JsonObject fromJson(String in) {
    return JsonDocument.fromJson(in).toJsonObject();
  }

  /**
   * Static constructor.
   * Construct a JSON array instance from an object.
   * Used default JSON context.
   *
   * @param value a object.
   * @return a JSON array instance from an object.
   */
  public static JsonObject fromObject(Object value) {
    return JsonValue.valueOf(value).toJsonObject();
  }

  /**
   * Static constructor.
   * Construct a JSON array instance from an object.
   * Used default JSON context.
   *
   * @param value a object.
   * @param type  the specified type.
   * @return a JSON array instance from an object.
   */
  public static JsonObject fromObject(Object value, Type type) {
    return JsonValue.valueOf(value, type).toJsonObject();
  }

  /**
   * Static constructor.
   * Construct a JSON array instance from an object.
   *
   * @param context the JSON context.
   * @param value   a object.
   * @return a JSON array instance from an object.
   */
  public static JsonObject fromObject(JsonContext context, Object value) {
    return JsonValue.valueOf(context, value).toJsonObject();
  }

  /**
   * Static constructor.
   * Construct a JSON array instance from an object.
   *
   * @param context the JSON context.
   * @param value   a object.
   * @param type    the specified type.
   * @return a JSON array instance from an object.
   */
  public static JsonObject fromObject(JsonContext context, Object value, Type type) {
    return JsonValue.valueOf(context, value, type).toJsonObject();
  }

  /**
   * Internal management JSON value map.
   */
  private final LinkedHashMap<String, JsonValue> proxy;

  /**
   * Constructs an empty insertion-ordered instance.
   * with the default initial capacity (16) and load factor (0.75).
   */
  public JsonObject() {
    proxy = new LinkedHashMap<>();
  }

  /**
   * Constructs an empty insertion-ordered instance.
   * with the specified initial capacity and a default load factor (0.75).
   *
   * @param initialCapacity the initial capacity.
   * @throws IllegalArgumentException if the initial capacity is negative.
   */
  public JsonObject(int initialCapacity) {
    proxy = new LinkedHashMap<>(initialCapacity);
  }

  /**
   * Constructs an empty insertion-ordered instance.
   * with the specified initial capacity and load factor.
   *
   * @param initialCapacity the initial capacity.
   * @param loadFactor      the load factor.
   * @throws IllegalArgumentException if the initial capacity is negative or the load factor is non-positive.
   */
  public JsonObject(int initialCapacity, float loadFactor) {
    proxy = new LinkedHashMap<>(initialCapacity, loadFactor);
  }

  /**
   * Constructs an insertion-ordered instance with the same mappings as the specified map.
   * The instance is created with a default load factor (0.75) and an initial capacity
   * sufficient to hold the mappings in the specified map.
   *
   * @param map the map whose mappings are to be placed in this JSON object.
   * @throws NullPointerException if the specified map is null.
   */
  public JsonObject(Map<String, ? extends JsonValue> map) {
    proxy = new LinkedHashMap<>(map);
  }

  /**
   * Returns the number of JSON members in this JSON object.
   * If this JSON object contains more than Integer.MAX_VALUE JSON members, returns Integer.MAX_VALUE.
   *
   * @return the number of JSON members in this JSON object.
   */
  @Override
  public int size() {
    return proxy.size();
  }

  /**
   * Returns true if the JSON object is empty.
   * that is, there is no JSON members in the JSON object,
   * the number of JSON members in the JSON object is 0.
   *
   * @return true if the JSON object is empty.
   */
  @Override
  public boolean isEmpty() {
    return proxy.isEmpty();
  }

  /**
   * Returns true if this JSON object contains a value for the specified name.
   *
   * @param name name whose presence in this JSON object is to be tested.
   * @return true if this JSON object contains a value for the specified name.
   */
  @Override
  public boolean containsName(String name) {
    return proxy.containsKey(name);
  }

  /**
   * Returns true if this JSON object maps one or more names to the specified JSON value.
   *
   * @param value the JSON value whose presence in this JSON object is to be tested.
   * @return true if this JSON object maps one or more names to the specified JSON value.
   */
  @Override
  public boolean containsValue(JsonValue value) {
    return proxy.containsValue(value);
  }

  /**
   * Returns the JSON value to which the specified name is mapped.
   *
   * @param name the name whose associated JSON value is to be returned.
   * @return the JSON value to which the specified name is mapped.
   */
  @Override
  public JsonValue get(String name) {
    return proxy.get(name);
  }

  /**
   * Associates the specified JSON value with the specified name in this JSON object.
   *
   * @param name  name with which the specified JSON value is to be associated.
   * @param value the JSON value to be associated with the specified name.
   * @return the previous JSON value associated with name, or null if there was no mapping for name.
   * @throws NullPointerException if the specified value is null.
   */
  @Override
  public JsonValue put(String name, JsonValue value) {
    return proxy.put(name, Objects.requireNonNull(value, "Put value is null."));
  }

  /**
   * Copies all the members from the specified map to this JSON object.
   *
   * @param members members to be stored in this JSON object.
   * @throws NullPointerException if the specified members is null.
   */
  @Override
  public void puts(Map<String, ? extends JsonValue> members) {
    proxy.putAll(members);
  }

  /**
   * Removes the mapping for a name from this JSON object if it is present.
   *
   * @param name name whose mapping is to be removed from the JSON object.
   * @return the previous JSON value associated with name, or null if there was no mapping for name.
   */
  @Override
  public JsonValue remove(String name) {
    return proxy.remove(name);
  }

  /**
   * Removes all the members from this JSON object.
   * The JSON object will be empty after this call returns.
   */
  @Override
  public void clear() {
    proxy.clear();
  }

  /**
   * Returns a Set view of the names contained in this JSON object.
   *
   * @return a Set view of the names contained in this JSON object.
   */
  @Override
  public Set<String> names() {
    return proxy.keySet();
  }

  /**
   * Returns a Collection view of the JSON values contained in this JSON object.
   *
   * @return a collection view of the JSON values contained in this JSON object.
   */
  @Override
  public Collection<JsonValue> values() {
    return proxy.values();
  }

  /**
   * Returns a Set view of the members contained in this JSON object.
   *
   * @return a set view of the members contained in this JSON object
   */
  @Override
  public Set<Map.Entry<String, JsonValue>> members() {
    return proxy.entrySet();
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param other the reference object with which to compare.
   * @return true if this object is the same as the obj argument; false otherwise.
   * @see Object#equals(Object)
   */
  @Override
  public boolean equals(Object other) {
    if (this == other) return true;
    if (other instanceof JsonObject) {
      JsonObject that = (JsonObject) other;
      return Objects.equals(proxy, that.proxy);
    }
    return false;
  }

  /**
   * Gets the hash code of the current instance.
   *
   * @return the hash code of the current instance.
   * @see Object#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hash(proxy);
  }
}
