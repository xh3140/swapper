package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.Basic)
public final class VersionTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-VERSION";
  private final Integer mVersion;

  public VersionTag(Integer version) {
    super(TAG_NAME);
    if (version == null) {
      throw new NullPointerException("HLS tag(" + TAG_NAME + ") version is null.");
    }
    if (version < 1) {
      throw new IllegalArgumentException("HLS tag(" + TAG_NAME + ") version < 1.");
    }
    mVersion = version;
  }

  public Integer version() {
    return mVersion;
  }

  @Override
  public String toString() {
    return String.format("%s:%d\n", mName, mVersion);
  }
}
