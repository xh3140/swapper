package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaPlaylist)
public final class PlayListTypeTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-PLAYLIST-TYPE";

  private final Type mType;

  public PlayListTypeTag(Type type) {
    super(TAG_NAME);
    mType = type;
  }

  public enum Type {EVENT, VOD}

  public Type type() {
    return mType;
  }
}
