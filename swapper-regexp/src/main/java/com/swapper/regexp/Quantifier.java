/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.regexp;

import java.util.Objects;
import java.util.Random;

/**
 * The regular expression quantifier.
 * ?        {@link Quantifier#ZERO_OR_ONE}            Match 0 or 1 times.
 * *        {@link Quantifier#ZERO_OR_MORE}           Match 0 or more times.
 * +        {@link Quantifier#ONE_OR_MORE}            Match 1 or more times.
 * {n}      {@link Quantifier#ofExactly(int)}         Match exactly n times.
 * {n,}     {@link Quantifier#ofLeast(int)}           Match at least n times.
 * {n,m}    {@link Quantifier#ofBetween(int, int)}    Match at least n but not more than m times.
 */
final class Quantifier implements Weighty {
  private static final int CODE_ZERO_OR_ONE = -1;
  private static final int CODE_ZERO_OR_MORE = -2;
  private static final int CODE_ONE_OR_MORE = -3;
  private static final int CODE_AT_EXACTLY = -4;
  private static final int CODE_AT_LEAST = -5;
  private static final int CODE_AT_BETWEEN = -6;

  public static final int MIN_TIMES = 0;
  public static final int MAX_TIMES = Integer.MAX_VALUE;
  public static final Quantifier ZERO_OR_ONE = new Quantifier(CODE_ZERO_OR_ONE, 0, 1);
  public static final Quantifier ZERO_OR_MORE = new Quantifier(CODE_ZERO_OR_MORE, 0, MAX_TIMES);
  public static final Quantifier ONE_OR_MORE = new Quantifier(CODE_ONE_OR_MORE, 1, MAX_TIMES);

  /**
   * Static constructor.
   * Construct a {@link Quantifier} of exactly times.
   * Regular expression pattern: {n}
   *
   * @param times exactly times.
   * @return a {@link Quantifier} instance.
   */
  public static Quantifier ofExactly(int times) {
    return new Quantifier(CODE_AT_EXACTLY, times, times);
  }

  /**
   * Static constructor.
   * Construct a {@link Quantifier} of at least times.
   * Regular expression pattern: {n,}
   *
   * @param atLeastTimes at least times.
   * @return a {@link Quantifier} instance.
   */
  public static Quantifier ofLeast(int atLeastTimes) {
    return new Quantifier(CODE_AT_LEAST, atLeastTimes, MAX_TIMES);
  }

  /**
   * Static constructor.
   * Construct a {@link Quantifier} of between times.
   * Regular expression pattern: {n,m}
   *
   * @param atLeastTimes at least times.
   * @param atMostTimes  at most times.
   * @return a {@link Quantifier} instance.
   */
  public static Quantifier ofBetween(int atLeastTimes, int atMostTimes) {
    return new Quantifier(CODE_AT_BETWEEN, atLeastTimes, atMostTimes);
  }

  private final int code;
  private final int atLeastTimes;
  private final int atMostTimes;

  /**
   * Private constructor.
   *
   * @param atLeastTimes at least times.
   * @param atMostTimes  at most times.
   * @throws IllegalArgumentException if times is over range.
   */
  private Quantifier(int code, int atLeastTimes, int atMostTimes) {
    if (atLeastTimes < MIN_TIMES) {
      throw new IllegalArgumentException("Quantifier is defined between " + MIN_TIMES + " and " + MAX_TIMES + ".");
    }
    if (atLeastTimes > atMostTimes) {
      throw new IllegalArgumentException("Quantifier is empty: {" + atLeastTimes + "," + atMostTimes + "}");
    }
    this.code = code;
    this.atLeastTimes = atLeastTimes;
    this.atMostTimes = atMostTimes;
  }

  /**
   * Gets at least times.
   *
   * @return at least times.
   */
  public int atLeastTimes() {
    return atLeastTimes;
  }

  /**
   * Gets at most times.
   *
   * @return at most times.
   */
  public int atMostTimes() {
    return atMostTimes;
  }

  /**
   * Such quantifiers can lead to naturally huge results,
   * because they have no upper bounds.
   * their pattern is '*', '+' and '{n,}'.
   */
  public boolean isCouldBeNaturalHuge() {
    return code == CODE_ZERO_OR_MORE || code == CODE_ONE_OR_MORE || code == CODE_AT_LEAST;
  }

  @Override
  public int weight() {
    return atMostTimes - atLeastTimes + 1;
  }

  /**
   * Randomly generate a times between {@link this#atLeastTimes}
   * and {@link this#atLeastTimes} (including both ends).
   *
   * @param random the random context
   * @return a random times.
   */
  public int random(Random random) {
    return atLeastTimes + random.nextInt(atMostTimes - atLeastTimes + 1);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o instanceof Quantifier) {
      Quantifier that = (Quantifier) o;
      return atLeastTimes == that.atLeastTimes && atMostTimes == that.atMostTimes;
    }
    return false;
  }

  @Override
  public int hashCode() {
    return Objects.hash(atLeastTimes, atMostTimes);
  }

  @Override
  public String toString() {
    switch (code) {
      case CODE_ZERO_OR_ONE:
        return "?";
      case CODE_ZERO_OR_MORE:
        return "*";
      case CODE_ONE_OR_MORE:
        return "+";
      case CODE_AT_EXACTLY:
        return "{" + atLeastTimes + "}";
      case CODE_AT_LEAST:
        return "{" + atLeastTimes + ",}";
      case CODE_AT_BETWEEN:
        return "{" + atLeastTimes + "," + atMostTimes + "}";
      default:
        throw new IllegalStateException("Undefined code: " + code);
    }
  }
}
