/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class JsonSymbols {
  private JsonSymbols() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  public static final char EOS = 0xffff;

  public static final char DIGIT_0 = 0x30;
  public static final char DIGIT_1 = 0x31;
  public static final char DIGIT_9 = 0x39;

  public static final char LETTER_U_A = 0x41;
  public static final char LETTER_U_E = 0x45;
  public static final char LETTER_U_F = 0x46;
  public static final char LETTER_L_A = 0x61;
  public static final char LETTER_L_B = 0x62;
  public static final char LETTER_L_E = 0x65;
  public static final char LETTER_L_F = 0x66;
  public static final char LETTER_L_L = 0x6c;
  public static final char LETTER_L_N = 0x6e;
  public static final char LETTER_L_R = 0x72;
  public static final char LETTER_L_S = 0x73;
  public static final char LETTER_L_T = 0x74;
  public static final char LETTER_L_U = 0x75;

  public static final char PLUS = 0x2b;
  public static final char MINUS = 0x2d;
  public static final char POINT = 0x2e;

  public static final char QUOTATION_MARK = 0x22;
  public static final char SOLIDUS = 0x2f;
  public static final char REVERSE_SOLIDUS = 0x5c;
  public static final char BACKSPACE = 0x08;
  public static final char FORM_FEED = 0x0c;
  public static final char SPACE = 0x20;
  public static final char HORIZONTAL_TAB = 0x09;
  public static final char LINE_FEED = 0x0a;
  public static final char CARRIAGE_RETURN = 0x0d;

  public static final char BEGIN_ARRAY = 0x5b;
  public static final char END_ARRAY = 0x5d;
  public static final char BEGIN_OBJECT = 0x7b;
  public static final char END_OBJECT = 0x7d;
  public static final char VALUE_SEPARATOR = 0x2c;
  public static final char NAME_SEPARATOR = 0x3a;


  public static final String S_NULL = "null";
  public static final String S_TRUE = "true";
  public static final String S_FALSE = "false";
  public static final String S_BEGIN_OBJECT = "{";
  public static final String S_END_OBJECT = "}";
  public static final String S_BEGIN_ARRAY = "[";
  public static final String S_END_ARRAY = "]";
  public static final String S_VALUE_SEPARATOR = ",";
  public static final String S_NAME_SEPARATOR = ":";
  public static final String S_END_DOCUMENT = "";

  public static final String S_POINT = ".";
  public static final String S_L_EXP = "e";
  public static final String S_U_EXP = "E";
  public static final String S_EMPTY_ARRAY = "[]";
  public static final String S_EMPTY_OBJECT = "{}";

  /**
   * JSON string escape characters map.
   * exp: \n -> \\n
   */
  public static final Map<Character, Character> ESCAPE_MAP;

  static {
    HashMap<Character, Character> map = new HashMap<>();
    map.put(QUOTATION_MARK, QUOTATION_MARK);
    map.put(REVERSE_SOLIDUS, REVERSE_SOLIDUS);
    map.put(BACKSPACE, LETTER_L_B);
    map.put(FORM_FEED, LETTER_L_F);
    map.put(LINE_FEED, LETTER_L_N);
    map.put(CARRIAGE_RETURN, LETTER_L_R);
    map.put(HORIZONTAL_TAB, LETTER_L_T);
    ESCAPE_MAP = Collections.unmodifiableMap(map);
  }

  /**
   * JSON string unescape characters map.
   * exp: \\n -> \n
   */
  public static final Map<Character, Character> UNESCAPE_MAP;

  static {
    HashMap<Character, Character> map = new HashMap<>();
    map.put(QUOTATION_MARK, QUOTATION_MARK);
    map.put(REVERSE_SOLIDUS, REVERSE_SOLIDUS);
    map.put(LETTER_L_B, BACKSPACE);
    map.put(LETTER_L_F, FORM_FEED);
    map.put(LETTER_L_N, LINE_FEED);
    map.put(LETTER_L_R, CARRIAGE_RETURN);
    map.put(LETTER_L_T, HORIZONTAL_TAB);
    UNESCAPE_MAP = Collections.unmodifiableMap(map);
  }

  /**
   * Get the specified character string or unicode string.
   *
   * @param c the specified character.
   * @return the specified character string or unicode string.
   */
  public static String display(char c) {
    if (Character.isWhitespace(c)) {
      return String.format("\\u%04x", (int) c);
    } else {
      return Character.toString(c);
    }
  }

  /**
   * Escapes the specified unescaped string to a JSON string.
   *
   * @param text the specified unescaped string.
   * @return the specified escaped string.
   */
  public static String escape(String text) {
    StringBuilder builder = new StringBuilder();
    char[] characters = text.toCharArray();
    for (char character : characters) {
      if (ESCAPE_MAP.containsKey(character)) {
        builder.append(REVERSE_SOLIDUS);
        builder.append(ESCAPE_MAP.get(character));
      } else {
        builder.append(character);
      }
    }
    return builder.toString();
  }

  /**
   * Unescapes the specified escaped string to a JSON string.
   *
   * @param text the specified escaped string.
   * @return the specified unescaped string.
   */
  public static String unescape(String text) {
    StringBuilder builder = new StringBuilder();
    char[] characters = text.toCharArray();
    boolean escape = false;
    for (char character : characters) {
      if (escape) {
        escape = false;
        if (ESCAPE_MAP.containsKey(character)) {
          builder.append(ESCAPE_MAP.get(character));
        } else {
          builder.append(REVERSE_SOLIDUS);
          builder.append(character);
        }
      } else if (character == REVERSE_SOLIDUS) {
        escape = true;
      } else {
        builder.append(character);
      }
    }
    return builder.toString();
  }
}
