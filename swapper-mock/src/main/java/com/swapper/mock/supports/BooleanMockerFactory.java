package com.swapper.mock.supports;

import com.swapper.mock.MockContext;
import com.swapper.mock.Mocker;
import com.swapper.mock.MockerFactory;

import java.lang.reflect.Type;

public enum BooleanMockerFactory implements MockerFactory {
  INSTANCE;

  @SuppressWarnings("unchecked")
  @Override
  public <T> Mocker<T> create(Type type) {
    return type == boolean.class || type == Boolean.class ? (Mocker<T>) BooleanMocker.INSTANCE : null;
  }

   enum BooleanMocker implements Mocker<Boolean> {
    INSTANCE;

    @Override
    public Boolean mock(MockContext context) {
      return context.random().nextBoolean();
    }
  }
}
