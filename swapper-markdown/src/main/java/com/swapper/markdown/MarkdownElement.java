package com.swapper.markdown;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

public abstract class MarkdownElement {
  private final int token_italic = 0;
  private final int token_bold = 0;
  private final int token_italic_and_bold = 0;
  private final int token_strikethrough = 0;
  private final int token_footnote = 0;
  private final int token_head = 0;
  private final int token_first_head_divider = 0;
  private final int token_second_head_divider = 0;

  private final int token_paragraph = 0;
  private final int token_divider = 0;

  private final int token_image = 0;
  private final int token_link = 0;
  private final int token_code = 0;
  private final int token_reference = 1;
  private final int token_inline_reference = 1;
  private final int token_ordered_list = 2;
  private final int token_unordered_list = 2;
  private final int token_table = 0;


  public abstract void writeToHtml(Writer writer) throws IOException;

  @Override
  public final String toString() {
    StringWriter writer = new StringWriter();
    try {
      writeToHtml(writer);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return writer.toString();
  }
}
