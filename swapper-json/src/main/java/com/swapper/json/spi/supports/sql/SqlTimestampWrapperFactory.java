/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.spi.supports.sql;

import com.swapper.json.*;

import java.lang.reflect.Type;
import java.sql.Timestamp;

/**
 * The {@link java.sql.Timestamp} wrapper factory.
 */
public enum SqlTimestampWrapperFactory implements JsonWrapperFactory {
  INSTANCE;

  @SuppressWarnings("unchecked")
  @Override
  public <T> JsonWrapper<T> create(JsonContext context, Type type) {
    return type == Timestamp.class ? (JsonWrapper<T>) Wrapper.INSTANCE : null;
  }

  public enum Wrapper implements JsonWrapper<Timestamp> {
    INSTANCE;

    @Override
    public JsonValue wrap(Timestamp value) {
      return value == null ? JsonValue.NULL : JsonString.valueOf(value.toString());
    }

    @Override
    public Timestamp unwrap(JsonValue value) {
      return JsonValue.NULL.equals(value) ? null : Timestamp.valueOf(value.toString());
    }
  }
}
