/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.supports;

import com.swapper.mock.MockContext;
import com.swapper.mock.Mocker;
import com.swapper.mock.MockerFactory;
import com.swapper.mock.config.MockLengthConfig;
import com.swapper.mock.config.MockSizeConfig;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.*;

public enum ObjectMockerFactory implements MockerFactory {
  INSTANCE;

  @SuppressWarnings("unchecked")
  @Override
  public <T> Mocker<T> create(Type type) {
    return type == Object.class ? (Mocker<T>) ObjectMocker.INSTANCE : null;
  }

  enum ObjectMocker implements Mocker<Object> {
    INSTANCE;

    @Override
    public Object mock(MockContext context) {
      switch (context.random().nextInt(13)) {
        case 0:
          return mockArray(context);
        case 1:
          return mockCollection(context);
        case 2:
          return mockMap(context);
        default:
          return mockCommon(context);
      }
    }

    private Object mockCommon(MockContext context) {
      switch (context.random().nextInt(11)) {
        case 0:
          return BooleanMockerFactory.BooleanMocker.INSTANCE.mock(context);
        case 1:
          return CharacterMockerFactory.CharacterMocker.INSTANCE.mock(context);
        case 2:
          return StringMockerFactory.StringMocker.INSTANCE.mock(context);
        default:
          return NumberMockerFactory.NumberMocker.INSTANCE.mock(context);
      }
    }

    private Object mockArray(MockContext context) {
      MockLengthConfig config = context.getConfig(MockLengthConfig.class);
      int length = config.random(context.random());
      Object array = Array.newInstance(Object.class, length);
      for (int i = 0; i < length; ++i) {
        Array.set(array, i, mockCommon(context));
      }
      return array;
    }

    private Object mockCollection(MockContext context) {
      MockSizeConfig config = context.getConfig(MockSizeConfig.class);
      int size = config.random(context.random());
      Collection<Object> collection = new ArrayList<>(size);
      for (int i = 0; i < size; ++i) {
        collection.add(mockCommon(context));
      }
      return collection;
    }

    private Object mockMap(MockContext context) {
      MockSizeConfig config = context.getConfig(MockSizeConfig.class);
      int size = config.random(context.random());
      Map<Object, Object> map = new HashMap<>(size);
      for (int i = 0; i < size; ++i) {
        map.put(mockCommon(context), mockCommon(context));
      }
      return map;
    }
  }
}
