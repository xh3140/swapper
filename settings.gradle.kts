dependencyResolutionManagement {
  repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
  repositories {
    mavenCentral()
    maven("https://mirrors.huaweicloud.com/repository/maven/")
  }
}
rootProject.name = "swapper"
include(":swapper-math")
include(":swapper-reflect")
include(":swapper-regexp")
include(":swapper-mock")
include(":swapper-json")
include(":swapper-json-ktx")
include(":swapper-xml")
include(":swapper-xls")
include(":swapper-hls")
include(":swapper-markdown")
include(":swapper-dcm")
