package com.swapper.hls.tag;

import com.swapper.hls.internal.enumerated.MediaType;
import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaPlaylist)
public final class MediaTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-MEDIA";

  private MediaType mType;
  private String mUri;
  private String mGroupId;
  private String mLanguage;
  private String mAssocLanguage;
  private String mName;
  private String mDefault;
  private String mAutoSelect;
  private String mForced;
  private String mInStreamId;
  private String mCharacteristics;
  private String mChannels;

  public MediaTag() {
    super(TAG_NAME);
  }

  @Override
  public String toString() {
    return "";
  }
}
