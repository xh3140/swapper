/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.supports;

import com.swapper.mock.MockContext;
import com.swapper.mock.Mocker;
import com.swapper.mock.MockerFactory;
import com.swapper.mock.config.MockIntegerConfig;

import java.lang.reflect.Type;

public enum IntegerMockerFactory implements MockerFactory {
  INSTANCE;

  @SuppressWarnings("unchecked")
  @Override
  public <T> Mocker<T> create(Type type) {
    return type == int.class || type == Integer.class ? (Mocker<T>) IntegerMocker.INSTANCE : null;
  }

   enum IntegerMocker implements Mocker<Integer> {
    INSTANCE;

    @Override
    public Integer mock(MockContext context) {
      MockIntegerConfig config = context.getConfig(MockIntegerConfig.class);
      int[] range = config.getRange();
      double factor = context.random().nextDouble();
      return (int) (factor * (1L + range[1] - range[0]) + range[0]);
    }
  }
}
