package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaOrMasterPlaylist)
public final class IndependentSegmentsTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-INDEPENDENT-SEGMENTS";

  private IndependentSegmentsTag() {
    super(TAG_NAME);
  }

  public static IndependentSegmentsTag getInstance() {
    return SingletonHolder.INSTANCE;
  }

  public static final class SingletonHolder {
    private static final IndependentSegmentsTag INSTANCE = new IndependentSegmentsTag();
  }
}
