package com.swapper.json.experimental.struct;

import com.swapper.json.JsonValue;

import java.util.Set;

public class LinkedRedBlackTree {

  static int hash(String name) {
    int h = name.hashCode();
    return h ^ (h >>> 16);
  }

  static class Member {
    final int hash;
    final String name;
    JsonValue value;
    Member next;

    Member(int hash, String name, JsonValue value, Member next) {
      this.hash = hash;
      this.name = name;
      this.value = value;
      this.next = next;
    }

    public String getName() {
      return name;
    }

    public JsonValue getValue() {
      return value;
    }

    public JsonValue setValue(JsonValue newValue) {
      JsonValue oldValue = value;
      value = newValue;
      return oldValue;
    }
  }

  transient Member[] table;
  transient Set<Member> members;
  transient int size;
  transient int modCount;
  int threshold;
  final float loadFactor=0.75f;
}
