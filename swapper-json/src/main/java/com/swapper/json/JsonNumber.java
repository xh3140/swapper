/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

/**
 * The JSON number value implement.
 * Number basic type includes:
 * {@link java.lang.Byte},
 * {@link java.lang.Short},
 * {@link java.lang.Integer},
 * {@link java.lang.Long},
 * {@link java.lang.Float},
 * {@link java.lang.Double},
 * {@link java.math.BigInteger},
 * {@link java.math.BigDecimal}.
 */
public final class JsonNumber extends JsonValue {
  /**
   * Static constructor.
   * Construct a JSON number instance from a number.
   *
   * @param value a number instance.
   * @return a JSON number instance from a number.
   */
  public static JsonNumber valueOf(Number value) {
    return new JsonNumber(value);
  }

  /**
   * The number value.
   */
  private final Number proxy;

  /**
   * Public constructor.
   *
   * @param value a value of JSON number.
   * @throws NullPointerException     if value is null.
   * @throws IllegalArgumentException if value is NaN or Infinite.
   */
  public JsonNumber(Number value) {
    Objects.requireNonNull(value, "value");
    if (value instanceof Float) {
      if (((Float) value).isNaN() || ((Float) value).isInfinite()) {
        throw new IllegalArgumentException("Numeric value must be finite, but was " + value + ".");
      }
    } else if (value instanceof Double) {
      if (((Double) value).isNaN() || ((Double) value).isInfinite()) {
        throw new IllegalArgumentException("Numeric value must be finite, but was " + value + ".");
      }
    }
    proxy = value;
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Number}.
   *
   * @return the JSON value as a value of type {@link java.lang.Number}.
   * @see JsonValue#toNumber()
   */
  @Override
  public Number toNumber() {
    return proxy;
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Byte}.
   *
   * @return the JSON value as a value of type {@link java.lang.Byte}.
   * @see JsonValue#toByte()
   * @see JsonString#toByte()
   */
  @Override
  public Byte toByte() {
    return proxy instanceof Byte ? (Byte) proxy : proxy.byteValue();
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Short}.
   *
   * @return the JSON value as a value of type {@link java.lang.Short}.
   * @see JsonValue#toShort()
   * @see JsonString#toShort()
   */
  @Override
  public Short toShort() {
    return proxy instanceof Short ? (Short) proxy : proxy.shortValue();
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Integer}.
   *
   * @return the JSON value as a value of type {@link java.lang.Integer}.
   * @see JsonValue#toInteger()
   * @see JsonString#toInteger()
   */
  @Override
  public Integer toInteger() {
    return proxy instanceof Integer ? (Integer) proxy : proxy.intValue();
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Long}.
   *
   * @return the JSON value as a value of type {@link java.lang.Long}.
   * @see JsonValue#toLong()
   * @see JsonString#toLong()
   */
  @Override
  public Long toLong() {
    return proxy instanceof Long ? (Long) proxy : proxy.longValue();
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Float}.
   *
   * @return the JSON value as a value of type {@link java.lang.Float}.
   * @throws NumberFormatException if the string does not contain a parsable float.
   * @see JsonValue#toFloat()
   * @see JsonString#toFloat()
   */
  @Override
  public Float toFloat() {
    return proxy instanceof Float ? (Float) proxy : proxy.floatValue();
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Double}.
   *
   * @return the JSON value as a value of type {@link java.lang.Double}.
   * @throws NumberFormatException if the string does not contain a parsable double.
   * @see JsonValue#toDouble()
   * @see JsonString#toDouble()
   */
  @Override
  public Double toDouble() {
    return proxy instanceof Double ? (Double) proxy : proxy.doubleValue();
  }

  /**
   * Returns the JSON value as a value of type {@link java.math.BigInteger}.
   *
   * @return the JSON value as a value of type {@link java.math.BigInteger}.
   * @see JsonValue#toBigInteger()
   * @see JsonString#toBigInteger()
   */
  @Override
  public BigInteger toBigInteger() {
    return proxy instanceof BigInteger ? (BigInteger) proxy : new BigInteger(proxy.toString());
  }

  /**
   * Returns the JSON value as a value of type {@link java.math.BigDecimal}.
   *
   * @return the JSON value as a value of type {@link java.math.BigDecimal}.
   * @throws NumberFormatException if string is not a valid representation of a {@link java.math.BigDecimal}.
   * @see JsonValue#toBigDecimal()
   * @see JsonString#toBigDecimal()
   */
  @Override
  public BigDecimal toBigDecimal() {
    return proxy instanceof BigDecimal ? (BigDecimal) proxy : new BigDecimal(proxy.toString());
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.String}.
   *
   * @return the JSON value as a value of type {@link java.lang.String}.
   * @see Object#toString()
   * @see JsonValue#toString()
   */
  @Override
  public String toString() {
    return proxy.toString();
  }

  /**
   * Returns the JSON formatted string.
   *
   * @return the JSON formatted string.
   * @see JsonValue#toJson()
   */
  @Override
  public String toJson() {
    return proxy.toString();
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param other the reference object with which to compare.
   * @return true if this object is the same as the obj argument; false otherwise.
   * @see Object#equals(Object)
   */
  @Override
  public boolean equals(Object other) {
    if (this == other) return true;
    if (other instanceof JsonNumber) {
      JsonNumber that = (JsonNumber) other;
      return proxy.equals(that.proxy);
    }
    return false;
  }

  /**
   * Gets the hash code of the current instance.
   *
   * @return the hash code of the current instance.
   * @see Object#hashCode()
   */
  @Override
  public int hashCode() {
    return proxy.hashCode();
  }
}
