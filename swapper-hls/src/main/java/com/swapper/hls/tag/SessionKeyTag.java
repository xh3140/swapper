package com.swapper.hls.tag;

import com.swapper.hls.internal.enumerated.KeyMethod;
import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaPlaylist)
public final class SessionKeyTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-SESSION-KEY";

  private KeyMethod mMethod;
  private String mUri;
  private Integer mVi;
  private String mKeyFormat;
  private String mKeyFormatVersions;

  public SessionKeyTag() {
    super(TAG_NAME);
  }


  @Override
  public String toString() {
    return "";
  }
}
