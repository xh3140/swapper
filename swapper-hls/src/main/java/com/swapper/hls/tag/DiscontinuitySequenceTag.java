package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaPlaylist)
public final class DiscontinuitySequenceTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-DISCONTINUITY-SEQUENCE";

  private final Integer mNumber;

  public DiscontinuitySequenceTag(Integer number) {
    super(TAG_NAME);
    mNumber = number;
  }

  public Integer number() {
    return mNumber;
  }
}
