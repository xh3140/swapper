/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.test;

import com.swapper.json.JsonContext;
import com.swapper.json.JsonDocument;
import com.swapper.json.JsonObject;
import com.swapper.json.test.bean.BooleansBean;
import com.swapper.json.test.bean.NumberArrayBean;
import com.swapper.json.test.bean.NumberListBean;
import com.swapper.json.test.bean.NumberSetBean;
import com.swapper.mock.MockContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public final class BeansTest {
  public static final class InnerArgumentsProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      List<Arguments> arguments = new ArrayList<>();
      arguments.add(Arguments.of(BooleansBean.class));
      arguments.add(Arguments.of(NumberArrayBean.class));
      arguments.add(Arguments.of(NumberListBean.class));
      arguments.add(Arguments.of(NumberSetBean.class));
      return arguments.stream();
    }
  }

  @ParameterizedTest
  @ArgumentsSource(InnerArgumentsProvider.class)
  public void test_class_json(Class<?> clazz) {
    JsonContext context = new JsonContext();
    Object raw = MockContext.defaultMock(clazz);
    JsonObject jsonObject = JsonObject.fromObject(context, raw);
    String json = jsonObject.toJson();
    Object gen = JsonDocument.fromJson(json).parseObject(context, clazz);
    Assertions.assertEquals(raw, gen);
  }
}
