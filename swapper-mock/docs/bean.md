# 问题：每次写测试类都会写equals、hashCode、toString方法，当字段多的时候，会很麻烦，但是他们的方法形式大多是一样的。

# 如何自动实现Bean的equals、hashCode、toString方法？

# 一种解决方案

通过继承的特性，在超类中实现equals、hashCode、toString方法，让派生类自由决定是否重写这些方法。

# 以toStrong为例

通过反射获取列的所有字段，并按一定的格式拼接为字符串。

```java
import java.lang.reflect.Field;

public abstract class AbstractBean {
  @Override
  public String toString() {
    return Helper.toString(this);
  }

  private static final class Helper {
    private Helper() {
      throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public static String toString(Object o) {
      if (o == null) return "null";
      Class<?> clazz = o.getClass();
      if (clazz.isArray()) {
        int iMax = Array.getLength(o) - 1;
        if (iMax == -1) return "[]";
        StringBuilder builder = new StringBuilder();
        builder.append('[');
        for (int i = 0; ; ++i) {
          builder.append(toString(Array.get(o, i)));
          if (i == iMax)
            return builder.append(']').toString();
          builder.append(", ");
        }
      }
      // If the class overrides the toString method, it is called directly.
      // 关键代码是就是try-catch这几行
      // 如果用getDeclaredField()方法，例如HashMap这些没有实现toString的方法会报错，死循环
      // 如何判断一个类或者它的超类是否重写Object的toString方法？
      // 可以通过getMethod()获取toString方法，它一定不会是空
      // 获取它的getDeclaringClass，就是实现/重写了toString的类
      // 如果这个类不是Object或者AbstractBean，则说明它重写了toString方法
      // 注意，排除AbstractBean是因为它的toString实现调用了本方法，会循环调用，造成死循环
      // 没有toString方法的列则通过后续的反射字段类实现
      try {
        Method method = clazz.getMethod("toString");
        Field field = Method.class.getDeclaredField("root");
        field.setAccessible(true);
        Class<?> rootClazz = ((Method) field.get(method)).getDeclaringClass();
        if (!Object.class.equals(rootClazz) && !AbstractBean.class.equals(rootClazz)) {
          return o.toString();
        }
      } catch (Exception e) {
        // do nothing
      }
      Field[] fields = Arrays.stream(clazz.getDeclaredFields())
        .filter(field -> !Modifier.isStatic(field.getModifiers()))
        .toArray(Field[]::new);
      int iMax = fields.length - 1;
      if (iMax == -1) return o.toString();
      StringBuilder builder = new StringBuilder();
      builder.append(clazz.getSimpleName()).append('{');
      for (int i = 0; ; ++i) {
        Field field = fields[i];
        field.setAccessible(true);
        builder.append(field.getName()).append("=");
        try {
          Class<?> type = field.getType();
          if (type == char.class || type == Character.class) {
            builder.append('\'').append(toString(field.get(o))).append('\'');
          } else if (type == String.class) {
            builder.append('"').append(toString(field.get(o))).append('"');
          } else {
            builder.append(toString(field.get(o)));
          }
        } catch (IllegalAccessException e) {
          e.printStackTrace();
        }
        if (i == iMax)
          return builder.append('}').toString();
        builder.append(", ");
      }
    }
  }
}
```



