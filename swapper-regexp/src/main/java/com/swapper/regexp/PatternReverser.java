/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.regexp;

import java.util.Random;

/**
 * The regular expression reverse generator.
 * Generate a random string that matches the regular expression.
 */
public final class PatternReverser {
  private final PatternNode root;
  private int hugeQuantifierLimit = 8;
  private boolean negatedWithinGraph = true;

  /**
   * The random number generator holder.
   * Save a singleton instance.
   */
  private static final class RandomNumberGeneratorHolder {
    private static final Random random = new Random();

    private RandomNumberGeneratorHolder() {
      throw new UnsupportedOperationException(getClass().getSimpleName());
    }
  }

  /**
   * Constructor a {@link PatternReverser} instance.
   *
   * @param pattern the regular expression pattern.
   * @throws NullPointerException if {@code pattern} is null.
   */
  public PatternReverser(String pattern) {
    root = PatternParser.parse(this, pattern);
  }

  /**
   * Generate a random string that matches the regular expression.
   *
   * @return a random string that matches the regular expression.
   */
  public String reverse() {
    return reverse(RandomNumberGeneratorHolder.random);
  }

  /**
   * Generate a random string that matches the regular expression.
   *
   * @param random the random context.
   * @return a random string that matches the regular expression.
   */
  public String reverse(Random random) {
    return root.random(random);
  }

  /**
   * Such quantifiers can lead to naturally huge results,
   * because they have no upper bounds.
   * their pattern is '*', '+' and '{n,}'.
   *
   * @return set quantifiers upper limit.
   * @see #setHugeQuantifierLimit(int)
   * @see Quantifier#isCouldBeNaturalHuge()
   */
  public int getHugeQuantifierLimit() {
    return hugeQuantifierLimit;
  }

  /**
   * Such quantifiers can lead to naturally huge results,
   * because they have no upper bounds.
   * their pattern is '*', '+' and '{n,}'.
   * This method sets an upper bound on these quantifiers.
   *
   * @param hugeQuantifierLimit an upper bound.
   */
  public void setHugeQuantifierLimit(int hugeQuantifierLimit) {
    if (hugeQuantifierLimit < 0) {
      throw new IllegalArgumentException("Quantifier times must be a non-negative number!");
    }
    this.hugeQuantifierLimit = hugeQuantifierLimit;
  }

  /**
   * A few negated meta ('\D', '\S', '\W', '\H' and '\V') represent a wide range
   * and may generate many garbled characters.
   * This switch can keep their full range within ASCII graph characters([0x21-0x7e]).
   *
   * @return if {@code true}, it represents ranges within ASCII graph characters.
   */
  public boolean isNegatedWithinGraph() {
    return negatedWithinGraph;
  }

  /**
   * A few negated meta ('\D', '\S', '\W', '\H' and '\V') represent a wide range
   * and may generate many garbled characters.
   * This switch can keep their full range within ASCII graph characters([0x21-0x7e]).
   *
   * @param negatedWithinGraph if {@code true}, it represents ranges within ASCII graph characters.
   */
  public void setNegatedWithinGraph(boolean negatedWithinGraph) {
    this.negatedWithinGraph = negatedWithinGraph;
  }
}
