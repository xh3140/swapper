plugins {
  `java-library`
  `maven-publish`
}

group = Publication.group
version = Publication.version

java {
  sourceCompatibility = JavaVersion.toVersion(Versions.java)
  targetCompatibility = JavaVersion.toVersion(Versions.java)
}

dependencies {
  testImplementation(Dependencies.junit_jupiter_api)
  testRuntimeOnly(Dependencies.junit_jupiter_engine)
  testImplementation(Dependencies.junit_jupiter_params)
  testImplementation(Dependencies.junit_platform_suite)
}

tasks.getByName<Test>("test") {
  useJUnitPlatform()
}

publishing {
  publications {
    create<MavenPublication>("mavenJava") {
      groupId = Publication.group
      artifactId = Publication.artifactIds.regexp
      version = Publication.version
      from(components["java"])
    }
  }
}
