package com.swapper.json.temp;

import com.swapper.json.io.JsonReader;
import com.swapper.json.io.JsonWriter;

import java.io.IOException;

public class FloatAdapter implements TypeAdapter<Float> {
  @Override
  public Float read(JsonReader reader) throws IOException {
    int token = reader.peek();
    if (token == JsonReader.TOKEN_NULL) {
      reader.nextNull();
      return null;
    }
    String s = reader.nextNumberString();
    return Float.parseFloat(s);
  }

  @Override
  public void write(JsonWriter writer, Float value) {

  }
}
