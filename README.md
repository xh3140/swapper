# swapper

**swapper**是一系列工具的集合。

- swapper-reflect：Java反射工具。
- swapper-regexp：由正则表达式随机生成匹配字符串的工具。
- swapper-data：随机数据生成工具。
- swapper-json：JSON序列化与反序列化工具。
- swapper-json-ktx：swapper-json的kotlin拓展。
- swapper-xml：xml文件解析工具（未完成）。
- swapper-hls：m3u8文件解析工具（未完成）。
