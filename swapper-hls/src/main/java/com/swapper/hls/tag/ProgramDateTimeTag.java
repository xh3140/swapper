package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

import java.util.Date;

@Tag(grouping = Tag.Grouping.MediaSegment)
public final class ProgramDateTimeTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-PROGRAM-DATE-TIME";

  private final Date mDate;

  public ProgramDateTimeTag(Date date) {
    super(TAG_NAME);
    if (date == null) {
      throw new NullPointerException("HLS tag(" + TAG_NAME + ") value is null.");
    }
    mDate = date;
  }

  public Date date() {
    return mDate;
  }
}
