#include <jni.h>
#include <stdio.h>
#include "com_swapper_math_TestNative.h"

JNIEXPORT void JNICALL Java_com_swapper_math_TestNative_helloFormCpp
  (JNIEnv *, jobject){
  printf("%s","Hello From Cpp.");
  }

JNIEXPORT jint JNICALL Java_com_swapper_math_TestNative_addByCpp
  (JNIEnv *, jobject, jint a, jint b){
  return a+b;
  }
