/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.json

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class JsonValueExTest {
  val a by lazy { 1 }

  @Test
  fun test_parseObject() {
    val jsonContext = JsonContext()
    val v1 = 123
    val jsonValue1 = JsonNumber.valueOf(v1)

    val v11 = jsonValue1.parseObject<Int>()
    val v12 = jsonValue1.parseObject<Int>(jsonContext)

    Assertions.assertEquals(v1, v11)
    Assertions.assertEquals(v11, v12)

    val v2 = "Swapper-JSON"
    val jsonValue2 = JsonString.valueOf(v2)

    val v21 = jsonValue2.parseObject<String>()
    val v22 = jsonValue2.parseObject<String>(jsonContext)

    Assertions.assertEquals(v2, v21)
    Assertions.assertEquals(v21, v22)
  }
}
