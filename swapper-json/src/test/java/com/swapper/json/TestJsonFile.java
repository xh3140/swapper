package com.swapper.json;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.swapper.json.io.JsonReader;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class TestJsonFile {
  private TestJsonFile() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  private static final List<String> RESOURCE_JSON_FILES;

  private static final FileFilter JSON_FILE_FILTER = pathname ->
    pathname.isDirectory() || pathname.getName().toLowerCase().endsWith(".json");

  static {
    URL url = TestJsonFile.class.getResource("location.txt");
    Objects.requireNonNull(url);
    File dir = new File(url.getFile()).getParentFile();
    RESOURCE_JSON_FILES = Collections.unmodifiableList(getJsonFiles(dir));
  }

  public static List<String> getResourceJsonFiles() {
    return RESOURCE_JSON_FILES;
  }

  private static List<String> getJsonFiles(File dir) {
    Objects.requireNonNull(dir);
    File[] listFiles = dir.listFiles(JSON_FILE_FILTER);
    if (listFiles == null || listFiles.length == 0) {
      return Collections.emptyList();
    }
    List<String> list = new ArrayList<>();
    for (File file : listFiles) {
      if (file.isFile()) {
        list.add(file.getAbsolutePath());
      } else {
        list.addAll(getJsonFiles(file));
      }
    }
    return list;
  }
}
