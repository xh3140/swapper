package com.swapper.json.config;

import java.lang.reflect.Field;

public enum MemberNamingPolicy implements MemberNamingStrategy {
  /**
   * Get the field name without any conversion.
   * String nameOfSerial = "swapper" -> {"nameOfSerial":"swapper"}
   */
  DEFAULT {
    @Override
    public String translate(Field member) {
      return member.getName();
    }
  },
  /**
   * Convert field name to camel naming.
   * String NameOfSerial = "swapper" -> {"nameOfSerial":"swapper"}
   * String name_of_serial = "swapper" -> {"nameOfSerial":"swapper"}
   * String name.of.serial = "swapper" -> {"nameOfSerial":"swapper"}
   */
  CAMEL_CASE {
    @Override
    public String translate(Field member) {
      return null;
    }
  },
  LOWER_CASE {
    @Override
    public String translate(Field member) {
      return null;
    }
  },
  UPPER_CASE {
    @Override
    public String translate(Field member) {
      return null;
    }
  };
}
