package com.swapper.hls.internal.enumerated;

public enum KeyMethod {
  NONE, AES_128, SAMPLE_AES
}
