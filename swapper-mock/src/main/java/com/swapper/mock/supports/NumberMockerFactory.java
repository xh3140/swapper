/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.supports;

import com.swapper.mock.MockContext;
import com.swapper.mock.Mocker;
import com.swapper.mock.MockerFactory;
import com.swapper.mock.config.MockLongConfig;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Random;

public enum NumberMockerFactory implements MockerFactory {
  INSTANCE;

  @SuppressWarnings("unchecked")
  @Override
  public <T> Mocker<T> create(Type type) {
    return type == Number.class ? (Mocker<T>) NumberMocker.INSTANCE : null;
  }

  enum NumberMocker implements Mocker<Number> {
    INSTANCE;

    @Override
    public Number mock(MockContext context) {
      switch (context.random().nextInt(8)) {
        case 0:
          return ByteMockerFactory.ByteMocker.INSTANCE.mock(context);
        case 1:
          return ShortMockerFactory.ShortMocker.INSTANCE.mock(context);
        case 2:
          return IntegerMockerFactory.IntegerMocker.INSTANCE.mock(context);
        case 3:
          return LongMockerFactory.LongMocker.INSTANCE.mock(context);
        case 4:
          return FloatMockerFactory.FloatMocker.INSTANCE.mock(context);
        case 5:
          return DoubleMockerFactory.DoubleMocker.INSTANCE.mock(context);
        case 6:
          return BigIntegerMockerFactory.BigIntegerMocker.INSTANCE.mock(context);
        default:
          return BigDecimalMockerFactory.BigDecimalMocker.INSTANCE.mock(context);
      }
    }
  }
}
