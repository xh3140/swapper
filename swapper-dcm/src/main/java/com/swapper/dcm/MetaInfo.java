package com.swapper.dcm;

import java.util.Objects;

/**
 * https://dicom.nema.org/dicom/2013/output/chtml/part10/chapter_7.html
 *
 */
public enum MetaInfo {
  FILE_PREAMBLE(null,null,"File Preamble"),
  DICOM_PREFIX(null,null,"DICOM Prefix"),
  META_INFORMATION_GROUP_LENGTH(0x0002,0x0000, "Meta Information Group Length"),
  FILE_META_INFORMATION_VERSION(0x0002,0x0001,"File Meta Information Version"),
  MEDIA_STORAGE_SOP_CLASS_UID(0x0002,0x0002,"Media Storage SOP Class UID"),
  MEDIA_STORAGE_SOP_INSTANCE_UID(0x0002,0x0003,"Media Storage SOP Instance UID"),
  TRANSFER_SYNTAX_UID(0x0002,0x0010,"Transfer Syntax UID"),
  IMPLEMENTATION_CLASS_UID(0x0002,0x0012,"Implementation Class UID"),
  IMPLEMENTATION_VERSION_NAME(0x0002,0x0013,"Implementation Version Name"),
  SOURCE_APPLICATION_ENTITY_TITLE(0x0002,0x0016,"Source Application Entity Title"),
  PRIVATE_INFORMATION_CREATOR_UID(0x0002,0x0100,"Private Information Creator UID"),
  PRIVATE_INFORMATION(0x0002,0x0102,"Private Information");

  private final Integer tagGroup;
  private final Integer tagElement;
  private final String attributeName;

  MetaInfo(Integer tagGroup, Integer tagElement, String attributeName) {
    this.tagGroup = tagGroup;
    this.tagElement = tagElement;
    this.attributeName = Objects.requireNonNull(attributeName);
  }

  public Integer getTagGroup() {
    return tagGroup;
  }

  public Integer getTagElement() {
    return tagElement;
  }

  public String getAttributeName() {
    return attributeName;
  }
}
