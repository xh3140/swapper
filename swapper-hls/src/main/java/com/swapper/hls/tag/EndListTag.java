package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaPlaylist)
public final class EndListTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-ENDLIST";

  private EndListTag() {
    super(TAG_NAME);
  }

  public static EndListTag getInstance() {
    return SingletonHolder.INSTANCE;
  }

  public static final class SingletonHolder {
    private static final EndListTag INSTANCE = new EndListTag();
  }
}
