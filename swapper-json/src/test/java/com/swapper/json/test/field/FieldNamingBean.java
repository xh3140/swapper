package com.swapper.json.test.field;

import com.swapper.json.annotations.FieldName;
import com.swapper.json.config.MemberNamingStrategy;
import com.swapper.json.config.MemberNamingStrategyProvider;
import com.swapper.json.annotations.FieldNamingStrategySource;
import com.swapper.mock.bean.QuickDirtyBean;

@FieldNamingStrategySource(FieldNamingBean.P.class)
public class FieldNamingBean extends QuickDirtyBean {
  @FieldName("ss")
  private int anInt;

  //  @FieldNamingStrategySource(provider = null, policy = FieldNamingPolicy.LOWER_CASE_WITH_DOTS)
  private boolean my_name_is_swapper;

  private boolean isBean;

  private String keyOfMap;

  private short aShort;

  public static class P implements MemberNamingStrategyProvider {
    @Override
    public MemberNamingStrategy provideStrategy() {
      return field -> field.getName() + "___houzhui";
    }
  }
}
