/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * The abstraction of JSON values.
 * It reduces the number of layers of aggregation compared
 * to providing encapsulated classes directly.
 *
 * @see JsonNumber      JSON number
 * @see JsonString      JSON string
 * @see JsonDocument    JSON array and object
 * @see JsonArray       JSON array
 * @see JsonObject      JSON object
 */
public abstract class JsonValue {
  /**
   * The JSON null literal.
   * This value should be used for all JSON null values.
   */
  public static final JsonValue NULL = new JsonValue() {
    @Override
    public String toJson() {
      return JsonSymbols.S_NULL;
    }

    @Override
    public String toString() {
      return JsonSymbols.S_NULL;
    }
  };

  /**
   * The JSON true literal.
   * This value should be used for all JSON true values.
   */
  public static final JsonValue TRUE = new JsonValue() {
    @Override
    public Boolean toBoolean() {
      return Boolean.TRUE;
    }

    @Override
    public String toJson() {
      return JsonSymbols.S_TRUE;
    }

    @Override
    public String toString() {
      return JsonSymbols.S_TRUE;
    }
  };

  /**
   * The JSON false literal.
   * This value should be used for all JSON false values.
   */
  public static final JsonValue FALSE = new JsonValue() {
    @Override
    public Boolean toBoolean() {
      return Boolean.FALSE;
    }

    @Override
    public String toJson() {
      return JsonSymbols.S_FALSE;
    }

    @Override
    public String toString() {
      return JsonSymbols.S_FALSE;
    }
  };

  /**
   * Convert to a JSON formatted string.
   *
   * @return the JSON formatted string.
   */
  public abstract String toJson();

  /**
   * Returns true if the JSON value is of type JSON null.
   *
   * @return returns true if the JSON value is of type JSON null.
   */
  public final boolean isNull() {
    return this == NULL;
  }

  /**
   * Returns true if the JSON value is of type JSON boolean.
   *
   * @return returns true if the JSON value is of type JSON boolean.
   */
  public final boolean isBoolean() {
    return this == TRUE || this == FALSE;
  }

  /**
   * Returns true if the JSON value is of type JSON number.
   *
   * @return returns true if the JSON value is of type JSON number.
   */
  public final boolean isNumber() {
    return this instanceof JsonNumber;
  }

  /**
   * Returns true if the JSON value is of type JSON string.
   *
   * @return returns true if the JSON value is of type JSON string.
   */
  public final boolean isString() {
    return this instanceof JsonString;
  }

  /**
   * Returns true if the JSON value is of type JSON array.
   *
   * @return returns true if the JSON value is of type JSON array.
   */
  public final boolean isJsonArray() {
    return this instanceof JsonArray;
  }

  /**
   * Returns true if the JSON value is of type JSON object.
   *
   * @return returns true if the JSON value is of type JSON object.
   */
  public final boolean isJsonObject() {
    return this instanceof JsonObject;
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Boolean}.
   * It needs to be implemented in {@link JsonValue#TRUE}, {@link JsonValue#FALSE}
   * and {@link JsonString}.
   *
   * @return the JSON value as a value of type {@link java.lang.Boolean}.
   * @throws UnsupportedOperationException if unsupported to boolean.
   * @see JsonValue#TRUE#toBoolean()
   * @see JsonValue#FALSE#toBoolean()
   * @see JsonString#toBoolean()
   */
  public Boolean toBoolean() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Number}.
   * It needs to be implemented in {@link JsonNumber}.
   *
   * @return the JSON value as a value of type {@link java.lang.Number}.
   * @throws UnsupportedOperationException if unsupported to number.
   * @see JsonNumber#toNumber()
   */
  public Number toNumber() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Byte}.
   * It needs to be implemented in {@link JsonNumber} and {@link JsonString}.
   *
   * @return the JSON value as a value of type {@link java.lang.Byte}.
   * @throws UnsupportedOperationException if unsupported to byte.
   * @see JsonNumber#toByte()
   * @see JsonString#toByte()
   */
  public Byte toByte() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Byte}.
   * It needs to be implemented in {@link JsonString}.
   *
   * @param radix the radix to be used while parsing string.
   * @return the JSON value as a value of type {@link java.lang.Byte}.
   * @throws NumberFormatException         if the string does not contain a parsable byte.
   * @throws UnsupportedOperationException if unsupported to byte with radix.
   * @see JsonString#toByte(int)
   */
  public Byte toByte(int radix) {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Short}.
   * It needs to be implemented in {@link JsonNumber} and {@link JsonString}.
   *
   * @return the JSON value as a value of type {@link java.lang.Short}.
   * @throws UnsupportedOperationException if unsupported to short.
   * @see JsonNumber#toShort()
   * @see JsonString#toShort()
   */
  public Short toShort() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Short}.
   * It needs to be implemented in {@link JsonString}.
   *
   * @param radix the radix to be used while parsing string.
   * @return the JSON value as a value of type {@link java.lang.Short}.
   * @throws NumberFormatException         if the string does not contain a parsable short.
   * @throws UnsupportedOperationException if unsupported to short with radix.
   * @see JsonString#toShort(int)
   */
  public Short toShort(int radix) {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Integer}.
   * It needs to be implemented in {@link JsonNumber} and {@link JsonString}.
   *
   * @return the JSON value as a value of type {@link java.lang.Integer}.
   * @throws UnsupportedOperationException if unsupported to integer.
   * @see JsonNumber#toInteger()
   * @see JsonString#toInteger()
   */
  public Integer toInteger() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Integer}.
   * It needs to be implemented in {@link JsonString}.
   *
   * @param radix the radix to be used while parsing string.
   * @return the JSON value as a value of type {@link java.lang.Integer}.
   * @throws NumberFormatException         if the string does not contain a parsable int.
   * @throws UnsupportedOperationException if unsupported to integer with radix.
   * @see JsonString#toInteger(int)
   */
  public Integer toInteger(int radix) {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Long}.
   * It needs to be implemented in {@link JsonNumber} and {@link JsonString}.
   *
   * @return the JSON value as a value of type {@link java.lang.Long}.
   * @throws UnsupportedOperationException if unsupported to long.
   * @see JsonNumber#toLong()
   * @see JsonString#toLong()
   */
  public Long toLong() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Long}.
   * It needs to be implemented in {@link JsonString}.
   *
   * @param radix the radix to be used while parsing string.
   * @return the JSON value as a value of type {@link java.lang.Long}.
   * @throws NumberFormatException         if the string does not contain a parsable long.
   * @throws UnsupportedOperationException if unsupported to long with radix.
   * @see JsonString#toLong(int)
   */
  public Long toLong(int radix) {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Float}.
   * It needs to be implemented in {@link JsonNumber} and {@link JsonString}.
   *
   * @return the JSON value as a value of type {@link java.lang.Float}.
   * @throws NumberFormatException         if the string does not contain a parsable float.
   * @throws UnsupportedOperationException if unsupported to float.
   * @see JsonNumber#toFloat()
   * @see JsonString#toFloat()
   */
  public Float toFloat() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Double}.
   * It needs to be implemented in {@link JsonNumber} and {@link JsonString}.
   *
   * @return the JSON value as a value of type {@link java.lang.Double}.
   * @throws NumberFormatException         if the string does not contain a parsable double.
   * @throws UnsupportedOperationException if unsupported to double.
   * @see JsonNumber#toDouble()
   * @see JsonString#toDouble()
   */
  public Double toDouble() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.math.BigInteger}.
   * It needs to be implemented in {@link JsonNumber} and {@link JsonString}.
   *
   * @return the JSON value as a value of type {@link java.math.BigInteger}.
   * @throws UnsupportedOperationException if unsupported to big integer.
   * @see JsonNumber#toBigInteger()
   * @see JsonString#toBigInteger()
   */
  public BigInteger toBigInteger() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.math.BigInteger}.
   * It needs to be implemented in {@link JsonString}.
   *
   * @param radix the radix to be used while parsing string.
   * @return the JSON value as a value of type {@link java.math.BigInteger}.
   * @throws NumberFormatException         if string is not a valid representation of a {@link java.math.BigInteger}.
   * @throws UnsupportedOperationException if unsupported to big integer with radix.
   * @see JsonString#toBigInteger(int)
   */
  public BigInteger toBigInteger(int radix) {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.math.BigDecimal}.
   * It needs to be implemented in {@link JsonNumber} and {@link JsonString}.
   *
   * @return the JSON value as a value of type {@link java.math.BigDecimal}.
   * @throws NumberFormatException         if string is not a valid representation of a {@link java.math.BigDecimal}.
   * @throws UnsupportedOperationException if unsupported to big decimal.
   * @see JsonNumber#toBigDecimal()
   * @see JsonString#toBigDecimal()
   */
  public BigDecimal toBigDecimal() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.String}.
   * It needs to be implemented in {@link JsonNumber}, {@link JsonString} and {@link JsonDocument}.
   * All subclasses must explicitly override this method.
   *
   * @return the JSON value as a value of type {@link java.lang.String}.
   * @throws UnsupportedOperationException if unsupported to string.
   * @see Object#toString()
   * @see JsonNumber#toString()
   * @see JsonString#toString()
   * @see JsonDocument#toString()
   */
  @Override
  public String toString() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  /**
   * Returns the JSON value as a value of type {@link JsonArray}.
   *
   * @return the JSON value as a value of type {@link JsonArray}.
   * @throws UnsupportedOperationException if unsupported to json array.
   */
  public final JsonArray toJsonArray() {
    if (this instanceof JsonArray) {
      return (JsonArray) this;
    } else {
      throw new UnsupportedOperationException(getClass().getSimpleName());
    }
  }

  /**
   * Returns the JSON value as a value of type {@link JsonObject}.
   *
   * @return the JSON value as a value of type {@link JsonObject}.
   * @throws UnsupportedOperationException if unsupported to json object.
   */
  public final JsonObject toJsonObject() {
    if (this instanceof JsonObject) {
      return (JsonObject) this;
    } else {
      throw new UnsupportedOperationException(getClass().getSimpleName());
    }
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Enum<E>}.
   * Convert according to the name attribute({@link java.lang.Enum#name()}) of the enumeration.
   *
   * @param enumType the type of enumeration.
   * @param <E>      the generics type of the enumeration.
   * @return the JSON value as a value of type {@link java.lang.Enum<E>}.
   * @see JsonValue#toString()
   */
  public final <E extends Enum<E>> Enum<E> toEnum(Class<E> enumType) {
    return Enum.valueOf(enumType, toString());
  }

  /**
   * Parses to an instance of the specified type.
   * Used default JSON context.
   *
   * @param type the specified type.
   * @param <T>  the specified generic type.
   * @return the instance of the specified type after parsing.
   */
  public final <T> T parseObject(Type type) {
    return parseObject(JsonContext.getDefault(), type);
  }

  /**
   * Parses to an instance of the specified type.
   *
   * @param context the JSON context.
   * @param type    the specified type.
   * @param <T>     the specified generic type.
   * @return the instance of the specified type after parsing.
   */
  public final <T> T parseObject(JsonContext context, Type type) {
    return (context != null ? context : JsonContext.getDefault()).<T>findWrapper(type).unwrap(this);
  }

  /**
   * Static constructor.
   * Construct a JSON value instance from an object.
   * Used default JSON context.
   *
   * @param value a object.
   * @return a JSON value instance from an object.
   */
  public static JsonValue valueOf(Object value) {
    return value == null ? NULL : JsonValue.valueOf(JsonContext.getDefault(), value, value.getClass());
  }

  /**
   * Static constructor.
   * Construct a JSON value instance from an object.
   * Used default JSON context.
   *
   * @param value a object.
   * @param type  the specified type.
   * @return a JSON value instance from an object.
   */
  public static JsonValue valueOf(Object value, Type type) {
    return value == null ? NULL : JsonValue.valueOf(JsonContext.getDefault(), value, type);
  }

  /**
   * Static constructor.
   * Construct a JSON value instance from an object.
   *
   * @param context the JSON context.
   * @param value   a object.
   * @return a JSON value instance from an object.
   */
  public static JsonValue valueOf(JsonContext context, Object value) {
    return value == null ? NULL : JsonValue.valueOf(context, value, value.getClass());
  }

  /**
   * Static constructor.
   * Construct a JSON value instance from an object.
   *
   * @param context the JSON context.
   * @param value   a object.
   * @param type    the specified type.
   * @return a JSON value instance from an object.
   */
  public static JsonValue valueOf(JsonContext context, Object value, Type type) {
    return value == null ? NULL
      : value instanceof JsonValue ? (JsonValue) value
      : value instanceof Boolean ? (Boolean) value ? TRUE : FALSE
      : value instanceof Number ? JsonNumber.valueOf((Number) value)
      : value instanceof String ? JsonString.valueOf((String) value)
      : (context != null ? context : JsonContext.getDefault()).findWrapper(type).wrap(value);
  }
}
