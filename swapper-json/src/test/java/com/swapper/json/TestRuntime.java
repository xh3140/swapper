package com.swapper.json;

import org.junit.jupiter.api.function.Executable;

public class TestRuntime {
  private TestRuntime() {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  public static void test(Executable executable) {
    Runtime runtime = Runtime.getRuntime();
    long startTime = System.nanoTime();
    long startMemory = runtime.totalMemory() - runtime.freeMemory();
    try {
      executable.execute();
    } catch (Throwable e) {
      e.printStackTrace();
    }
    long endTime = System.nanoTime();
    long endMemory = runtime.totalMemory() - runtime.freeMemory();
    System.out.printf("Time: %f ms, Memory: %f M.\n",
      (double) (endTime - startTime) / 1e6,
      (double) (endMemory - startMemory) / 1024 / 1024
    );
  }
}
