package com.swapper.json.temp;

import com.swapper.json.io.JsonReader;
import com.swapper.json.io.JsonWriter;

import java.io.IOException;

public interface TypeAdapter<T> {
  T read(JsonReader reader) throws IOException;

  void write(JsonWriter writer, T value);
}
