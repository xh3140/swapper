package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaSegment)
public final class InfTag extends AbstractTag {
  public static final String TAG_NAME = "#EXTINF";
  private final Float mDuration;
  private final String mTitle;

  public InfTag(Float duration) {
    this(duration, null);
  }

  public InfTag(Float duration, String title) {
    super(TAG_NAME);
    if (duration == null) {
      throw new NullPointerException("HLS tag(" + TAG_NAME + ") duration is null.");
    }
    if (duration < 0f) {
      throw new IllegalArgumentException("HLS tag(" + TAG_NAME + ") duration < 0f.");
    }
    if (title != null && (title.isEmpty() || title.trim().isEmpty())) {
      throw new IllegalArgumentException("HLS tag(" + TAG_NAME + ") title is empty or blank.");
    }
    mDuration = duration;
    mTitle = title;
  }

  public Float duration() {
    return mDuration;
  }

  public String title() {
    return mTitle;
  }

  @Override
  public String toString() {
    if (mTitle == null) {
      return String.format("%s:%f,\n", mName, mDuration);
    } else {
      return String.format("%s:%f,%s\n", mName, mDuration, mTitle);
    }
  }
}
