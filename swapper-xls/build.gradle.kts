plugins {
  `java-library`
}

group = Publication.group
version = Publication.version

java {
  sourceCompatibility = JavaVersion.toVersion(Versions.java)
  targetCompatibility = JavaVersion.toVersion(Versions.java)
}

dependencies {
  testImplementation(Dependencies.junit_jupiter_api)
  testRuntimeOnly(Dependencies.junit_jupiter_engine)
  testImplementation(Dependencies.junit_jupiter_params)
  testImplementation(Dependencies.junit_platform_suite)
  implementation("org.apache.poi:poi:5.2.2")
  implementation("org.apache.poi:poi-ooxml:5.2.2")
}

tasks.getByName<Test>("test") {
  useJUnitPlatform()
}
