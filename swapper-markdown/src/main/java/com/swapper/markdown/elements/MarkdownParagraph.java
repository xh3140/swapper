package com.swapper.markdown.elements;

import com.swapper.markdown.MarkdownElement;

import java.io.IOException;
import java.io.Writer;

public class MarkdownParagraph extends MarkdownElement {
  private final String _content;

  public MarkdownParagraph(String content) {
    _content = content;
  }

  @Override
  public void writeToHtml(Writer writer) throws IOException {
    writer.write("<p>");
    writer.write(_content);
    writer.write("</p>");
    writer.write(System.lineSeparator());
  }
}
