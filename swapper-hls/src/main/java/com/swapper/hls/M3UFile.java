package com.swapper.hls;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class M3UFile {
  private final File m3u8File;
  private final List<File> tsFileFlow;

  public M3UFile(File file) {
    if (file == null) {
      throw new IllegalArgumentException("The m3u8 file is null!");
    }
    if (!file.exists()) {
      throw new IllegalArgumentException("The m3u8 file is not exists: " + file.getAbsolutePath());
    }
    m3u8File = file;
    tsFileFlow = new ArrayList<File>();
  }

  public File getFile() {
    return m3u8File;
  }
}
