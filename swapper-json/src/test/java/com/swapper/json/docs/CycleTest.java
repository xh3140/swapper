/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.docs;

import com.swapper.json.JsonObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CycleTest {
  @Test
  public void test() {
    // serialization
    TestBean raw = new TestBean();
    JsonObject jo1 = JsonObject.fromObject(raw);
    String json = jo1.toJson();
    // deserialization
    JsonObject jo2 = JsonObject.fromJson(json);
    TestBean gen = jo2.parseObject(TestBean.class);
    // assertions
    Assertions.assertEquals(raw, gen);
  }
}
