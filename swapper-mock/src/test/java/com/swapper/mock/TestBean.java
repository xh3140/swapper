/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock;

import com.swapper.mock.annotations.*;
import com.swapper.mock.bean.QuickDirtyBean;
import com.swapper.regexp.PatternReverser;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Random;

/**
 * For code brevity, use the public modifier instead of the setter and getter.
 */
public final class TestBean extends QuickDirtyBean {
  public Object mObject;
  public Number mNumber;
  public Boolean mBoolean;

  @MockRange(chars = {'A', 'Z'})
  public Character mCharacter;

  @MockRange(bytes = {10, 100})
  public Byte mByte;

  public Short mShort;

  public Integer mInteger;

  public Long mLong;

  public Float mFloat;

  public Double mDouble;

  @MockBigInteger(unsigned = false, bits = 99)
  public BigInteger mBigInteger;

  @MockBigDecimal(bits = 88, scale = 5)
  public BigDecimal mBigDecimal;

  @MockString(pattern = "张三|李四|王五")
  public String mString;

  public int[] mIntegerArray;

  @MockString(pattern = "[0-9a-f]{16}")
  public Map<Integer, String> mMap;

  public Map<Integer, Integer> mMap2;

  public Map<Integer, String> mMap3;

  @MockerSource(MockerProvider1.class)
  public String[] mStringArray;

  private static final class MockerProvider1 implements MockerProvider<String[]> {

    @Override
    public Mocker<String[]> provideMocker() {
      return context -> {
        Random random = context.random();
        int length = random.nextInt(10);
        String[] strings = new String[length];
        PatternReverser reverser = new PatternReverser("\\w+");
        for (int i = 0; i < length; i++) {
          strings[i] = reverser.reverse(random);
        }
        return strings;
      };
    }
  }
}
