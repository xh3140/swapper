/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.experimental.io;

import com.swapper.json.*;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;


public final class _JsonWriter implements Flushable, Closeable {
  private final Writer writer;

  public static void write(Writer out, JsonDocument document) {
    try {
      new _JsonWriter(out).write(document);
    } catch (IOException e) {
      throw new JsonIOException(e);
    }
    try {
      out.flush();
    } catch (IOException e) {
      throw new JsonIOException(e);
    }
    try {
      out.close();
    } catch (IOException e) {
      throw new JsonIOException(e);
    }
  }

  public _JsonWriter(Writer out) {
    writer = Objects.requireNonNull(out);
  }

  public void write(JsonDocument document) throws IOException {
    if (document instanceof JsonArray) {
      writeArray((JsonArray) document);
    } else if (document instanceof JsonObject) {
      writeObject((JsonObject) document);
    } else {
      throw new IllegalStateException("JsonDocument must be JsonArray or JsonObject.");
    }
  }

  private void writeValue(JsonValue value) throws IOException {
    if (value instanceof JsonArray) {
      writeArray((JsonArray) value);
    } else if (value instanceof JsonObject) {
      writeObject((JsonObject) value);
    } else {
      writer.append(value.toJson());
    }
  }

  private void writeArray(JsonArray array) throws IOException {
    int iMax = array.size() - 1;
    if (iMax == -1) {
      writer.append(JsonSymbols.S_EMPTY_ARRAY);
      return;
    }
    writer.append(JsonSymbols.BEGIN_ARRAY);
    for (int i = 0; ; ++i) {
      writeValue(array.get(i));
      if (i == iMax) {
        writer.append(JsonSymbols.END_ARRAY);
        return;
      }
      writer.append(JsonSymbols.VALUE_SEPARATOR);
    }
  }

  private void writeObject(JsonObject object) throws IOException {
    int iMax = object.size() - 1;
    if (iMax == -1) {
      writer.append(JsonSymbols.S_EMPTY_OBJECT);
      return;
    }
    Iterator<Map.Entry<String, JsonValue>> iterator = object.members().iterator();
    writer.append(JsonSymbols.BEGIN_OBJECT);
    for (int i = 0; ; ++i) {
      Map.Entry<String, JsonValue> member = iterator.next();
      writer.append(JsonSymbols.QUOTATION_MARK)
        .append(member.getKey())
        .append(JsonSymbols.QUOTATION_MARK)
        .append(JsonSymbols.NAME_SEPARATOR);
      writeValue(member.getValue());
      if (i == iMax) {
        writer.append(JsonSymbols.END_OBJECT);
        return;
      }
      writer.append(JsonSymbols.VALUE_SEPARATOR);
    }
  }


  @Override
  public void flush() throws IOException {
    writer.flush();
  }


  @Override
  public void close() throws IOException {
    writer.close();
  }
  @Override
  public String toString() {
    return writer.toString();
  }
}
