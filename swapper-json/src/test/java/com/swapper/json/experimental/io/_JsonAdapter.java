package com.swapper.json.experimental.io;

import java.io.IOException;

public interface _JsonAdapter<T> {
  T read(_JsonReader reader) throws IOException;

  void write(_JsonWriter writer, T value) throws IOException;
}
