/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.regexp;

import java.util.Objects;

/**
 * The character range.
 * It's an element of character classes.
 *
 * @see CharacterClasses the character classes implement.
 */
final class CharacterRange implements Comparable<CharacterRange>, Weighty {
  private final int begin;
  private final int end;

  /**
   * Public constructor.
   *
   * @param codePoint the beginning and end character code point.
   */
  public CharacterRange(int codePoint) {
    if (!Character.isValidCodePoint(codePoint)) {
      throw new IllegalArgumentException("The specified code point is invalid.");
    }
    this.begin = this.end = codePoint;
  }

  /**
   * Public constructor.
   *
   * @param begin the beginning character code point.
   * @param end   the end character code point.
   */
  public CharacterRange(int begin, int end) {
    if (begin > end) {
      throw new IllegalArgumentException("The begin of the range is greater than the end.");
    }
    if (!Character.isValidCodePoint(begin) || !Character.isValidCodePoint(end)) {
      throw new IllegalArgumentException("The specified code point is invalid.");
    }
    this.begin = begin;
    this.end = end;
  }

  /**
   * Gets the beginning character code point.
   *
   * @return the beginning character code point.
   */
  public int begin() {
    return begin;
  }

  /**
   * Gets the end character code point.
   *
   * @return the end character code point.
   */
  public int end() {
    return end;
  }

  /**
   * Gets the offset character with respect to begin.
   *
   * @return the offset character with respect to begin.
   * @throws IndexOutOfBoundsException if offset out of range.
   */
  public int offset(int offset) {
    int offsetCodePoint = begin + offset;
    if (offsetCodePoint > end || offsetCodePoint < begin) {
      throw new IndexOutOfBoundsException("The offset character out of range.");
    }
    return offsetCodePoint;
  }

  @Override
  public int weight() {
    return end - begin + 1;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o instanceof CharacterRange) {
      CharacterRange that = (CharacterRange) o;
      return begin == that.begin && end == that.end;
    }
    return false;
  }

  @Override
  public int hashCode() {
    return Objects.hash(begin, end);
  }

  @Override
  public int compareTo(CharacterRange other) {
    int cmp = begin - other.begin;
    if (cmp == 0) {
      cmp = end - other.end;
    }
    return cmp;
  }

  @Override
  public String toString() {
    if (begin == end) {
      return String.format("[%d]", begin);
    } else {
      return String.format("[%d,%d]", begin, end);
    }
  }
}
