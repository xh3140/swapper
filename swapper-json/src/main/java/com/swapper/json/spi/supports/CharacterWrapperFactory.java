/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.spi.supports;

import com.swapper.json.*;

import java.lang.reflect.Type;

/**
 * The {@code char} and {@link Character} wrapper factory.
 */
public enum CharacterWrapperFactory implements JsonWrapperFactory {
  INSTANCE;

  @SuppressWarnings("unchecked")
  @Override
  public <T> JsonWrapper<T> create(JsonContext context, Type type) {
    return type == char.class || type == Character.class ? (JsonWrapper<T>) Wrapper.INSTANCE : null;
  }

  public enum Wrapper implements JsonWrapper<Character> {
    INSTANCE;

    @Override
    public JsonValue wrap(Character value) {
      return value == null ? JsonValue.NULL : JsonString.valueOf(Character.toString(value));
    }

    @Override
    public Character unwrap(JsonValue value) {
      return JsonValue.NULL.equals(value) ? null : value.toString().charAt(0);
    }
  }
}
