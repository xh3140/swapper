/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.test.bean;


import com.swapper.mock.bean.QuickDirtyBean;

import java.util.List;

/**
 * For code brevity, use the public modifier instead of the setter and getter.
 */
public class BooleansBean extends QuickDirtyBean {
  public boolean[] mBooleanArray1;

  public Boolean[] mBooleanArray2;

  public List<Boolean> mBooleanList;
}
