package com.swapper.json.jmh.bean;

import java.util.Map;

public class ChoiceQuestion {
  private String title;
  private Map<ChoiceItem, String> options;
  private String answers;
  private Shuffler shuffler;
}
