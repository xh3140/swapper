package com.swapper.json.experimental.lazy;

public interface Lazy<T> {
  T get();

  boolean isInitialized();
}
