package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaPlaylist)
public final class IFrameStreamInfTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-I-FRAME-STREAM-INF";

  private String mUri;

  public IFrameStreamInfTag() {
    super( TAG_NAME);
  }

  @Override
  public String toString() {
    return "";
  }
}
