plugins {
  `kotlin-jvm`
  `maven-publish`
}

group = Publication.group
version = Publication.version

dependencies {
  api(project(":swapper-json"))
  implementation(Dependencies.kotlin_stdlib)
  testImplementation(Dependencies.junit_jupiter_api)
  testRuntimeOnly(Dependencies.junit_jupiter_engine)
  testImplementation(Dependencies.junit_jupiter_params)
  testImplementation(Dependencies.junit_platform_suite)
}

tasks.getByName<Test>("test") {
  useJUnitPlatform()
}

publishing {
  publications {
    create<MavenPublication>("mavenJava") {
      groupId = Publication.group
      artifactId = Publication.artifactIds.json_ktx
      version = Publication.version
      from(components["java"])
    }
  }
}
