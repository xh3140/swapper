/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.spi.supports;

import com.swapper.json.TestSupportByArray;
import org.junit.jupiter.api.RepeatedTest;

import java.time.*;

public class TimeSupportsTest {
  @RepeatedTest(10)
  public void test_support_local_date() {
    TestSupportByArray.test(LocalDate.now());
  }

  @RepeatedTest(10)
  public void test_support_local_time() {
    TestSupportByArray.test(LocalTime.now());
  }

  @RepeatedTest(10)
  public void test_support_local_date_time() {
    TestSupportByArray.test(LocalDateTime.now());
  }

  @RepeatedTest(10)
  public void test_support_zoned_date_time() {
    TestSupportByArray.test(ZonedDateTime.now());
  }

  @RepeatedTest(10)
  public void test_support_offset_time() {
    TestSupportByArray.test(OffsetTime.now());
  }

  @RepeatedTest(10)
  public void test_support_offset_date_time() {
    TestSupportByArray.test(OffsetDateTime.now());
  }

  @RepeatedTest(10)
  public void test_support_instant() {
    TestSupportByArray.test(Instant.now());
  }

  @RepeatedTest(10)
  public void test_support_zone_id() {
    TestSupportByArray.test(ZoneId.systemDefault());
  }

  @RepeatedTest(10)
  public void test_support_year() {
    TestSupportByArray.test(Year.now());
  }

  @RepeatedTest(10)
  public void test_support_month() {
    TestSupportByArray.test(Month.of(5));
  }

  @RepeatedTest(10)
  public void test_support_day_of_week() {
    TestSupportByArray.test(DayOfWeek.of(3));
  }

  @RepeatedTest(10)
  public void test_support_year_month() {
    TestSupportByArray.test(YearMonth.now());
  }

  @RepeatedTest(10)
  public void test_support_month_day() {
    TestSupportByArray.test(MonthDay.now());
  }

  @RepeatedTest(10)
  public void test_support_duration() {
    TestSupportByArray.test(Duration.ofSeconds(999));
  }

  @RepeatedTest(10)
  public void test_support_period() {
    TestSupportByArray.test(Period.ofDays(999));
  }
}
