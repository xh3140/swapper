buildscript {
  repositories {
    mavenCentral()
    maven("https://mirrors.huaweicloud.com/repository/maven/")
  }
}

tasks.withType<JavaCompile> {
  options.encoding = "UTF-8"
  sourceCompatibility = Versions.java
  targetCompatibility = Versions.java
}

tasks.register<Delete>("clean") {
  delete(rootProject.buildDir)
}

tasks.wrapper {
  gradleVersion = Versions.gradle
  distributionType = Wrapper.DistributionType.ALL
}
