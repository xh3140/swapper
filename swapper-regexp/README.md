# swapper-regexp

[![Platform & Version Support](https://img.shields.io/badge/Java-1.8+-green.svg)]()
[![License](https://img.shields.io/badge/License-Apache2.0-blue.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)

## 一、介绍

**swapper-regexp**提供了一个由正则表达式逆向随机生成匹配字符串的工具。

## 二、引用

#### 在gradle项目中引用

```groovy
repositories {
  maven { url "https://jitpack.io" }
}

dependencies {
  implementation 'com.gitee.swapper:regexp:${version}'
}
```

## 三、语法支持

- 或语法`|`：`m|n`，匹配`m`或者`n`
- 组语法`()`：`(pattern)`，匹配`pattern`表达式
- 字符集语法`[]`
  - `[...]`：匹配`[...]`中的所有字符，例如`[abc]`，等价于`a|b|c`
  - `[m-n]`：表示一个闭区间，匹配区间内的所有字符，例如`[a-z]`匹配所有小写字母
  - `[^...]`：匹配不在`[...]`中的所有字符，例如`[^a-z]`匹配所有不是小写字母的字符
- 转义
  - `\x`：后2个字符均为16进制数(`[0-9A-Fa-f]`)时生效，例如`\x41`匹配字符`A`
  - `\u`：后4个字符均为16进制数(`[0-9A-Fa-f]`)时生效，例如`\u0061`匹配字符`a`
- 元字符
  - `.`：任何非`\r`和`\n`的字符，等价于`[^\r\n]`
  - `\d`：数字字符，等价于`[0-9]`
  - `\D`：非数字字符，等价于`[^0-9]`
  - `\s`：空白字符，等价于`[\t-\r\x20]`
  - `\S`：非空白字符，等价于`[^\t-\r\x20]`
  - `\w`：数字、字母、下划线字符，等价于`[0-9A-F_a-f]`
  - `\W`：非数字、字母、下划线字符，等价于`[^0-9A-F_a-f]`
- 量词
  - `?`：匹配`0`次或`1`次，等价于`{0,1}`
  - `*`：匹配`0`次或`更多`次，等价于`{0,}`
  - `+`：匹配`1`次或`更多`次，等价于`{1,}`
  - `{m}`：匹配`m`次，`m`为非负整数
  - `{m,}`：匹配`m`次或`更多`次，`m`为非负整数
  - `{m,n}`：至少匹配`m`次，至多匹配`n`次，`m`为非负整数，`n`不小于`m`

## 四、特殊处理

为了让生成字符串不显得累赘，仅仅支持了部分语法，并且对于一些自由度高的语法做了特殊的处理， 这些处理都封装在`RandomNodeConfig`对象中。

#### 1.量词的特殊处理

对于`* + {m,}`这3个量词，它们匹配的范围上界都是不限定的，如果不加以处理， 极大可能生成**巨大字符串**。 建议谨慎使用这3个量词。 通过`setPreferredLength(int)`方法，可以设置一个偏好长度`L`
，表示上界与下界的差值，它的默认值是`16`。 如果你不想要这样的约束，你可以将它设置为`Integer.MAX_VALUE`。

- `*`：等价于`{0,L}`
- `+`：等价于`{1,L+1}`
- `{m,}`：等价于`{m,m+L}`

#### 2.元字符的特殊处理

`. \D \S \W`这4个元字符，由于他们的表示范围太大， 导致绝大多数时候它们生成的字符串都是不理想的。 建议谨慎使用这4个元字符。 通过`setMetaNegatedUniversal(int)`方法，可以设置这4个元字符的全集，
缩小它们的范围。 有3个全集范围`UNIVERSAL_WHOLE`、`UNIVERSAL_ASCII`、`UNIVERSAL_ASCII_DISPLAY`
分别表示`[\u0000-\uffff]`、`[\u0000-\u007f]`、`[\u0020-\u007e]`。 如果你不想要这样的约束，你可以将它设置为`UNIVERSAL_WHOLE`。

## 五、最佳实践

正则表达式描述了一种字符串匹配的模式， 使用正则表达式时，更多的是一种“泛化”的使用，希望对于所有的可能都能匹配。 但作为它的逆操作，这样的方式可能会得到不理想的结果。 所以在使用正则表达式生成字符串时，更建议将它“具体化”。

例如打算生成一个整数：

- 坏的示范：`\d+`
- 好的示范：`0|[1-9]\d{0,8}`

## 六、均匀分布

对于随机生成来说，我们希望的是对于每一种可能的结果生成概率都是相等的。

例如对于某次测试的10000次生成来说：

当概率不相等时：

- `[aaaaabbbc]`：`{a=55329, b=33378, c=11293}`
- `a|b|c|d|e`：`{a=19753, b=19989, c=20254, d=20012, e=19992}`
- `0|[5-8]|[a-c]`：`{0=33237, a=11304, b=11083, c=10949, 5=8352, 6=8322, 7=8375, 8=8378}`
- `[a-c][d-e]|xyz`：`{cd=8248, bd=8350, ce=8201, be=8519, ad=8451, ae=8386, xyz=49845}`

当概率相等时：

- `[aaaaabbbc]`：`{a=33345, b=33518, c=33137}`
- `a|b|c|d|e`：`{a=19984, b=20029, c=19905, d=19993, e=20089}`
- `0|[5-8]|[a-c]`：`{0=12479, a=12423, b=12577, c=12439, 5=12487, 6=12647, 7=12565, 8=12383}`
- `[a-c][d-e]|xyz`：`{cd=14153, ce=14277, bd=14268, ad=14356, be=14459, ae=14246, xyz=14241}`

正如上面的测试那样，当你打算生成一个整数(`0|[1-9]\d*`)时，如果不是均匀分布， 大概有`50%`的结果都是`0`，这显然不是我们所期望的。

## 七、代码演示

使用`Junit5`测试

```java
public class RegexpReverseGeneratorTest {
  public static final class InnerArgumentsProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      List<Arguments> arguments = new ArrayList<>();
      arguments.add(Arguments.of("整数或浮点数", "(0|-?[1-9][0-9]*)(\\.[0-9]+)?((e|E)(\\+|-)?[0-9]+)?"));
      arguments.add(Arguments.of("RGB颜色", "#[0-9A-F]{6}"));
      arguments.add(Arguments.of("QQ号码", "[1-9]\\d{4,10}"));
      arguments.add(Arguments.of("手机号码", "(13[0-9]|14[0-9]|15[0-9]|16[0-9]|17[0-9]|18[0-9]|19[0-9])\\d{8}"));
      arguments.add(Arguments.of("中国大陆身份证号", "\\d{15}(\\d\\d[\\dxX])?"));
      return arguments.stream();
    }
  }

  @ParameterizedTest
  @ArgumentsSource(InnerArgumentsProvider.class)
  public void test(String title, String pattern) {
    Pattern p0 = Pattern.compile(pattern);
    Random random = new Random(System.currentTimeMillis());
    RegexpReverseGenerator rrg = new RegexpReverseGenerator(pattern);
    System.out.println(title + ": " + pattern);
    for (int i = 0; i < 5; ++i) {
      String s = rrg.generate(random);
      System.out.println(s);
      Assertions.assertTrue(p0.matcher(s).matches());
    }
    System.out.println();
  }
}
```

输出结果

```
整数或浮点数: (0|-?[1-9][0-9]*)(\.[0-9]+)?((e|E)(\+|-)?[0-9]+)?
-247225.427337582
180607959978274
-340942659.933758971018587e+949
-4409627867053.53882645903E+77592860383606095
-5482.8737497868E-464586489963

RGB颜色: #[0-9A-F]{6}
#14C2E0
#4B97B6
#D96DFB
#876C25
#D6427C

QQ号码: [1-9]\d{4,10}
448283
18575697973
207138267
240437565
60145619

手机号码: (13[0-9]|14[0-9]|15[0-9]|16[0-9]|17[0-9]|18[0-9]|19[0-9])\d{8}
13255826154
18443920840
17712021392
18640280478
15295979041

中国大陆身份证号: \d{15}(\d\d[\dxX])?
286702555826154
454392084077879200
392286640280478585
229597904110550
205084232940103413

```
