/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json;

import java.io.Reader;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Stream;

/**
 * JsonArray is an encapsulation of the array type in JSON.
 * The {@link JsonValueList} interface is implemented through static proxy mode.
 */
public final class JsonArray extends JsonDocument implements JsonValueList, RandomAccess {
  /**
   * Static constructor.
   * Construct a JSON array instance from a JSON string.
   *
   * @param json a JSON string.
   * @return an instance constructed from a JSON string.
   * @see JsonDocument#fromJson(String)
   */
  public static JsonArray fromJson(String json) {
    return JsonDocument.fromJson(json).toJsonArray();
  }

  /**
   * Static constructor.
   * Construct a JSON array instance from a JSON reader.
   *
   * @param reader a JSON reader.
   * @return an instance constructed from a JSON reader.
   * @see JsonDocument#fromJson(Reader)
   */
  public static JsonArray fromJson(Reader reader) {
    return JsonDocument.fromJson(reader).toJsonArray();
  }

  /**
   * Static constructor.
   * Construct a JSON array instance from an object.
   * Used default JSON context.
   *
   * @param value a object.
   * @return a JSON array instance from an object.
   * @see JsonValue#valueOf(Object)
   */
  public static JsonArray fromObject(Object value) {
    return JsonValue.valueOf(value).toJsonArray();
  }

  /**
   * Static constructor.
   * Construct a JSON array instance from an object.
   * Used default JSON context.
   *
   * @param value a object.
   * @param type  the specified type.
   * @return a JSON array instance from an object.
   * @see JsonValue#valueOf(Object, Type)
   */
  public static JsonArray fromObject(Object value, Type type) {
    return JsonValue.valueOf(value, type).toJsonArray();
  }

  /**
   * Static constructor.
   * Construct a JSON array instance from an object.
   *
   * @param context the JSON context.
   * @param value   a object.
   * @return a JSON array instance from an object.
   * @see JsonValue#valueOf(JsonContext, Object)
   */
  public static JsonArray fromObject(JsonContext context, Object value) {
    return JsonValue.valueOf(context, value).toJsonArray();
  }

  /**
   * Static constructor.
   * Construct a JSON array instance from an object.
   *
   * @param context the JSON context.
   * @param value   a object.
   * @param type    the specified type.
   * @return a JSON array instance from an object.
   * @see JsonValue#valueOf(JsonContext, Object, Type)
   */
  public static JsonArray fromObject(JsonContext context, Object value, Type type) {
    return JsonValue.valueOf(context, value, type).toJsonArray();
  }

  /**
   * Static proxy object.
   * The {@link JsonValueList} interface is implemented through static proxy mode.
   */
  private final List<JsonValue> proxy;

  /**
   * Constructs an empty JSON array with an initial capacity of ten.
   */
  public JsonArray() {
    proxy = new ArrayList<>();
  }

  /**
   * Constructs an empty JSON array with the specified initial capacity.
   *
   * @param initialCapacity the initial capacity of the JSON array.
   */
  public JsonArray(int initialCapacity) {
    proxy = new ArrayList<>(initialCapacity);
  }

  /**
   * Constructs a JSON array containing the values of the specified
   * collection, in the order they are returned by the collection's
   * iterator.
   *
   * @param values the collection whose values are to be placed into this JSON array.
   * @throws NullPointerException if the specified collection is null.
   */
  public JsonArray(Collection<? extends JsonValue> values) {
    proxy = new ArrayList<>(values);
  }

  /**
   * Returns the number of JSON values in this JSON array.
   * If this JSON array contains more than {@link Integer#MAX_VALUE} JSON values,
   * returns {@link Integer#MAX_VALUE}.
   *
   * @return the number of JSON values in this JSON array.
   */
  @Override
  public int size() {
    return proxy.size();
  }

  /**
   * Returns true if the JSON array is empty.
   * that is, there is no JSON value in the JSON array, the number of JSON values in the JSON array is 0.
   *
   * @return true if the JSON array is empty.
   */
  @Override
  public boolean isEmpty() {
    return proxy.isEmpty();
  }

  /**
   * Returns the index of the first occurrence of the specified JSON value in this JSON array,
   * or -1 if this JSON array does not contain the JSON value.
   *
   * @param value the JSON value to search for.
   * @return the index of the first occurrence of the specified JSON value in this JSON array,
   * or -1 if this JSON array does not contain the JSON value.
   */
  @Override
  public int indexOf(JsonValue value) {
    return proxy.indexOf(value);
  }

  /**
   * Returns the index of the last occurrence of the specified JSON value in this JSON array,
   * or -1 if this JSON array does not contain the JSON value.
   *
   * @param value the JSON value to search for.
   * @return the index of the last occurrence of the specified JSON value in this JSON array,
   * or -1 if this JSON array does not contain the JSON value.
   */
  @Override
  public int lastIndexOf(JsonValue value) {
    return proxy.indexOf(value);
  }

  /**
   * Gets the JSON value at the specified index of the JSON array.
   *
   * @param index the specified index.
   * @return the JSON value at the specified index in this JSON array.
   * @throws IndexOutOfBoundsException if index out of bounds.
   */
  @Override
  public JsonValue get(int index) {
    return proxy.get(index);
  }

  /**
   * Replaces the JSON value at the specified index in this JSON array with the specified JSON value.
   *
   * @param index index of the JSON value to replace.
   * @param value the JSON value to be stored at the specified index.
   * @throws IndexOutOfBoundsException if index out of bounds.
   */
  @Override
  public JsonValue set(int index, JsonValue value) {
    return proxy.set(index, value == null ? JsonValue.NULL : value);
  }

  /**
   * Appends the specified JSON value to the end of this JSON array.
   *
   * @param value JSON value to be appended to this JSON array.
   */
  @Override
  public void append(JsonValue value) {
    proxy.add(value == null ? JsonValue.NULL : value);
  }

  /**
   * Appends all the JSON values to the end of this JSON array.
   *
   * @param values the JSON values to be added to this JSON array.
   * @return true if this JSON array changed as a result of the call.
   * @throws NullPointerException if the specified values is null.
   */
  @Override
  public boolean appends(Collection<? extends JsonValue> values) {
    return proxy.addAll(values);
  }

  /**
   * Inserts the specified JSON value at the specified position in this JSON array.
   *
   * @param index index at which the specified JSON value is to be inserted.
   * @param value the JSON value to be inserted.
   * @throws IndexOutOfBoundsException if index out of bounds.
   */
  @Override
  public void insert(int index, JsonValue value) {
    proxy.add(index, value == null ? JsonValue.NULL : value);
  }

  /**
   * Inserts all the JSON values into this JSON array at the specified position.
   *
   * @param index  index at which to insert the first JSON value from the specified collection.
   * @param values the JSON values to be added to this JSON array.
   * @return true if this JSON array changed as a result of the call.
   * @throws IndexOutOfBoundsException if index out of bounds.
   * @throws NullPointerException      if the specified values is null.
   */
  @Override
  public boolean inserts(int index, Collection<? extends JsonValue> values) {
    return proxy.addAll(index, values);
  }

  /**
   * Removes the JSON value at the specified index in this JSON array.
   *
   * @param index the index of the JSON value to be removed.
   * @return the JSON value previously at the specified index.
   * @throws IndexOutOfBoundsException if index out of bounds.
   */
  @Override
  public JsonValue remove(int index) {
    return proxy.remove(index);
  }

  /**
   * Removes from this JSON array all of its JSON values that are contained in the
   * specified JSON values.
   *
   * @param values the specified JSON values to be removed from this JSON array.
   * @return {@code true} if this JSON array changed as a result of the call.
   * @throws NullPointerException if the specified values is null.
   * @see JsonValueList#removes(Collection)
   */
  @Override
  public boolean removes(Collection<? extends JsonValue> values) {
    return proxy.removeAll(values);
  }

  /**
   * Retains only the JSON values in this JSON array that are contained in the
   * specified JSON values.
   *
   * @param values the specified JSON values to be retained in this JSON array.
   * @return {@code true} if this JSON array changed as a result of the call.
   * @throws NullPointerException if the specified values is null.
   */
  @Override
  public boolean retains(Collection<? extends JsonValue> values) {
    return proxy.retainAll(values);
  }

  /**
   * Removes all the JSON values from this JSON array.
   * The JSON array will be empty after this call returns.
   */
  @Override
  public void clear() {
    proxy.clear();
  }

  /**
   * Used to support the for-each syntax.
   * Returns an iterator over the JSON value in this JSON array in proper sequence.
   *
   * @return an iterator over the JSON value in this JSON array in proper sequence.
   */
  @SuppressWarnings("all")
  @Override
  public Iterator<JsonValue> iterator() {
    return proxy.iterator();
  }

  /**
   * Returns a sequential {@code Stream} with this list as its source.
   *
   * @return a sequential {@code Stream} over the values in this list.
   */
  @Override
  public Stream<JsonValue> stream() {
    return proxy.stream();
  }

  /**
   * Returns an array containing all the JSON values in this JSON array in proper sequence.
   *
   * @return an array containing all the JSON values in this JSON array in proper sequence.
   */
  @Override
  public JsonValue[] toArray() {
    return proxy.toArray(new JsonValue[0]);
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param other the reference object with which to compare.
   * @return true if this object is the same as the obj argument; false otherwise.
   * @see Object#equals(Object)
   */
  @Override
  public boolean equals(Object other) {
    if (this == other) return true;
    if (other instanceof JsonArray) {
      JsonArray that = (JsonArray) other;
      return Objects.equals(proxy, that.proxy);
    }
    return false;
  }

  /**
   * Gets the hash code of the current instance.
   *
   * @return the hash code of the current instance.
   * @see Object#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hash(proxy);
  }
}
