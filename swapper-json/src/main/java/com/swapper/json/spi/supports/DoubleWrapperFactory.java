/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.json.spi.supports;

import com.swapper.json.*;

import java.lang.reflect.Type;

/**
 * The {@code double} and {@link Double} wrapper factory.
 */
public enum DoubleWrapperFactory implements JsonWrapperFactory {
  INSTANCE;

  @SuppressWarnings("unchecked")
  @Override
  public <T> JsonWrapper<T> create(JsonContext context, Type type) {
    return type == double.class || type == Double.class ? (JsonWrapper<T>) Wrapper.INSTANCE : null;
  }

  public enum Wrapper implements JsonWrapper<Double> {
    INSTANCE;

    @Override
    public JsonValue wrap(Double value) {
      return value == null ? JsonValue.NULL : JsonNumber.valueOf(value);
    }

    @Override
    public Double unwrap(JsonValue value) {
      return JsonValue.NULL.equals(value) ? null : value.toDouble();
    }
  }
}
