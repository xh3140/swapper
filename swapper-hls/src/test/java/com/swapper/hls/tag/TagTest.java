package com.swapper.hls.tag;

import com.swapper.hls.internal.enumerated.KeyMethod;
import org.junit.jupiter.api.Test;

import java.net.URI;

public class TagTest {

  @Test
  public void test() {
    System.out.println(M3UTag.getInstance());
    System.out.println(new VersionTag(5));
    System.out.println(new InfTag(3.14f, "abc"));
    System.out.println(new ByteRangeTag(1024, 128));
    System.out.println(DiscontinuityTag.getInstance());
    System.out.println(new KeyTag(
            KeyMethod.AES_128,
            URI.create("https://fanyi.youdao.com/"),
            "0x63d74c6e27222d33a607f343efee4f53",
            "identity",
            "1"
    ));
    System.out.println(new MapTag());
  }
}
