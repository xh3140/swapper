/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json;

import org.junit.jupiter.api.Assertions;

import java.lang.reflect.Array;
import java.util.function.Supplier;

public final class TestSupportByArray {
  private TestSupportByArray() {
    throw new UnsupportedOperationException(getClass().getName());
  }

  public static void test(Object component) {
    Class<?> componentType = component.getClass();
    Object rawArray = Array.newInstance(componentType, 1);
    Array.set(rawArray, 0, component);
    JsonArray rawJsonArray = JsonArray.fromObject(rawArray);
    String json = rawJsonArray.toJson();
    System.out.println(json);
    JsonArray genJsonArray = JsonArray.fromJson(json);
    Object genArray = genJsonArray.parseObject(rawArray.getClass());
    Object actual = Array.get(genArray, 0);
    Assertions.assertEquals(component, actual);
  }

  public static void test(Supplier<Object> supplier) {
    test(supplier.get());
  }
}
