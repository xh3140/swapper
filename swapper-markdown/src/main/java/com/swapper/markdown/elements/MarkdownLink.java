package com.swapper.markdown.elements;

import com.swapper.markdown.MarkdownElement;

import java.io.IOException;
import java.io.Writer;

public class MarkdownLink extends MarkdownElement {
  private String _name;
  private String _href;

  @Override
  public void writeToHtml(Writer writer) throws IOException {
    writer.write("<a href=\"");
    writer.write(_href);
    writer.write("\">");
    writer.write(_name);
    writer.write("</a>");
    writer.write(System.lineSeparator());
  }
}
