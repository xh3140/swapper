package com.swapper.mock;

@FunctionalInterface
public interface MockerProvider<T> {
  Mocker<T> provideMocker();
}
