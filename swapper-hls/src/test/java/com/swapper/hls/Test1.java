package com.swapper.hls;

import com.swapper.base.Scanner;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Test1 {

  @Test
  public void t() throws IOException {
    InputStream inputStream = Test1.class.getResourceAsStream("/f1.m3u8");
    assert inputStream != null;
    InputStreamReader reader = new InputStreamReader(inputStream);

    HLSTokenizer tokenizer = new HLSTokenizer(new Scanner(reader));

    for (int i = 0; i < 9999; i++) {
      HLSToken token = tokenizer.tokenize();
      if (i > 826) {
        System.out.println(i + "  " + token);
      }
    }
  }

}
