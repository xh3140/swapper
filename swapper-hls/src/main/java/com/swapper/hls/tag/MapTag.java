package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Attribute;
import com.swapper.hls.internal.info.Tag;

import java.net.URI;

@Tag(grouping = Tag.Grouping.MediaSegment)
public final class MapTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-MAP";

  public MapTag() {
    super(TAG_NAME);
  }

  @Attribute(necessity = Attribute.Necessity.Required)
  public static final class UriAttribute extends AbstractAttribute<URI> {
    public static final String ATTRIBUTE_NAME = "URI";

    private UriAttribute(URI value) {
      super(ATTRIBUTE_NAME, value);
    }

    @Override
    public URI verify(URI value) {
      if (value == null) {
        throw new NullPointerException("HLS tag(" + TAG_NAME + ") attribute(" + ATTRIBUTE_NAME + ") is null.");
      }
      return value;
    }

    @Override
    public String toString() {
      return mName + '=' + '"' + mValue + '"';
    }
  }

  @Attribute(necessity = Attribute.Necessity.Optional)
  public static final class ByteRangeAttribute extends AbstractAttribute<String> {
    public static final String ATTRIBUTE_NAME = "BYTERANGE";

    private ByteRangeAttribute(String value) {
      super(ATTRIBUTE_NAME, value);
    }

    @Override
    public String verify(String value) {
      return null;
    }
  }

  @Override
  public String toString() {
    return String.format("%s:%d\n", name());
  }
}
