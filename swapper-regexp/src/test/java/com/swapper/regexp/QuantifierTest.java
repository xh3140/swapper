/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.regexp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class QuantifierTest {
  @Test
  public void test_constructor_with_negative_times() {
    assertThrows(IllegalArgumentException.class, () -> Quantifier.ofExactly(-1));
  }

  @Test
  public void test_constructor_with_empty_times() {
    assertThrows(IllegalArgumentException.class, () -> Quantifier.ofBetween(5, 2));
  }

  @Test
  public void test_method_weight() {
    assertEquals(1, Quantifier.ofBetween(3, 3).weight());
    assertEquals(2, Quantifier.ofBetween(3, 4).weight());
    assertEquals(3, Quantifier.ofBetween(3, 5).weight());
    assertEquals(4, Quantifier.ofBetween(3, 6).weight());
    assertEquals(5, Quantifier.ofBetween(3, 7).weight());
  }
}
