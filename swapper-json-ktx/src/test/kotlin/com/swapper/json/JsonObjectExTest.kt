/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.json

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class JsonObjectExTest {

  data class Data(val name: String, val id: Int)

  @Test
  fun test_parseObject() {
    val data = Data("xh3140", 666)

    val jsonContext = JsonContext()
    val jsonObject = JsonObject.fromObject(jsonContext, data)

    val data1 = jsonObject.parseObject<Data>()
    val data2 = jsonObject.parseObject<Data>(jsonContext)

    Assertions.assertEquals(data, data1)
    Assertions.assertEquals(data1, data2)
  }

  @Test
  fun test_parseMap_parseMutableMap() {
    val map = mapOf("a" to 1, "b" to 2, "c" to 3)

    val jsonContext = JsonContext()
    val jsonObject = JsonObject.fromObject(jsonContext, map)

    val map1 = jsonObject.parseMap<String, Int>()
    val map2 = jsonObject.parseMap<String, Int>(jsonContext)
    val map3 = jsonObject.parseMutableMap<String, Int>()
    val map4 = jsonObject.parseMutableMap<String, Int>(jsonContext)

    for ((key, value) in map) {
      Assertions.assertTrue(map1.containsKey(key))
      Assertions.assertTrue(map2.containsKey(key))
      Assertions.assertTrue(map3.containsKey(key))
      Assertions.assertTrue(map4.containsKey(key))
      Assertions.assertEquals(value, map1[key])
      Assertions.assertEquals(value, map2[key])
      Assertions.assertEquals(value, map3[key])
      Assertions.assertEquals(value, map4[key])
    }
  }
}
