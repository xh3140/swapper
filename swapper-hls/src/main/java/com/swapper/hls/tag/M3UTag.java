package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.Basic)
public final class M3UTag extends AbstractTag {
  public static final String TAG_NAME = "#EXTM3U";

  private M3UTag() {
    super(TAG_NAME);
  }

  public static M3UTag getInstance() {
    return SingletonHolder.INSTANCE;
  }

  private static final class SingletonHolder {
    private static final M3UTag INSTANCE = new M3UTag();
  }
}
