/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json;

/**
 * JSON value wrapper.
 *
 * @param <T> the specified type.
 * @see JsonValue the JSON value implment.
 */
public interface JsonWrapper<T> {
  /**
   * Wraps a value of the specified type as JSON value.
   *
   * @param value a value of the specified type.
   * @return a JSON value from a value of the specified type.
   */
  JsonValue wrap(T value);

  /**
   * Unwrap a value of the specified type from JSON value.
   *
   * @param value a JSON value.
   * @return a value of the specified type from a JSON value.
   */
  T unwrap(JsonValue value);
}
