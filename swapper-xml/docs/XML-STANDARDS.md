### W3C XML Standards

| Standard | Draft / Proposal | Publish |
| ---- | ---- | ---- |
| [XML 1.0](http://www.w3.org/TR/1998/REC-xml-19980210) |  | 1998-02-10|
| [XML 1.0 (2.Ed)](http://www.w3.org/TR/2000/REC-xml-20001006) |  | 2000-10-06 |
| [XML 1.0 (3.Ed)](http://www.w3.org/TR/2004/REC-xml-20040204/) |  | 2004-02-04 |
| [XML 1.1](http://www.w3.org/TR/2004/REC-xml11-20040204/) |  | 2004-02-04 |
| [XML 1.1 (2.Ed)](http://www.w3.org/TR/xml11/) |  | 2006-08-16 |
| [XML 1.0 Namespaces](http://www.w3.org/TR/1999/REC-xml-names-19990114/) |  | 1999-01-14 |
| [XML 1.0 Namespaces SE](http://www.w3.org/TR/xml-names/) |  | 2004-03-04 |
| [XML 1.1 Namespaces](http://www.w3.org/TR/2004/REC-xml-names11-20040204/) |  | 2004-03-04 |
| [XML 1.1 Namespaces SE](http://www.w3.org/TR/xml-names11/) |  | 2006-08-16 |
| [XML Infoset](http://www.w3.org/TR/2001/REC-xml-infoset-20011024/) |  | 2001-10-24 |
| [XML Infoset (2.Ed)](http://www.w3.org/TR/xml-infoset/) |  | 2004-02-04 |
| [XML Base](http://www.w3.org/TR/xmlbase/) |  | 2001-06-27 |
| [XLink 1.0](http://www.w3.org/TR/xlink/) |  | 2001-06-27 |
| [XPointer Framework](http://www.w3.org/TR/2003/REC-xptr-framework-20030325/) |  | 2003-03-25 |
| [XPointer element() scheme](http://www.w3.org/TR/2003/REC-xptr-element-20030325/) |  | 2003-03-25 |
| [XPointer xmlns() scheme](http://www.w3.org/TR/2003/REC-xptr-xmlns-20030325/) |  | 2003-03-25 |
| [XInclude 1.0](http://www.w3.org/TR/2004/REC-xinclude-20041220/) |  | 2004-12-20 |
| [XInclude 1.0 SE](http://www.w3.org/TR/xinclude/) |  | 2006-11-15 |
| [XML Processing Model](http://www.w3.org/TR/proc-model-req/) | 2004-04-05 |  |
| [XMLHttpRequest Object](http://www.w3.org/TR/XMLHttpRequest/) | 2010-08-03 |  |