/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.experimental.safe;

public final class UnsafeCast {
  private UnsafeCast() {
    throw new UnsupportedOperationException(getClass().getName());
  }

  /**
   * Cast an object to a specified type used unsafe way.
   *
   * @param o   The object being cast.
   * @param <T> the specified type.
   * @return The cast object.
   * @throws ClassCastException If the object cannot be cast to the specified type.
   */
  @SuppressWarnings("unchecked")
  public static <T> T cast(Object o) {
    return (T) o;
  }
}
