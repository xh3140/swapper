package com.swapper.math;

import com.swapper.math.utils.BigDecimalUtils;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class BigDecimalUtilsTest {

  @Test
  public void test_sqrt() {
    BigDecimal sqrt = BigDecimalUtils.sqrt(BigDecimal.valueOf(5));
    System.out.println(sqrt);
  }
}
