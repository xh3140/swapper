/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.test.struct;

import com.swapper.json.JsonNumber;
import com.swapper.json.TestDataSource;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JsonNumberTest {

  @Test
  @DisplayName("should throw null pointer exception when use null construct.")
  public void test_constructor_use_null() {
    assertThrows(NullPointerException.class, () -> JsonNumber.valueOf(null));
  }

  @Test
  @DisplayName("should throw illegal argument exception when use nan and infinity construct.")
  public void test_constructor_use_nan_and_infinity() {
    assertThrows(IllegalArgumentException.class, () -> JsonNumber.valueOf(Float.NaN));
    assertThrows(IllegalArgumentException.class, () -> JsonNumber.valueOf(Float.POSITIVE_INFINITY));
    assertThrows(IllegalArgumentException.class, () -> JsonNumber.valueOf(Float.NEGATIVE_INFINITY));
    assertThrows(IllegalArgumentException.class, () -> JsonNumber.valueOf(Double.NaN));
    assertThrows(IllegalArgumentException.class, () -> JsonNumber.valueOf(Double.POSITIVE_INFINITY));
    assertThrows(IllegalArgumentException.class, () -> JsonNumber.valueOf(Double.NEGATIVE_INFINITY));
  }

  @RepeatedTest(10)
  @DisplayName("should equal constructor value and toNumber method return value.")
  public void test_method_toNumber() {
    Number number = TestDataSource.generate(Number.class);
    JsonNumber jsonNumber = JsonNumber.valueOf(number);
    assertEquals(number, jsonNumber.toNumber());
  }

  @RepeatedTest(10)
  @DisplayName("should equal constructor byte value and toByte method return value.")
  public void test_method_toByte() {
    Number number = TestDataSource.generate(Number.class);
    JsonNumber jsonNumber = JsonNumber.valueOf(number);
    assertEquals(number.byteValue(), jsonNumber.toByte());
  }

  @RepeatedTest(10)
  @DisplayName("should equal constructor short value and toShort method return value.")
  public void test_method_toShort() {
    Number number = TestDataSource.generate(Number.class);
    JsonNumber jsonNumber = JsonNumber.valueOf(number);
    assertEquals(number.shortValue(), jsonNumber.toShort());
  }

  @RepeatedTest(10)
  @DisplayName("should equal constructor int value and toInteger method return value.")
  public void test_method_toInteger() {
    Number number = TestDataSource.generate(Number.class);
    JsonNumber jsonNumber = JsonNumber.valueOf(number);
    assertEquals(number.intValue(), jsonNumber.toInteger());
  }

  @RepeatedTest(10)
  @DisplayName("should equal constructor long value and toLong method return value.")
  public void test_method_toLong() {
    Number number = TestDataSource.generate(Number.class);
    JsonNumber jsonNumber = JsonNumber.valueOf(number);
    assertEquals(number.longValue(), jsonNumber.toLong());
  }

  @RepeatedTest(10)
  @DisplayName("should equal constructor float value and toFloat method return value.")
  public void test_method_toFloat() {
    Number number = TestDataSource.generate(Number.class);
    JsonNumber jsonNumber = JsonNumber.valueOf(number);
    assertEquals(number.floatValue(), jsonNumber.toFloat());
  }

  @RepeatedTest(10)
  @DisplayName("should equal constructor double value and toDouble method return value.")
  public void test_method_toDouble() {
    Number number = TestDataSource.generate(Number.class);
    JsonNumber jsonNumber = JsonNumber.valueOf(number);
    assertEquals(number.doubleValue(), jsonNumber.toDouble());
  }

  @RepeatedTest(10)
  @DisplayName("should equal constructor value and toBigInteger method return value.")
  public void test_method_toBigInteger() {
    BigInteger number = TestDataSource.generate(BigInteger.class);
    JsonNumber jsonNumber = JsonNumber.valueOf(number);
    assertEquals(number, jsonNumber.toBigInteger());
  }

  @RepeatedTest(10)
  @DisplayName("should equal constructor value and toBigDecimal method return value.")
  public void test_method_toBigDecimal() {
    BigDecimal number = TestDataSource.generate(BigDecimal.class);
    JsonNumber jsonNumber = JsonNumber.valueOf(number);
    assertEquals(number, jsonNumber.toBigDecimal());
  }

  @RepeatedTest(10)
  @DisplayName("should equal constructor value toString and toString method return value.")
  public void test_method_toString() {
    Number number = TestDataSource.generate(Number.class);
    JsonNumber jsonNumber = JsonNumber.valueOf(number);
    assertEquals(number.toString(), jsonNumber.toString());
  }
}
