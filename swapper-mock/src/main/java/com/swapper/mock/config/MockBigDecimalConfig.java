/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.config;

import com.swapper.mock.annotations.MockBigDecimal;
import com.swapper.mock.MockConfig;

import java.lang.annotation.Annotation;

public final class MockBigDecimalConfig extends MockConfig {
  private boolean unsigned = true;
  private int bits = Double.SIZE + 1;
  private int scale = Double.SIZE + 1;

  public MockBigDecimalConfig() {
  }

  public MockBigDecimalConfig(boolean unsigned, int bits, int scale) {
    setUnsigned(unsigned);
    setBits(bits);
    setScale(scale);
  }

  @Override
  public void withAnnotationConfig(Annotation annotation) {
    if (annotation instanceof MockBigDecimal) {
      MockBigDecimal config = (MockBigDecimal) annotation;
      setUnsigned(config.unsigned());
      setBits(config.bits());
    }
  }

  public boolean isUnsigned() {
    return unsigned;
  }

  public int getBits() {
    return bits;
  }

  public int getScale() {
    return scale;
  }

  public void setUnsigned(boolean unsigned) {
    this.unsigned = unsigned;
  }

  public void setBits(int bits) {
    if (bits < 1) {
      throw new IllegalArgumentException("The bits of an integer must be greater than 0.");
    }
    this.bits = bits;
  }

  public void setScale(int scale) {
    if (scale < 0) {
      throw new IllegalArgumentException("The scale of an decimal must be non-negative.");
    }
    this.scale = scale;
  }
}
