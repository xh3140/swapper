/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.regexp;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PatternParserTest {
  @ParameterizedTest
  @ValueSource(strings = {"a?", "a*", "a+", "a{5}", "a{5,}", "a{5,8}"})
  public void test_parse_quantifier(String pattern) {
    PatternReverser context = new PatternReverser("context");
    PatternNode unit = PatternParser.parse(context, pattern);
    assertTrue(unit instanceof QuantifierNode);
  }

  @ParameterizedTest
  @ValueSource(strings = {".", "\\d", "\\w", "\\s", "\\D", "\\W", "\\S"})
  public void test_parse_meat_classes(String pattern) {
    PatternReverser context = new PatternReverser("context");
    PatternNode unit = PatternParser.parse(context, pattern);
    System.out.println(unit);
    assertTrue(unit instanceof CharacterClassesNode);
  }

  private static final class OctHexArgumentsProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      List<Arguments> arguments = new ArrayList<>();
      arguments.add(Arguments.of(5, Collections.singletonList("\\05")));
      arguments.add(Arguments.of(19, Collections.singletonList("\\023")));
      arguments.add(Arguments.of(209, Collections.singletonList("\\0321")));
      arguments.add(Arguments.of(48, Arrays.asList("\\x30", "\\u0030")));
      arguments.add(Arguments.of(42, Arrays.asList("\\x2a", "\\x2A", "\\u002a", "\\u002A")));
      arguments.add(Arguments.of(188, Arrays.asList("\\xbc", "\\xBc", "\\u00bc", "\\u00BC")));
      arguments.add(Arguments.of(8364, Arrays.asList("\\u20ac", "\\u20Ac", "\\u20aC", "\\u20AC")));
      return arguments.stream();
    }
  }

  @ParameterizedTest
  @ArgumentsSource(OctHexArgumentsProvider.class)
  public void test_parse_oct_and_hex_character(int codePoint, List<String> patterns) {
    PatternReverser context = new PatternReverser("context");
    for (String pattern : patterns) {
      PatternNode node = PatternParser.parse(context, pattern);
      assertTrue(node instanceof CharacterNode);
      assertEquals(codePoint, ((CharacterNode) node).codePoint());
    }
  }

  @Test
  public void test_parse_surrogate_character() {
    PatternReverser context = new PatternReverser("context");
    PatternNode node = PatternParser.parse(context, "[\\x{1F600}-\\x{1F64F}]{10}");
    System.out.println(node.random(new Random()));
  }

  @Test
  public void test_parse_negated_classes() {
    PatternReverser context = new PatternReverser("context");
    PatternNode node1 = PatternParser.parse(context, "[^0-9]");
    assertTrue(node1 instanceof CharacterClassesNode);
    CharacterClasses classes1 = ((CharacterClassesNode) node1).classes();
    assertEquals(CharacterClasses.NON_DIGIT, classes1);
    PatternNode unit2 = PatternParser.parse(context, "[^\t-\r\\x20]");
    assertTrue(unit2 instanceof CharacterClassesNode);
    CharacterClasses classes2 = ((CharacterClassesNode) unit2).classes();
    assertEquals(CharacterClasses.NON_WHITESPACE, classes2);
    PatternNode unit3 = PatternParser.parse(context, "[^0-9A-Z_a-z]");
    assertTrue(unit3 instanceof CharacterClassesNode);
    CharacterClasses classes3 = ((CharacterClassesNode) unit3).classes();
    assertEquals(CharacterClasses.NON_WORD, classes3);
  }
}
