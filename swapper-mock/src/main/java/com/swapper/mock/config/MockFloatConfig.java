/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.swapper.mock.config;

import com.swapper.mock.MockConfig;
import com.swapper.mock.annotations.MockRange;

import java.lang.annotation.Annotation;

public final class MockFloatConfig extends MockConfig {
  private final float[] range = {Float.MIN_VALUE, Float.MAX_VALUE};

  public MockFloatConfig() {
  }

  public MockFloatConfig(float lower, float upper) {
    setRange(lower, upper);
  }

  @Override
  public void withAnnotationConfig(Annotation annotation) {
    if (annotation instanceof MockRange) {
      MockRange config = (MockRange) annotation;
      float[] range = config.floats();
      if (range.length == 1) {
        setRange(range[0], range[0]);
      } else if (range.length == 2) {
        setRange(range[0], range[1]);
      } else {
        throw new IllegalArgumentException("The range array length must be 1 or 2.");
      }
    }
  }

  public float[] getRange() {
    return range.clone();
  }

  public void setRange(float lower, float upper) {
    if (lower > upper) {
      throw new IllegalArgumentException("The upper bound is less than the lower bound.");
    }
    range[0] = lower;
    range[1] = upper;
  }

  public void setValue(float value) {
    range[0] = range[1] = value;
  }
}
