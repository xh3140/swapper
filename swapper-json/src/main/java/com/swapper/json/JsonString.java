/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

/**
 * The JSON string value implement.
 * String type: {@link java.lang.String}.
 */
public final class JsonString extends JsonValue {
  /**
   * Static constructor.
   * Construct a JSON string instance from a string.
   *
   * @param value a string instance.
   * @return a JSON string instance from a string.
   */
  public static JsonString valueOf(String value) {
    return new JsonString(value);
  }

  /**
   * The string value.
   */
  private final String proxy;

  /**
   * Public constructor.
   *
   * @param value a value of JSON string.
   * @throws NullPointerException if value is null.
   */
  public JsonString(String value) {
    proxy = Objects.requireNonNull(value);
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Boolean}.
   *
   * @return the JSON value as a value of type {@link java.lang.Boolean}.
   * @see JsonValue#TRUE#toBoolean()
   * @see JsonValue#FALSE#toBoolean()
   * @see JsonValue#toBoolean()
   */
  @Override
  public Boolean toBoolean() {
    if (JsonSymbols.S_TRUE.equals(proxy)) {
      return Boolean.TRUE;
    } else if (JsonSymbols.S_FALSE.equals(proxy)) {
      return Boolean.FALSE;
    } else {
      throw new UnsupportedOperationException(getClass().getName());
    }
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Byte}.
   *
   * @return the JSON value as a value of type {@link java.lang.Byte}.
   * @see JsonValue#toByte()
   * @see JsonNumber#toByte()
   */
  @Override
  public Byte toByte() {
    return Byte.parseByte(proxy);
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Byte}.
   *
   * @param radix the radix to be used while parsing string.
   * @return the JSON value as a value of type {@link java.lang.Byte}.
   * @throws NumberFormatException if the string does not contain a parsable byte.
   * @see JsonValue#toByte(int)
   * @see JsonString#toByte(int)
   */
  @Override
  public Byte toByte(int radix) {
    return Byte.parseByte(proxy, radix);
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Short}.
   *
   * @return the JSON value as a value of type {@link java.lang.Short}.
   * @see JsonValue#toShort()
   * @see JsonNumber#toShort()
   */
  @Override
  public Short toShort() {
    return Short.parseShort(proxy);
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Short}.
   *
   * @param radix the radix to be used while parsing string.
   * @return the JSON value as a value of type {@link java.lang.Short}.
   * @throws NumberFormatException if the string does not contain a parsable short.
   * @see JsonValue#toShort(int)
   * @see JsonString#toShort(int)
   */
  @Override
  public Short toShort(int radix) {
    return Short.parseShort(proxy, radix);
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Integer}.
   *
   * @return the JSON value as a value of type {@link java.lang.Integer}.
   * @see JsonValue#toInteger()
   * @see JsonNumber#toInteger()
   */
  @Override
  public Integer toInteger() {
    return Integer.parseInt(proxy);
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Integer}.
   *
   * @param radix the radix to be used while parsing string.
   * @return the JSON value as a value of type {@link java.lang.Integer}.
   * @throws NumberFormatException if the string does not contain a parsable int.
   * @see JsonValue#toInteger(int)
   * @see JsonString#toInteger(int)
   */
  @Override
  public Integer toInteger(int radix) {
    return Integer.parseInt(proxy, radix);
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Long}.
   *
   * @return the JSON value as a value of type {@link java.lang.Long}.
   * @see JsonValue#toLong()
   * @see JsonNumber#toLong()
   */
  @Override
  public Long toLong() {
    return Long.parseLong(proxy);
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Long}.
   *
   * @param radix the radix to be used while parsing string.
   * @return the JSON value as a value of type {@link java.lang.Long}.
   * @throws NumberFormatException if the string does not contain a parsable long.
   * @see JsonValue#toLong(int)
   * @see JsonString#toLong(int)
   */
  @Override
  public Long toLong(int radix) {
    return Long.parseLong(proxy, radix);
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Float}.
   *
   * @return the JSON value as a value of type {@link java.lang.Float}.
   * @throws NumberFormatException if the string does not contain a parsable float.
   * @see JsonValue#toFloat()
   * @see JsonNumber#toFloat()
   */
  @Override
  public Float toFloat() {
    return Float.parseFloat(proxy);
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.Double}.
   *
   * @return the JSON value as a value of type {@link java.lang.Double}.
   * @throws NumberFormatException if the string does not contain a parsable double.
   * @see JsonValue#toDouble()
   * @see JsonNumber#toDouble()
   */
  @Override
  public Double toDouble() {
    return Double.parseDouble(proxy);
  }

  /**
   * Returns the JSON value as a value of type {@link java.math.BigInteger}.
   *
   * @return the JSON value as a value of type {@link java.math.BigInteger}.
   * @see JsonValue#toBigInteger()
   * @see JsonNumber#toBigInteger()
   */
  @Override
  public BigInteger toBigInteger() {
    return new BigInteger(proxy);
  }

  /**
   * Returns the JSON value as a value of type {@link java.math.BigInteger}.
   *
   * @param radix the radix to be used while parsing string.
   * @return the JSON value as a value of type {@link java.math.BigInteger}.
   * @throws NumberFormatException if string is not a valid representation of a {@link java.math.BigInteger}.
   * @see JsonValue#toBigInteger(int)
   * @see JsonString#toBigInteger(int)
   */
  @Override
  public BigInteger toBigInteger(int radix) {
    return new BigInteger(proxy, radix);
  }

  /**
   * Returns the JSON value as a value of type {@link java.math.BigDecimal}.
   *
   * @return the JSON value as a value of type {@link java.math.BigDecimal}.
   * @throws NumberFormatException if string is not a valid representation of a {@link java.math.BigDecimal}.
   * @see JsonValue#toBigDecimal()
   * @see JsonNumber#toBigDecimal()
   */
  @Override
  public BigDecimal toBigDecimal() {
    return new BigDecimal(proxy);
  }

  /**
   * Returns the JSON value as a value of type {@link java.lang.String}.
   *
   * @return the JSON value as a value of type {@link java.lang.String}.
   * @see Object#toString()
   * @see JsonValue#toString()
   */
  @Override
  public String toString() {
    return proxy;
  }

  /**
   * Returns the JSON formatted string.
   * Note: ignore {@link JsonSymbols#SOLIDUS}
   *
   * @return the JSON formatted string.
   * @see JsonValue#toJson()
   */
  @Override
  public String toJson() {
    StringBuilder builder = new StringBuilder(proxy.length() * 5 / 4 + 1);
    builder.append(JsonSymbols.QUOTATION_MARK);
    for (char character : proxy.toCharArray()) {
      switch (character) {
        case JsonSymbols.QUOTATION_MARK:
          builder.append(JsonSymbols.REVERSE_SOLIDUS).append(JsonSymbols.QUOTATION_MARK);
          break;
        case JsonSymbols.REVERSE_SOLIDUS:
          builder.append(JsonSymbols.REVERSE_SOLIDUS).append(JsonSymbols.REVERSE_SOLIDUS);
          break;
        case JsonSymbols.BACKSPACE:
          builder.append(JsonSymbols.REVERSE_SOLIDUS).append(JsonSymbols.LETTER_L_B);
          break;
        case JsonSymbols.FORM_FEED:
          builder.append(JsonSymbols.REVERSE_SOLIDUS).append(JsonSymbols.LETTER_L_F);
          break;
        case JsonSymbols.LINE_FEED:
          builder.append(JsonSymbols.REVERSE_SOLIDUS).append(JsonSymbols.LETTER_L_N);
          break;
        case JsonSymbols.CARRIAGE_RETURN:
          builder.append(JsonSymbols.REVERSE_SOLIDUS).append(JsonSymbols.LETTER_L_R);
          break;
        case JsonSymbols.HORIZONTAL_TAB:
          builder.append(JsonSymbols.REVERSE_SOLIDUS).append(JsonSymbols.LETTER_L_T);
          break;
        default:
          builder.append(character);
          break;
      }
    }
    builder.append(JsonSymbols.QUOTATION_MARK);
    return builder.toString();
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param other the reference object with which to compare.
   * @return true if this object is the same as the obj argument; false otherwise.
   * @see Object#equals(Object)
   */
  @Override
  public boolean equals(Object other) {
    if (this == other) return true;
    if (other instanceof JsonString) {
      JsonString that = (JsonString) other;
      return proxy.equals(that.proxy);
    }
    return false;
  }

  /**
   * Gets the hash code of the current instance.
   *
   * @return the hash code of the current instance.
   * @see Object#hashCode()
   */
  @Override
  public int hashCode() {
    return proxy.hashCode();
  }
}
