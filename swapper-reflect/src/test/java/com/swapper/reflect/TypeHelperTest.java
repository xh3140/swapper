package com.swapper.reflect;

import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.time.DayOfWeek;
import java.time.Month;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.swapper.reflect.TypeHelper.*;
import static org.junit.jupiter.api.Assertions.*;

class TypeHelperTest {

  @Test
  public void test_method_isPrimitive() {
    assertTrue(isPrimitive(void.class));
    assertTrue(isPrimitive(boolean.class));
    assertTrue(isPrimitive(char.class));
    assertTrue(isPrimitive(byte.class));
    assertTrue(isPrimitive(short.class));
    assertTrue(isPrimitive(int.class));
    assertTrue(isPrimitive(long.class));
    assertTrue(isPrimitive(float.class));
    assertTrue(isPrimitive(double.class));
  }

  @Test
  public void test_method_not_isPrimitive() {
    assertFalse(isPrimitive(Void.class));
    assertFalse(isPrimitive(Boolean.class));
    assertFalse(isPrimitive(Character.class));
    assertFalse(isPrimitive(Byte.class));
    assertFalse(isPrimitive(Short.class));
    assertFalse(isPrimitive(Integer.class));
    assertFalse(isPrimitive(Long.class));
    assertFalse(isPrimitive(Float.class));
    assertFalse(isPrimitive(Double.class));
  }

  @Test
  public void test_method_isEnum() {
    assertTrue(isEnum(Month.class));
    assertTrue(isEnum(DayOfWeek.class));
    assertFalse(isEnum(Object.class));
    assertFalse(isEnum(java.lang.Enum.class));
  }

  @Test
  public void test_method_isArray() {
    assertTrue(isArray(int[].class));
    assertTrue(isArray(Byte[].class));
    assertTrue(isArray(List[].class));
    assertFalse(isArray(Object.class));
  }

  @Test
  public void test_method_getArrayComponentType() {
    assertEquals(boolean.class, getArrayComponentType(boolean[].class));
    assertEquals(char.class, getArrayComponentType(char[].class));
    assertEquals(byte.class, getArrayComponentType(byte[].class));
    assertEquals(short.class, getArrayComponentType(short[].class));
    assertEquals(int.class, getArrayComponentType(int[].class));
    assertEquals(long.class, getArrayComponentType(long[].class));
    assertEquals(float.class, getArrayComponentType(float[].class));
    assertEquals(double.class, getArrayComponentType(double[].class));

    assertEquals(Boolean.class, getArrayComponentType(Boolean[].class));
    assertEquals(Character.class, getArrayComponentType(Character[].class));
    assertEquals(Byte.class, getArrayComponentType(Byte[].class));
    assertEquals(Short.class, getArrayComponentType(Short[].class));
    assertEquals(Integer.class, getArrayComponentType(Integer[].class));
    assertEquals(Long.class, getArrayComponentType(Long[].class));
    assertEquals(Float.class, getArrayComponentType(Float[].class));
    assertEquals(Double.class, getArrayComponentType(Double[].class));

    assertEquals(List.class, getArrayComponentType(List[].class));
    assertEquals(Set.class, getArrayComponentType(Set[].class));
    assertEquals(Map.class, getArrayComponentType(Map[].class));

    assertNull(getArrayComponentType(Boolean.class));
    assertNull(getArrayComponentType(Character.class));
    assertNull(getArrayComponentType(Byte.class));
    assertNull(getArrayComponentType(Short.class));
    assertNull(getArrayComponentType(Integer.class));
    assertNull(getArrayComponentType(Long.class));
    assertNull(getArrayComponentType(Float.class));
    assertNull(getArrayComponentType(Double.class));
  }

  @Test
  public void test_method_getCollectionElementType() {
    {
      TypeParser<List<String>> parser = new TypeParser<List<String>>() {
      };
      assertEquals(String.class, getCollectionElementType(parser.getType(), parser.getRawType()));
    }
    {
      TypeParser<List<String>> parser1 = new TypeParser<List<String>>() {
      };
      TypeParser<List<List<String>>> parser2 = new TypeParser<List<List<String>>>() {
      };
      assertEquals(parser1.getType(), getCollectionElementType(parser2.getType(), parser2.getRawType()));
    }
  }

  @Test
  public void test_method_getMapKeyAndValueTypes() {
    {
      TypeParser<Map<Integer, String>> parser = new TypeParser<Map<Integer, String>>() {
      };
      Type[] types = getMapKeyAndValueTypes(parser.getType(), parser.getRawType());
      assertNotNull(types);
      assertEquals(Integer.class, types[0]);
      assertEquals(String.class, types[1]);
    }
    {
      TypeParser<Set<Number>> parser1 = new TypeParser<Set<Number>>() {
      };
      TypeParser<Map<Character, Set<Number>>> parser2 = new TypeParser<Map<Character, Set<Number>>>() {
      };
      Type[] types = getMapKeyAndValueTypes(parser2.getType(), parser2.getRawType());
      assertNotNull(types);
      assertEquals(Character.class, types[0]);
      assertEquals(parser1.getType(), types[1]);
    }
  }
}
