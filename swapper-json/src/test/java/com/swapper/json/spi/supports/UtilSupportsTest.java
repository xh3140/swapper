/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.spi.supports;

import com.swapper.json.JsonArray;
import com.swapper.json.TestSupportByArray;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.Currency;
import java.util.UUID;

public class UtilSupportsTest {

  @Test
  public void test_support_currency() {
    Currency.getAvailableCurrencies().forEach(currency -> {
      Currency[] rawArray = {currency};
      JsonArray rawJsonArray = JsonArray.fromObject(rawArray);
      String json = rawJsonArray.toJson();
      JsonArray genJsonArray = JsonArray.fromJson(json);
      Currency[] genArray = genJsonArray.parseObject(Currency[].class);
      Assertions.assertEquals(rawArray[0].getCurrencyCode(), genArray[0].getCurrencyCode());
    });
  }

  @RepeatedTest(10)
  public void test_support_uuid() {
    TestSupportByArray.test(UUID.randomUUID());
  }
}
