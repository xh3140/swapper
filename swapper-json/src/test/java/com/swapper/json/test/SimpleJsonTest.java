/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.test;

import com.swapper.json.JsonDocument;
import com.swapper.json.JsonValue;
import com.swapper.reflect.TypeParser;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class SimpleJsonTest {
  public static final class InnerArgumentsProvider implements ArgumentsProvider {

    private Arguments args(String json, Type... types) {
      return Arguments.of(json, Arrays.asList(types));
    }

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      List<Arguments> arguments = new ArrayList<>();
      arguments.add(args("[]", Object.class, Object[].class));
      arguments.add(args("{}", Object.class, Map.class));
      arguments.add(args("[null,true,false]", Object[].class, Boolean[].class));
      arguments.add(args("[0,-123,123.0123,0.0,0e0,-0e1,1.23e23,-1e+3,1e-5,1131.211E+23,1232.00233e-23]",
        Number[].class, BigDecimal[].class,
        new TypeParser<List<Number>>() {
        }.getType()
      ));
      arguments.add(args("[\"\",\" \",\"Hello, World!\",\"\\b\\f\\n\\r\\t\"]", String[].class));
      arguments.add(args("[\"\u4f60\u597d\uff0c\u4e16\u754c\uff01\"]", String[].class));
      arguments.add(args("[[1,2,3],[4,5,6],[7,8,9]]", int[][].class, Number[][].class));
      arguments.add(args("{\"a\":[1,2,3],\"b\":[4,5,6],\"c\":[7,8,9]}",
        new TypeParser<Map<Character, byte[]>>() {
        }.getType(),
        new TypeParser<Map<String, List<Integer>>>() {
        }.getType()
      ));
      return arguments.stream();
    }

  }

  @BeforeAll
  public static void beforeAll() {
    System.out.println("Test simple json start ...");
  }

  @DisplayName("Test Simple Json")
  @ParameterizedTest
  @ArgumentsSource(InnerArgumentsProvider.class)
  public void test(String json, List<Type> types) {
    JsonDocument document = JsonDocument.fromJson(json);
    types.forEach(type -> {
      Object object = document.parseObject(type);
      Assertions.assertNotNull(object);
      JsonValue value = JsonDocument.valueOf(object, type);
      System.out.println(value.toJson());
    });
  }

  @AfterAll
  public static void afterAll() {
    System.out.println("Test simple json end !");
  }
}
