package com.swapper.hls.tag;

import com.swapper.hls.internal.info.Attribute;
import com.swapper.hls.internal.info.Tag;

@Tag(grouping = Tag.Grouping.MediaSegment)
public final class DateRangeTag extends AbstractTag {
  public static final String TAG_NAME = "#EXT-X-DATERANGE";
  private IdAttribute mId;
  private ClassAttribute mClass;
  private StartDateAttribute mStartDate;
  private EndDateAttribute mEndDate;
  private DurationAttribute mDuration;
  private PlannedDurationAttribute mPlannedDuration;
  private ClientAttribute mClient;
  private EndDateAttribute mEndOnNext;

  public DateRangeTag() {
    super(TAG_NAME);
  }

  @Attribute(necessity = Attribute.Necessity.Required)
  public static final class IdAttribute extends AbstractAttribute<String> {
    public static final String ATTRIBUTE_NAME = "ID";

    private IdAttribute(String value) {
      super(ATTRIBUTE_NAME, value);
    }

    @Override
    public String verify(String value) {
      return null;
    }
  }

  @Attribute(necessity = Attribute.Necessity.Required)
  public static final class ClassAttribute extends AbstractAttribute<String> {
    public static final String ATTRIBUTE_NAME = "CLASS";

    private ClassAttribute(String value) {
      super(ATTRIBUTE_NAME, value);
    }

    @Override
    public String verify(String value) {
      return null;
    }
  }

  @Attribute(necessity = Attribute.Necessity.Required)
  public static final class StartDateAttribute extends AbstractAttribute<String> {
    public static final String ATTRIBUTE_NAME = "START-DATE";

    private StartDateAttribute(String value) {
      super(ATTRIBUTE_NAME, value);
    }

    @Override
    public String verify(String value) {
      return null;
    }
  }

  @Attribute(necessity = Attribute.Necessity.Required)
  public static final class EndDateAttribute extends AbstractAttribute<String> {
    public static final String ATTRIBUTE_NAME = "END-DATE";

    private EndDateAttribute(String value) {
      super(ATTRIBUTE_NAME, value);
    }

    @Override
    public String verify(String value) {
      return null;
    }
  }

  @Attribute(necessity = Attribute.Necessity.Required)
  public static final class DurationAttribute extends AbstractAttribute<Float> {
    public static final String ATTRIBUTE_NAME = "DURATION";

    private DurationAttribute(Float value) {
      super(ATTRIBUTE_NAME, value);
    }

    @Override
    public Float verify(Float value) {
      return null;
    }
  }

  @Attribute(necessity = Attribute.Necessity.Required)
  public static final class PlannedDurationAttribute extends AbstractAttribute<Float> {
    public static final String ATTRIBUTE_NAME = "PLANNED-DURATION";

    private PlannedDurationAttribute(Float value) {
      super(ATTRIBUTE_NAME, value);
    }

    @Override
    public Float verify(Float value) {
      return null;
    }
  }

  @Attribute(necessity = Attribute.Necessity.Required)

  public static final class ClientAttribute extends AbstractAttribute<String> {
    private ClientAttribute(String name, String value) {
      super(name, value);
      if (!name.startsWith("X-")) {
        throw new IllegalStateException("The HLS attribute name is invalid.");
      }
    }

    @Override
    public String verify(String value) {
      return null;
    }
  }

  @Attribute(necessity = Attribute.Necessity.Required)

  public static final class SCTE35CmdAttribute extends AbstractAttribute<String> {
    public static final String ATTRIBUTE_NAME = "SCTE35-CMD";

    private SCTE35CmdAttribute(String value) {
      super(ATTRIBUTE_NAME, value);
    }

    @Override
    public String verify(String value) {
      return null;
    }
  }

  @Attribute(necessity = Attribute.Necessity.Required)
  public static final class SCTE35OutAttribute extends AbstractAttribute<String> {
    public static final String ATTRIBUTE_NAME = "SCTE35-OUT";

    private SCTE35OutAttribute(String value) {
      super(ATTRIBUTE_NAME, value);
    }

    @Override
    public String verify(String value) {
      return null;
    }
  }

  @Attribute(necessity = Attribute.Necessity.Required)

  public static final class SCTE35InAttribute extends AbstractAttribute<String> {
    public static final String ATTRIBUTE_NAME = "SCTE35-IN";

    private SCTE35InAttribute(String value) {
      super(ATTRIBUTE_NAME, value);
    }

    @Override
    public String verify(String value) {
      return null;
    }
  }

  @Attribute(necessity = Attribute.Necessity.Required)
  public static final class EndOnNextAttribute extends AbstractAttribute<String> {
    public static final String ATTRIBUTE_NAME = "END-ON-NEXT";

    private EndOnNextAttribute(String value) {
      super(ATTRIBUTE_NAME, value);
    }

    @Override
    public String verify(String value) {
      return null;
    }
  }
}
