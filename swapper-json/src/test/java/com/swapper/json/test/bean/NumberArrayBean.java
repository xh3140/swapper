/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.test.bean;

import com.swapper.mock.bean.QuickDirtyBean;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * For code brevity, use the public modifier instead of the setter and getter.
 */
public final class NumberArrayBean extends QuickDirtyBean {
  public byte[] mByteArray1;

  public Byte[] mByteArray2;

  public short[] mShortArray1;

  public Short[] mShortArray2;

  public int[] mIntegerArray1;

  public Integer[] mIntegerArray2;

  public long[] mLongArray1;

  public Long[] mLongArray2;

  public float[] mFloatArray1;

  public Float[] mFloatArray2;

  public double[] mDoubleArray1;

  public Double[] mDoubleArray2;

  public BigInteger[] mBigIntegerArray;

  public BigDecimal[] mBigDecimalArray;
}
