/* This file is part of swapper project
 *
 * Copyright (C) 2020 The Swapper Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.swapper.json.docs;

import com.swapper.json.JsonObject;
import com.swapper.json.JsonValue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.DayOfWeek;
import java.util.Date;

public class DeserializationTest {
  @Test
  public void testAutoDeserialization() {
    String json = "{" +
            "\"mNull\": null," +
            "\"mBooleanTrue\": true," +
            "\"mBooleanFalse\": false," +
            "\"mByte\": 127," +
            "\"mShort\": 32767," +
            "\"mInteger\": 2147483647," +
            "\"mLong\": 9223372036854775807," +
            "\"mFloat\": 3.4028235E38," +
            "\"mDouble\": 1.7976931348623157E308," +
            "\"mBigInteger\": 142829797372960495909951083877238563774," +
            "\"mBigDecimal\": 3.14159265358979323846264338327950288419716939937510," +
            "\"mStringEmpty\": \"\"," +
            "\"mStringNormal\": \"Hello,world!\"," +
            "\"mStringEscape\": \" \\\\ \\r \\n \\b \\t \\f \\\" \"," +
            "\"mStringHexText\": \"\u4f60\u597d\uff0c\u4e16\u754c\uff01\"," +
            "\"mCharacter\": \"#\"," +
            "\"mDate\": 1607703132000," +
            "\"mEnum\": \"MONDAY\"" +
            "}";
    // Auto build the data structure and fill in the parameters
    JsonObject jsonObject = JsonObject.fromJson(json);
    // JSON deserialization
    TestBean bean = jsonObject.parseObject(TestBean.class);
    Assertions.assertNotNull(bean);
  }

  @Test
  public void testManuallyDeserialization() {
    // Manually build the data structure and fill in the parameters
    JsonObject jsonObject = new JsonObject();
    jsonObject.put("mNull", JsonValue.valueOf(null));
    jsonObject.put("mBooleanTrue", JsonValue.valueOf(true));
    jsonObject.put("mBooleanFalse", JsonValue.valueOf(false));
    jsonObject.put("mByte", JsonValue.valueOf(Byte.MAX_VALUE));
    jsonObject.put("mShort", JsonValue.valueOf(Short.MAX_VALUE));
    jsonObject.put("mInteger", JsonValue.valueOf(Integer.MAX_VALUE));
    jsonObject.put("mLong", JsonValue.valueOf(Long.MAX_VALUE));
    jsonObject.put("mFloat", JsonValue.valueOf(Float.MAX_VALUE));
    jsonObject.put("mDouble", JsonValue.valueOf(Double.MAX_VALUE));
    jsonObject.put("mBigInteger", JsonValue.valueOf(new BigInteger("6b7404b3f87e49376fac3b6a43dbe3be", 16)));
    jsonObject.put("mBigDecimal", JsonValue.valueOf(new BigDecimal("3.14159265358979323846264338327950288419716939937510")));
    jsonObject.put("mStringEmpty", JsonValue.valueOf(""));
    jsonObject.put("mStringNormal", JsonValue.valueOf("Hello,world!"));
    jsonObject.put("mStringEscape", JsonValue.valueOf(" \\ \r \n \b \t \f \" "));
    jsonObject.put("mStringHexText", JsonValue.valueOf("\u4f60\u597d\uff0c\u4e16\u754c\uff01"));
    jsonObject.put("mCharacter", JsonValue.valueOf('#'));
    jsonObject.put("mDate", JsonValue.valueOf(new Date(1607703132000L)));
    jsonObject.put("mEnum", JsonValue.valueOf(DayOfWeek.MONDAY));
    // JSON deserialization
    TestBean bean = jsonObject.parseObject(TestBean.class);
    Assertions.assertNotNull(bean);
  }
}
